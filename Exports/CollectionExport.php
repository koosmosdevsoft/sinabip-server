<?php

namespace sinabipmuebles\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class CollectionExport extends DefaultValueBinder  implements WithColumnFormatting,ShouldAutoSize,FromArray,WithMapping,WithHeadingRow,WithCustomValueBinder
{
    protected $invoices;

    public function map($invoice): array
    {
        return $invoice;
    }
    
    public function columnFormats(): array
    {
        return [
            // 'B' => '###########',
            'B' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
        ];
    }
    public function __construct()
    {
        // $this->invoices = $invoices;
    }

    public function load(array $invoices){
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }
    
    public function bindValue(Cell $cell, $value)
    {
        if (strlen($value)  == 12) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING2);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}

?>