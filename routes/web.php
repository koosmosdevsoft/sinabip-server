<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('MenuBandejaReporteria/{id}', 'MenuPrincipalController@MenuBandejaReporteria');
Route::post('ListadoPrediosWeb', 'PrediosWebController@ListadoPrediosWeb');
Route::post('BuscarPrediosWeb', 'PrediosWebController@BuscarPrediosWeb');
Route::post('BusquedaAvanzadaPredios', 'PrediosWebController@BusquedaAvanzadaPredios');

Route::post('ListadoPrediosCS', 'PrediosWebController@ListadoPrediosCS');
Route::post('InsertarPrediosWeb', 'PrediosWebController@InsertarPrediosWeb');
Route::post('ObtenerUbigeo', 'PrediosWebController@ObtenerUbigeo');
Route::post('ObtenerEntidades', 'PrediosWebController@ObtenerEntidades');
Route::post('ActualizarRUC', 'PrediosWebController@ActualizarRUC');

/*REGISTRAR*/
Route::post('DatosPrincipales', 'PrediosWebController@DatosPrincipales');
Route::post('fabricaLinderos', 'PrediosWebController@fabricaLinderos');
Route::post('Limitaciones', 'PrediosWebController@Limitaciones');
Route::post('Actos', 'PrediosWebController@Actos');
Route::post('DatosTecnicos', 'PrediosWebController@DatosTecnicos');
Route::post('Construcciones', 'PrediosWebController@Construcciones');
Route::post('Obras', 'PrediosWebController@Obras');
Route::post('Zona', 'PrediosWebController@Zona');
/*OBTENER*/
Route::get('ObtenerData/{accion}/{codinterno}', 'PrediosWebController@ObtenerData');
Route::get('DetallesPiso/{codinterno}/{codconst}', 'PrediosWebController@DetallesPiso');
Route::get('UltimoProceso/{codinterno}', 'PrediosWebController@UltimoProceso');
Route::get('UltimoLL/{codinterno}', 'PrediosWebController@UltimoLL');
Route::get('UltimoLF/{codinterno}', 'PrediosWebController@UltimoLF');
Route::get('UltimoLT/{codinterno}', 'PrediosWebController@UltimoLT');
Route::get('UltimoO/{codinterno}', 'PrediosWebController@UltimoO');
Route::get('UltimoC/{codinterno}', 'PrediosWebController@UltimoC');
Route::get('UltimoDetPiso/{codinterno}/{codigo_construccion}', 'PrediosWebController@UltimoDetPiso');
Route::get('UltimoIA/{codinterno}', 'PrediosWebController@UltimoIA');
/*ELIMINAR*/
Route::get('EliminarProceso/{codinterno}/{item}', 'PrediosWebController@EliminarProceso');
Route::get('EliminarLL/{codinterno}/{item}', 'PrediosWebController@EliminarLL');
Route::get('EliminarLF/{codinterno}/{item}', 'PrediosWebController@EliminarLF');
Route::get('EliminarActosAd/{codinterno}/{item}', 'PrediosWebController@EliminarActosAd');
Route::get('EliminarLT/{codinterno}/{item}', 'PrediosWebController@EliminarLT');
Route::get('EliminarO/{codinterno}/{item}', 'PrediosWebController@EliminarO');
Route::get('EliminarC/{codinterno}/{item}', 'PrediosWebController@EliminarC');
Route::get('EliminarDP/{codinterno}/{item}/{codconst}', 'PrediosWebController@EliminarDP');
Route::get('Verificar_cus/{cod_interno}/{idControlCalidad}','ControlRegistroController@Verificar_cus');
Route::get('Verificar_historico/{idControlCalidad}','ControlRegistroController@Verificar_historico');
Route::post('SubirDocumentoLegajo/{data}' , 'BusquedaController@SubirDocumentoLegajo');
Route::get('EliminarIndependizacion/{codigo}/{item}', 'PrediosWebController@EliminarIndependizacion');
Route::get('validar_cus/{nrocus}', 'PrediosWebController@validar_cus');
//Route::post('Guardar_imagenes' , 'BusquedaController@Guardar_imagenes');

Route::get('VerPDF/{data}' , 'BusquedaController@VerPDF');
Route::get('VisualizarPDF/{archivo}/{cus}','BusquedaController@VisualizarPDF');
Route::get('VisualizarFoto/{archivo}','AporteFotosController@VisualizarFoto');
Route::get('VisualizarFoto/{archivo}/{codigo}','AporteFotosController@VisualizarFoto');
Route::post('ListadoAporteFotos' , 'AporteFotosController@ListadoAporteFotos');

Route::post('AgregarExpediente', 'PrediosWebController@AgregarExpediente');
Route::post('CargarListadoExpedientesRelacionados', 'PrediosWebController@CargarListadoExpedientesRelacionados');
Route::post('QuitarExpediente', 'PrediosWebController@QuitarExpediente');

Route::post('AgregarCus', 'PrediosWebController@AgregarCus');
Route::post('CargarListadoCusRelacionados', 'PrediosWebController@CargarListadoCusRelacionados');
Route::post('QuitarCus', 'PrediosWebController@QuitarCus');


/* Imprimir Caratula */ 
Route::get('Imprimir_caratula/{numero}','ControlRegistroController@Imprimir_caratula');
Route::post('subirArchivo', 'PrediosWebController@subirArchivo');
Route::get('verReportePDF/{ruc}','PrediosWebController@verReportePDF');
Route::get('ExportaPrediosExcel/{ruc}','PrediosWebController@ExportaPrediosExcel');
Route::get('ValidarExistenciaRUC/{ruc}','PrediosWebController@ValidarExistenciaRUC');

Route::post('AdjuntarCusMasivo/{data}','estandarizacionController@AdjuntarCusMasivo');
Route::post('AdjuntarActoMasivo/{data}','estandarizacionController@AdjuntarActoMasivo');
