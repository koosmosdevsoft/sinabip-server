<?php


Route::get('demo', function(){
    return 'ss';
});

//DATOS GENERALES
Route::post('traerData','ReportesController@traerData');
Route::get('detalleporDepartamento/{codTecnico}/{codLegal}/{mes}/{anio}','ReportesController@detalleporDepartamento');
Route::get('descargarXLS_detalleporDepartamento/{codTecnico}/{codLegal}/{mes}/{anio}','ReportesController@descargarXLS_detalleporDepartamento');
Route::get('detalleporEtapas/{codTecnico}/{codLegal}/{etapa}/{anio}','ReportesController@detalleporEtapas');
Route::get('descargarXLS_detalleporEtapas/{codTecnico}/{codLegal}/{etapa}/{anio}','ReportesController@descargarXLS_detalleporEtapas');
Route::get('detallesinPoligonos/{anio}/{accion}','ReportesController@detallesinPoligonos');
Route::get('descargarXLS_detallesinPoligonos/{anio}/{accion}','ReportesController@descargarXLS_detallesinPoligonos');
