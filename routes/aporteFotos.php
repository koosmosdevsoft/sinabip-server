<?php

/* NOTIFICACIONES */
Route::post('ListadoAporteFotos' , 'AporteFotosController@ListadoAporteFotos');
Route::post('Guardar_imagenes' , 'AporteFotosController@Guardar_imagenes');

Route::post('EliminarAporteFotos' , 'AporteFotosController@EliminarAporteFotos');


Route::get('verDemo/{cus}', 'AporteFotosController@verImagenes');
Route::post('cargaImagenesPrueba' , 'AporteFotosController@cargaImagenesPrueba');  
Route::post('Guardar_imagenes_prueba' , 'AporteFotosController@Guardar_imagenes_prueba');


