<?php

Route::post('buscarLiterales','estandarizacionController@buscarLiterales');
Route::post('grabarEntidad','estandarizacionController@grabarEntidad');
Route::post('VincularCusIndividual','estandarizacionController@VincularCusIndividual');
Route::post('VincularCusMasivo','estandarizacionController@VincularCusMasivo');

Route::post('VincularActoIndividual','estandarizacionController@VincularActoIndividual');
Route::post('VincularActoMasivo','estandarizacionController@VincularActoMasivo');

Route::post('VincularNiveles','estandarizacionController@VincularNiveles');

Route::post('cargarListaGobRegionales','estandarizacionController@cargarListaGobRegionales');
Route::post('cambiar_gobRegional','estandarizacionController@cambiar_gobRegional');

Route::post('VincularPredios','estandarizacionController@VincularPredios');

Route::post('cargarListaNivelGobierno','estandarizacionController@cargarListaNivelGobierno');
Route::post('cargarListaNivelGeneral','estandarizacionController@cargarListaNivelGeneral');
Route::post('cargarListaNivelSectorial','estandarizacionController@cargarListaNivelSectorial');
Route::post('cargarListaOrganismo','estandarizacionController@cargarListaOrganismo');

Route::post('obtenerDescripcionEntidad','estandarizacionController@obtenerDescripcionEntidad');

