<?php

//DATOS GENERALES
Route::get('CondicionRegistro','BusquedaController@CondicionRegistro');
Route::get('Calificacion','BusquedaController@Calificacion');
Route::get('Via','BusquedaController@Via');
Route::get('Detalle','BusquedaController@Detalle');
Route::get('Habilitacion','BusquedaController@Habilitacion'); 
Route::get('Competencia','BusquedaController@Competencia');

//ADQUISICION
Route::post('Otorgante','BusquedaController@Otorgante');
Route::get('TipoDocumento','BusquedaController@TipoDocumento');
Route::get('Modalidad','BusquedaController@Modalidad');
Route::get('DispositivoLegal','BusquedaController@DispositivoLegal');
Route::get('Documentos','BusquedaController@Documentos');
Route::get('Aporte','BusquedaController@Aporte');
Route::post('Instituciones','BusquedaController@Instituciones');
Route::get('Obtener_Inst/{codigo}','BusquedaController@Obtener_Inst');
Route::get('Oficina/{coddepa}','BusquedaController@Oficina');
Route::get('Partida','BusquedaController@Partida');
Route::get('DetalleApoEquiUrb/{filtro}','BusquedaController@DetalleApoEquiUrb');


//modal
Route::get('Lindero','BusquedaController@Lindero');
Route::get('NombreLindero/{codlindero}','BusquedaController@NombreLindero');
Route::get('Acto','BusquedaController@Acto');
Route::get('DetalleActo/{codacto}','BusquedaController@DetalleActo');
Route::get('Moneda','BusquedaController@Moneda');
//LIMITACIONES
Route::get('Restricciones','BusquedaController@Restricciones');
Route::get('DetalleRestricciones/{codrestriccion}','BusquedaController@DetalleRestricciones');
Route::get('Materias','BusquedaController@Materias');
Route::get('GetNombreMaterias/{codmateria}','BusquedaController@GetNombreMaterias');
Route::get('ListadoProcesosJudiciales/{codigo_interno}','BusquedaController@ListadoProcesosJudiciales');
Route::get('antecedentes/{codigo_interno}/{codigo_detalle}','BusquedaController@antecedentes');
Route::get('datosJudiciales/{codigo_detalle}','BusquedaController@datosJudiciales');
Route::get('sentencia/{codigo_detalle}','BusquedaController@sentencia');
Route::get('ActoIA','BusquedaController@ActoIA');
Route::get('TipoRegistro','BusquedaController@TipoRegistro');


//ADMINISTRACIONES
Route::get('ActoAd','BusquedaController@ActoAd');
Route::get('DetalleActoAd/{codacto}','BusquedaController@DetalleActoAd');
Route::get('DetalleNombreActo/{coddetalle}','BusquedaController@DetalleNombreActo');
Route::get('NombreDetalleDatosFabrica/{coddetalle}','BusquedaController@NombreDetalleDatosFabrica');
Route::get('Generico','BusquedaController@Generico');
Route::get('Especifico/{codgenerico}','BusquedaController@Especifico');
Route::get('DerechoInscrito','BusquedaController@DerechoInscrito');
Route::get('Vigencia','BusquedaController@Vigencia');
Route::get('NombreInstitucion/{codinstitucion}','BusquedaController@NombreInstitucion');
Route::get('ObtenerItem/{codinterno}','BusquedaController@ObtenerItem');
//DATOS TECNICOS
Route::get('Zona','BusquedaController@Zona');
Route::get('Terreno','BusquedaController@Terreno');
Route::get('Zonificacion','BusquedaController@Zonificacion');
Route::get('Ocupacion','BusquedaController@Ocupacion');
Route::get('ObPoseedor/{codinterno}','BusquedaController@ObPoseedor');
//CONSTRUCCIONES
Route::get('EstadoHabilitacion','BusquedaController@EstadoHabilitacion');
Route::get('SituacionFisica','BusquedaController@SituacionFisica');
Route::get('MaterialConstruccion','BusquedaController@MaterialConstruccion');
Route::get('Combos','BusquedaController@Combos');
//OBRAS 
Route::get('Valorizacion','BusquedaController@Valorizacion');
Route::get('Materiales','BusquedaController@Materiales');
Route::get('Estado','BusquedaController@Estado');
//ZONA PLAYA 
Route::get('Lam','BusquedaController@Lam');
Route::get('Playa','BusquedaController@Playa');
Route::get('Restringido','BusquedaController@Restringido');
Route::get('Privado','BusquedaController@Privado');
//LEGAJO DIGITAL
Route::get('ListadoTipoDocumentos','BusquedaController@ListadoTipoDocumentos');
Route::get('VerPD/{archivo}','BusquedaController@VerPD');
Route::post('Registrar_Aporte_Legajo_temporal' , 'BusquedaController@Registrar_Aporte_Legajo_temporal');
Route::post('AceptarArchivoAdjunto' , 'BusquedaController@AceptarArchivoAdjunto');
Route::post('ListadoAporteDocumentos' , 'BusquedaController@ListadoAporteDocumentos');
Route::post('EliminarArchivoAdjunto' , 'BusquedaController@EliminarArchivoAdjunto');
Route::post('EliminarDocumentoAporte' , 'BusquedaController@EliminarDocumentoAporte');
Route::post('datosSituacionCatastral' , 'BusquedaController@datosSituacionCatastral');
Route::post('GuardarObservaciones' , 'BusquedaController@GuardarObservaciones');