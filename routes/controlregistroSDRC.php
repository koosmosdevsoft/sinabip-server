<?php

/* Control de Registro */
Route::post('Busqueda_Cus_registrados_internos' , 'ControlRegistroController@Busqueda_Cus_registrados_internos');
Route::post('Busqueda_Cus_registrados_externos' , 'ControlRegistroController@Busqueda_Cus_registrados_externos');
Route::post('Aprobar_cus' , 'ControlRegistroController@Aprobar_cus');
Route::get('UpdateNotificacion/{codinterno}/{cod_user}/{observacion}/{idControlCalidad}' , 'ControlRegistroController@UpdateNotificacion');


Route::post('obtenerTipoAccionDetalleUD' , 'ControlRegistroController@obtenerTipoAccionDetalleUD');  
Route::post('Busqueda_Cus_registrados_notificados' , 'ControlRegistroController@Busqueda_Cus_registrados_notificados');
Route::post('Busqueda_Cus_registrados_aprobados' , 'ControlRegistroController@Busqueda_Cus_registrados_aprobados');
Route::get('verificador_errores_registro/{codinterno}/{idControlCalidad}' , 'ControlRegistroController@verificador_errores_registro');
Route::get('Verificar_cusCheck/{codinterno}/{idControlCalidad}/{pestana}' , 'ControlRegistroController@Verificar_cusCheck');
Route::post('GuardarNotificacion', 'ControlRegistroController@GuardarNotificacion');  
