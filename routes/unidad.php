<?php

//DATOS GENERALES
Route::post('ListadoDocumentos','UnidadController@ListadoDocumentos');
Route::get('TecnicoLegal/{codusu}','UnidadController@TecnicoLegal');
Route::post('GenerarUnidad','UnidadController@GenerarUnidad');
Route::post('ListadoUnidad','UnidadController@ListadoUnidad');
Route::get('CargarVerificacion/{cod_documental}','UnidadController@CargarVerificacion');
Route::get('Documentos/{tipo}','UnidadController@Documentos');
Route::post('BuscarDocumento','UnidadController@BuscarDocumento');
Route::post('AgregarDocumento','UnidadController@AgregarDocumento');
Route::post('EliminarDocumento','UnidadController@EliminarDocumento');
Route::post('BuscarUD','UnidadController@BuscarUD');
Route::get('EliminarUnidad/{codigo}','UnidadController@EliminarUnidad');   
Route::post('BusquedaCUSparaActualizar','UnidadController@BusquedaCUSparaActualizar'); 
Route::post('TerminarProceso','UnidadController@TerminarProceso'); 
Route::post('ActualizarTipoDetalleAccionUD','UnidadController@ActualizarTipoDetalleAccionUD'); 
Route::get('BuscarCodigoPredio/{nroCUS}','UnidadController@BuscarCodigoPredio'); 
Route::get('BuscarCUS/{codigopredio}','UnidadController@BuscarCUS'); 
Route::get('ObtenerSuma/{codigopredio}','UnidadController@ObtenerSuma'); 
Route::post('ActualizaNotificacion','UnidadController@ActualizaNotificacion'); 
Route::post('ActualizaNotificacion','UnidadController@ActualizaNotificacion'); 
Route::post('EnviarSbn','UnidadController@EnviarSbn'); 
Route::get('ValidarEstadoUD/{nroCUS}','UnidadController@ValidarEstadoUD'); 
Route::get('verificacionExistenciaDocsTL/{codUD}','UnidadController@verificacionExistenciaDocsTL'); 
Route::get('traerProfesionalesUD/{codUD}','UnidadController@traerProfesionalesUD'); 
Route::post('actualizarProfesionales','UnidadController@actualizarProfesionales'); 
Route::get('existeCUS/{codcus}','UnidadController@existeCUS'); 
Route::post('CargarPrediosExternos','UnidadController@CargarPrediosExternos');
Route::get('openverDocumentos/{codigoudocumental}','UnidadController@openverDocumentos'); 
Route::get('openDocumentosxDefecto','UnidadController@openDocumentosxDefecto'); 
Route::post('ObtenerDatosDocumento','UnidadController@ObtenerDatosDocumento');
Route::get('ValidarUD/{cus}','UnidadController@ValidarUD');
Route::post('CargarListadoAuditoriaPredios','UnidadController@CargarListadoAuditoriaPredios');
Route::post('openDetalleAuditoria','UnidadController@openDetalleAuditoria');
Route::get('validarExistenciaRUC/{nroRUC}','UnidadController@validarExistenciaRUC'); 

