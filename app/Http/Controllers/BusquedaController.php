<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;

class BusquedaController extends Controller
{
   
    //DATOS GENERALES
    public function CondicionRegistro(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_CONDICION_REGISTRO');
        return response()->success($data);                             
    }
    public function Calificacion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_CALIFICACION_REGISTRO');
        return response()->success($data);                             
    }
    public function Via(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_VIAS');
        return response()->success($data);                             
    }
    public function Detalle(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPODETALLE');
        return response()->success($data);                             
    }
    public function Habilitacion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_HABILITACION');
        return response()->success($data);                             
    }
    public function Competencia(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_COMPETENCIA');
        return response()->success($data);                              
    }

    //ADQUISCION
    public function Otorgante(Request $request){               
        $nombre    = $request->valor;
        $page     = $request->page;
        $records  = $request->records;   
        $seleccion = $request->seleccion; 

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_INSTITUCIONES_OTORGANTES ?,?,?,?',[$nombre,$page,$records,$seleccion]);
        return response()->success($data);                             
    }
    public function TipoDocumento(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_DOCUMENTOS');
        return response()->success($data);                             
    }
    public function Modalidad(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_MODALIDAD_ADQUISICION');
        return response()->success($data);                             
    }
    public function DispositivoLegal(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DISPOSITIVO_LEGAL');
        return response()->success($data);                             
    }
    public function Documentos(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DOCUMENTOS');
        return response()->success($data);                             
    }
    public function Aporte(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_APORTES');
        return response()->success($data);                             
    }
    
    public function Instituciones(Request $request){          
        $nombre    = $request->valor; 
        $page     = $request->page;
        $records  = $request->records;
        $seleccion = $request->seleccion;      
        
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_INSTITUCIONES_FAVORECIDAS ?,?,?,?',[$nombre,$page,$records, $seleccion]);
        return response()->success($data);                             
    } 

    public function Obtener_Inst($codigo)
    {
         $data = DB::connection('sqlsrv_S_')->select('SELECT CODIGO_INSTITUCION,nombre_institucion FROM ENTIDADES WHERE CODIGO_INSTITUCION = ?',[$codigo]);
        return response()->success($data);
    }

    public function Oficina($coddepa){               
               
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_OFICINA_REGISTRAL ?',[$coddepa]);
        return response()->success($data);                             
    }
    public function Partida(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_PARTIDA_REGISTRAL');
        return response()->success($data);                             
    }
    public function DetalleApoEquiUrb($filtro){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DETALLE_DE_APORTE_EQUIPAMIENTO ?', [$filtro]);
        return response()->success($data);                             
    }
    //modal
    public function Lindero(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_LINDEROS_LEGALES');
        return response()->success($data);                             
    }
    public function NombreLindero($codlindero){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_OBTENER_NOMBRE_LINDEROS  ?',[$codlindero]);
        return response()->success($data);                             
    }
    public function Acto(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_ACTOS_DATOS_FABRICA');
        return response()->success($data);                             
    }
    public function DetalleActo($codacto){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DETALLE_DATOS_FABRICA ?',[$codacto]);
        return response()->success($data);                             
    }

    public function Moneda(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_MONEDA');
        return response()->success($data);                             
    }

    //LIMITACIONES
    public function Restricciones(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_RESTRICCIONES');
        return response()->success($data);                             
    }
    public function DetalleRestricciones($codrestriccion){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DETALLE_RESTRICCIONES ? ',[$codrestriccion]);
        return response()->success($data);                             
    }
    public function Materias(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_MATERIAS');
        return response()->success($data);                             
    }

    public function GetNombreMaterias($codmateria){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT DESCRIPCION_MATERIAS AS materia FROM
         TBL_MATERIAS WHERE ID_ESTADO = 1 and CODIGO_MATERIAS = $codmateria");
        return response()->success($data);                             
    }

    public function ListadoProcesosJudiciales($codigo_interno){               
    
        $data = DB::connection('sqlsrv_S_')->select('EXEC dbo.PA_SINABIP_LISTADO_PROCESOS_JUDICIALES ?', [$codigo_interno]);
        return response()->success($data);                             
    }

    public function antecedentes($codigo_interno, $codigo_detalle){               
    
        $data = DB::connection('sqlsrv_S_')->select('EXEC dbo.PA_SINABIP_LISTADO_ANTECEDENTES ?, ?', [$codigo_interno, $codigo_detalle]);
        return response()->success($data);                             
    }

    public function datosJudiciales($codigo_detalle){               
    
        $data = DB::connection('sqlsrv_S_')->select('EXEC dbo.PA_SINABIP_LISTADO_DATOS_JUDICIALES ?', [$codigo_detalle]);
        return response()->success($data);                             
    }

    public function sentencia($codigo_detalle){               
    
        $data = DB::connection('sqlsrv_S_')->select('EXEC dbo.PA_SINABIP_LISTADO_SENTENCIA_JUDICIAL ?', [$codigo_detalle]);
        return response()->success($data);                             
    }

    //ADMINISTRACION Y ADQUISICION
     public function ActoAd(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_ACTO');
        return response()->success($data);                             
    }
    public function DetalleActoAd($codacto){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DETALLE_ACTO ?',[$codacto]);
        return response()->success($data);                             
    }
    public function DetalleNombreActo($coddetalle){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_NOMBRE_DETALLE_ACTO ?',[$coddetalle]);
        return response()->success($data);                             
    }
    public function NombreDetalleDatosFabrica($coddetalle){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_NOMBRE_DETALLE_DATOS_FABRICA ?',[$coddetalle]);
        return response()->success($data);                             
    }
    public function NombreInstitucion($codinstitucion){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_NOMBRE_INSTITUCION  ?',[$codinstitucion]);
        return response()->success($data);                             
    }
    public function Generico(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_USO_GENERICO');
        return response()->success($data);                             
    }
    public function Especifico($codgenerico){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_USO_ESPECIFICO ?', [$codgenerico]);
        return response()->success($data);                             
    }
    public function DerechoInscrito(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DERECHO_INSCRITO');
        return response()->success($data);                             
    }
    public function Vigencia(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_VIGENCIA_ACTO');
        return response()->success($data);                             
    }
    public function ObtenerItem($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_CALCULAR_ITEM_ACTOS ?', [$codinterno]);
        return response()->success($data);                             
    }
    //DATOS TECNICOS
    public function ObPoseedor($codigo){               
          
        $data = DB::connection('sqlsrv_S_')->select('SELECT NOMBRE_INSTITUCION FROM ENTIDADES WHERE CODIGO_INSTITUCION = ?',[$codigo]); 
         return response()->success($data);                            
    }
    public function Zona(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPOZONA');
        return response()->success($data);                             
    }
    public function Terreno(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPOTERRENO');
        return response()->success($data);                             
    }
    public function Zonificacion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_ZONIFICACION');
        return response()->success($data);                             
    }
    public function Ocupacion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_NIVEL_OCUPACION');
        return response()->success($data);                             
    }
    //CONSTRUCCIONES
    public function EstadoHabilitacion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_ESTADO_HABILITACION');
        return response()->success($data);                             
    }
    public function SituacionFisica(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_SITUACION_FISICA');
        return response()->success($data);                             
    }
    public function MaterialConstruccion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_MATERIAL_PREDOMINANTE_CONSTRUCCION');
        return response()->success($data);                             
    }
    public function Combos(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DETALLE_POR_PISOS');
        return response()->success($data);                             
    }
    //OBRAS
    public function Valorizacion(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_VALORIZACION');
        return response()->success($data);                             
    }
    public function Materiales(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_MATERIAL');
        return response()->success($data);                             
    }
    public function Estado(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_ESTADO');
        return response()->success($data);                             
    }
    //ZONA PLAYA 
    public function Lam(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_LAM');
        return response()->success($data);                             
    }
    public function Playa(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_IZP');
        return response()->success($data);                             
    }
    public function Restringido(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_IZDR');
        return response()->success($data);                             
    }
    public function Privado(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_IZDP');
        return response()->success($data);                             
    }

    //LEGAJO DIGITAL
    public function ListadoTipoDocumentos(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_DOCUMENTO_DIGITALES');
        return response()->success($data);                             
    }

    public function Registrar_Aporte_Legajo_temporal(Request $request){ 

        $codigo_interno = 212521;
        $tipo_documento = 1;
        $cood_usuario = 1913;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_REGISTRO_APORTE_LEGAJO_TEMPORAL ?,?,?",[$codigo_interno, $tipo_documento, $cood_usuario]
        );
        
        return response()->success($data[0]);
    }

    public function SubirDocumentoLegajo(Request $request,$data){
        //dd($request->hasFile('file0'));
		$data = explode('-',$data);
        $COD_DOCUMENTO      = $data[0];
        $NOMBRE_DOCUMENTO	= $data[1];
         
        if ($request->hasFile('file0')) {
            $image = $request->file('file0');
           
			$ARCHIVO_EXTENSION = 'pdf';
			$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
			$FECHA_ARCHIVO				= date('Y').''.date('m').''.date('d');

            $name = "Asientos/legajostmp/";
            //$ARCHIVO_NOMBRE_GENERADO ='Inf_Final_Inv_'.$COD_DOCUMENTO.'_E'.$id.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
            $ARCHIVO_NOMBRE_GENERADO =$COD_DOCUMENTO.'_'.$NOMBRE_DOCUMENTO.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
            
            if(Config::get('app.APP_LINUX')){
				// file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
				// $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
				// $url = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
				// shell_exec('cp '.$origen.' '.$url);

                \Storage::disk('local')->put("public/".$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                $origen = storage_path('app/public')."/".$ARCHIVO_NOMBRE_GENERADO;
                $url = " ".$ARCHIVO_NOMBRE_GENERADO;  
                $final = '/mnt/srvinfowww/'.$name.$ARCHIVO_NOMBRE_GENERADO;
                shell_exec('mv '.$origen.' '.$final);

			}else{
                $url = Storage::disk('publicAporteDoc')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO);
			}

            $datos = array(
                'url' => $url
            );
            return response()->json($datos);
        }
	}
    
    public function VerPDF(Request $request,$archivo){
        $ruta_Doc = '';
        // dd($archivo);
        if(Config::get('app.APP_LINUX')) {
            $ruta_Doc = Config::get('app.ruta_documentosLIX');
            $name = $ruta_Doc . "Asientos/legajostmp/";
        }else{
            $ruta_Doc = Config::get('app.ruta_documentosWIN');
            $name = $ruta_Doc . "Asientos/legajostmp/";
        }
        $contents = $name.str_replace(" ", "", $archivo);
        // dd($contents);
		header("Content-type: application/pdf");
		header('Content-Disposition: inline; filename="'.$contents.'"');
		$data = file_get_contents($contents);
		
		echo $data;
		exit;
		
    }

    public function VisualizarPDF(Request $request,$archivo, $CUS){
        $ruta_Doc = '';
        $archivo = str_replace(" ", "", $archivo); 
        $existe = false;
        if(Config::get('app.APP_LINUX')) {
            $ruta_Doc = Config::get('app.ruta_documentosLIX'); 
            $name = $ruta_Doc . "Asientos/" . $CUS . "/";
            // dd($existe,$name.$archivo,file_exists($name.$archivo));
            if(!file_exists($name.$archivo)){
                $ruta_Doc = Config::get('app.ruta_documentosLIXProd');
                $name = $ruta_Doc . "Asientos/" . $CUS . "/";
                if(file_exists($name.$archivo)){
                	$existe = true;
                }
            }else{
            	$existe = true;
            }
        }else{
            $ruta_Doc = Config::get('app.ruta_documentosWIN');
            $name = $ruta_Doc . "Asientos/" . $CUS . "/";
            if(!file_exists($name.$archivo)){
                $ruta_Doc = Config::get('app.ruta_documentosWINProd');
                $name = $ruta_Doc . "Asientos/" . $CUS . "/";
            	if(file_exists($name.$archivo)){
                	$existe = true;
                }
            }else{
            	$existe = true;
            }
        }
        
        // dd($existe,$name.$archivo);
        // dd($contents);
        // dd(Config::get('app.APP_LINUX'),Config::get('app.ruta_documentosLIX'),Config::get('app.ruta_documentosLIXProd'));
        if($existe == false){
        	echo json_encode(array('existe'=>false,'descripcion'=> 'No se encuentra el archivo'));
        }else{
			header("Content-type: application/pdf");
	        header('Content-Disposition: inline; filename="'.$name.$archivo.'"');
	        $data = file_get_contents($name.$archivo);
			echo $data;
			exit;
        }
		
    }
    

    public function AceptarArchivoAdjunto(Request $request){
        
        $codigo_personal = $request->codigo_personal;
        $codigo_interno = $request->codigo_interno;
        $codigo_documento = $request->codigo_documento;
        $cood_usuario = $request->cood_usuario;
        $nombreArchivo_Origen = $request->nombreArchivo;
        $observaciones = $request->observaciones;


        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_REGISTRO_APORTE_LEGAJO_DEFINITIVO ?,?,?,?,?",[
                $codigo_personal,
                $codigo_interno, 
                $codigo_documento,
                $cood_usuario,
                $observaciones
            ]
        );
        

        $nombreArchivo_Destino = $data[0]->NOMBRE_ARCHIVO;
        $nombreCarpetaCUS = $data[0]->CUS;

        $carpeta_origen = "Asientos/legajostmp/";
        $carpeta_destino = "Asientos/" . $nombreCarpetaCUS . '/';

        /* VALIDAR EXISTENCIA DE CARPETA CUS */       

        if(Config::get('app.APP_LINUX')){
            $Filefinal = '/mnt/srvinfowww/'.$carpeta_destino;

            if(!file_exists($Filefinal)){
                mkdir($Filefinal, 0777,true);
            }

            $origen = '/mnt/srvinfowww/'.$carpeta_origen.$nombreArchivo_Origen;
            $final = $Filefinal.str_replace(" ", "", $nombreArchivo_Destino);
            shell_exec('mv '.$origen.' '.$final);

        }else{
            $exists = Storage::disk('publicAporteDoc')->exists('Asientos' . $nombreCarpetaCUS);
            //Storage::getMetadata('your-path')['type'] === 'dir' 
            if(!$exists){
                Storage::disk('publicAporteDoc')->makeDirectory('Asientos', $nombreCarpetaCUS, true);
            }
            $url = Storage::disk('publicAporteDoc')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO);
            Storage::disk('publicAporteDoc')->move($carpeta_origen.$nombreArchivo_Origen, $carpeta_destino.$nombreArchivo_Destino);
        }


        return response()->success([
            "resultado" => (count($data) > 0) ?$data : []
        ]);

    }

    public function EliminarArchivoAdjunto(Request $request){
        
        $codigo_personal = $request->codigo_personal;
        $codigo_interno = $request->codigo_interno;
        $codigo_documento = $request->codigo_documento;
        $cood_usuario = $request->cood_usuario;
        $nombreArchivo_Origen = $request->nombreArchivo;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_ELIMINACION_APORTE_DOCUMENTO ?,?,?,?",[
                $codigo_personal,
                $codigo_interno, 
                $codigo_documento,
                $cood_usuario
            ]
        );
        
        $nombreArchivo_Destino = $data[0]->NOMBRE_ARCHIVO;

        $name = "Asientos/legajostmp/";
        $nombreArchivo = $request->nombreArchivo;

        if(Config::get('app.APP_LINUX')){

            $Filefinal = '/mnt/srvinfowww/'.$name.str_replace(" ", "", $nombreArchivo);

            if(file_exists($Filefinal)){
                unlink($Filefinal);
            }

            $url = $nombreArchivo;
            // http://sinabip-t.sbn.gob.pe/VerPDF/Inf_Final_Inv_2_E1_20200721_a997.pdf

            // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            // $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
            // $url = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
            // shell_exec('cp '.$origen.' '.$url);
        }else{ 
            $url = Storage::disk('publicAporteDoc')->delete($name.$nombreArchivo);
        }
        
        
        $datos = array(
            'url' => $url
        );
        
        return response()->json($datos);
    }



    public function EliminarDocumentoAporte(Request $request){
        
        $codigo_interno = $request->codigo_interno;
        $item_documento = $request->item_documento;
        $codigo_personal = $request->codigo_personal;
        $cood_usuario = $request->cood_usuario;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_ELIMINACION_APORTE_DOCUMENTO ?,?,?,?",[
                $codigo_interno, 
                $item_documento,
                $codigo_personal,
                $cood_usuario
            ]
        );
        
       
        $nombreArchivo_a_Eliminar = $data[0]->NOMBRE_ARCHIVO;
        $CUS_a_Eliminar = $data[0]->CUS;
        $name = "Asientos/" . $CUS_a_Eliminar. '/';

        if(Config::get('app.APP_LINUX')){

            $Filefinal = '/mnt/srvinfowww/'.$name.str_replace(" ", "", $nombreArchivo_a_Eliminar);
            if(file_exists($Filefinal)){
                unlink($Filefinal);
            }
            $url =$nombreArchivo_a_Eliminar;

        }else{ 
            $url = Storage::disk('publicAporteDoc')->delete($name.$nombreArchivo_a_Eliminar);
        }
        
        return response()->success([
            "resultado" => (count($data) > 0) ?$data : [],
            "imagen" => $url
        ]);
    }


    public function ListadoAporteDocumentos(Request $request){ 

        $codigo_interno = $request->codigo_interno;
        $page = $request->page;
        $records = $request->records;


        $data = DB::connection('sqlsrv_S_')->select( 
            "exec dbo.PA_SINABIP_LISTADO_APORTE_DOCUMENTOS ?,?,?",[$codigo_interno, $page, $records]
        );
        
        return response()->success([
            "listadoAportes" => (count($data) > 0) ?$data : []
        ]);
    }

    public function datosSituacionCatastral(Request $request){ 

        $codigo_interno = $request->codigo_interno;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_DATOS_SITUACION_CATASTRAL ?",[$codigo_interno]
        );
        
        return response()->success([
            "datosCatastral" => (count($data) > 0) ?$data : []
        ]);
    }

    public function GuardarObservaciones(Request $request){ 

        $codigo_interno = $request->codigo_interno;
        $observaciones = $request->observaciones;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_GUARDAR_OBSERVACIONES_LEGALES ?, ?",[$codigo_interno, $observaciones]
        );
        
        return response()->success([
            "resultado" => (count($data) > 0) ?$data : []
        ]);
    }


    public function ActoIA(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_ACTO_INDEPENDIZACION');
        return response()->success($data);                             
    }

    public function TipoRegistro(){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_TIPO_REGISTRO_INDEPENDIZACION');
        return response()->success($data);                             
    }
       

}

