<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;
use Pdf_Resumen_Firma;
use Pdf_cabecera_caratula;


class ControlRegistroController extends Controller
{
   
    public function Busqueda_Cus_registrados_internos(Request $request){  
      
        $ruc            = $request->rucs;
        $cus            = $request->cus;
        $departamento   = $request->coddepa;
        $provincia      = $request->codprov;
        $distrito       = $request->coddist;
        $tipoSeleccion  = $request->tipoSeleccion;
        $fechaInicio    = $request->fechaInicio;
        $fechaFinal     = $request->fechaFinal;
        $page           = $request->page;
        $records        = $request->records;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_LISTADO_CONTROL_REGISTRO_PREDIOS_INTERNOS ?,?,?,?,?,?,?,?,?,?",[
                $ruc,
                $cus,
                $departamento,
                $provincia,
                $distrito,
                $tipoSeleccion,
                $fechaInicio,
                $fechaFinal,
                $page,
                $records
            ]
        );
        
        return response()->success([
            "listadoCusRegistradosInternos" => (count($data) > 0) ?$data : [],
            "listadoCusRegistradosOK" => [],
            "tipoSeleccion" => $tipoSeleccion
        ]);
    }

    public function Busqueda_Cus_registrados_externos(Request $request){  
      
        $ruc            = $request->rucs;
        $cus            = $request->cus;
        $departamento   = $request->coddepa;
        $provincia      = $request->codprov;
        $distrito       = $request->coddist;
        $page           = $request->page;
        $records        = $request->records;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_LISTADO_CONTROL_REGISTRO_PREDIOS_EXTERNOS ?,?,?,?,?,?,?",[
                $ruc,
                $cus,
                $departamento,
                $provincia,
                $distrito,
                $page,
                $records
            ]
        );
        
        return response()->success([
            "listadoCusRegistradosExternos" => (count($data) > 0) ?$data : []
        ]);
    }


    public function BuscarCodigoPredio($nroCUS){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT CODIGO_INTERNO[aux_codinterno] FROM LIBROS WHERE NRO_RSINABIP = $nroCUS");
        return response()->success($data);                
    }
    

    public function Aprobar_cus(Request $request){  
      
        $codigo_interno = $request->codigo_interno;
        $tipoRegistro   = $request->tipoRegistro;
        $codigo_usuario = $request->codigo_usuario;
        $observaciones  = $request->observaciones;
        $aprobado       = $request->aprobado;
        $idControlCalidad = $request->idControlCalidad;
        $tipoCus = $request->tipoCus;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_APROBACION_CUS_UD ?,?,?,?,?,?,?",[
                $codigo_interno,
                $tipoRegistro,
                $codigo_usuario,
                $observaciones,
                $aprobado,  
                $idControlCalidad,
                $tipoCus
            ]
        );
        
        return response()->success([
            "resultadoAprobacion" => (count($data) > 0) ?$data : []
        ]);
    }

    public function UpdateNotificacion($codinterno,$cod_user,$observacion, $idControlCalidad){                   
        /*UPDATE DE NOTIFICACIONES Y UNIDAD DOCUMENTAL*/
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_UPDATE_NOTIFICACIONES ?,?,?,?', [$codinterno,$cod_user,$observacion, $idControlCalidad]);
        /*INSERTAR OBSERVACIONES - CABECERA*/
        $data_campos= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
            ['2', $codinterno, $idControlCalidad]
        );

        $item = 1;
        $valor = DB::connection('sqlsrv_S_')->select("SELECT TOP 1 item FROM TBL_OBSERVACIONES_NOTIFICADAS WHERE ID_NOTIFICACION_APROB = $idControlCalidad ORDER BY item DESC");         
        if ($valor==[])
        {
            $item = 1;
        }
        else 
        {
            $item = $valor[0]->item + 1;
        }

        if (isset($data_campos[0])){
            $NOMBRE_ESTANDARIZADO           = $data_campos[0]->NOMBRE_ESTANDARIZADO;
            $AREA_REGISTRAL_VACIO           = $data_campos[0]->AREA_REGISTRAL_VACIO;
            $CODIGO_COMPETENCIA_VACIO       = $data_campos[0]->CODIGO_COMPETENCIA_VACIO;
            $DESCRIPCION_TIPODETALLE_VACIO  = $data_campos[0]->DESCRIPCION_TIPODETALLE_VACIO;
            $CODIGO_USO_ESPECIFICO_VACIO    = $data_campos[0]->CODIGO_USO_ESPECIFICO_VACIO;
            $AREA_DESOCUPADA_VACIO          = $data_campos[0]->AREA_DESOCUPADA_VACIO;   
            $FECHA_APORTE_VACIO             = $data_campos[0]->FECHA_APORTE_VACIO;            
        

            $aux_data_cab = DB::connection('sqlsrv_S_')->select('exec PA_SINABIP_OBSERVACIONES_NOTIFICADAS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?',[$idControlCalidad, $codinterno, $item,'PROPIETARIO DEL CUS',$NOMBRE_ESTANDARIZADO,'AREA REGISTRAL',$AREA_REGISTRAL_VACIO,'COMPETENCIA',$CODIGO_COMPETENCIA_VACIO,'DETALLE DE APORTE O EQUIPAMIENTO',$DESCRIPCION_TIPODETALLE_VACIO,'USO ESPECIFICO',$CODIGO_USO_ESPECIFICO_VACIO,'AREA DESOCUPADA',$AREA_DESOCUPADA_VACIO,'APORTE GRAFICO', $FECHA_APORTE_VACIO,'','','','','','','','C']);
        }
        
       /*INSERTAR DOCUMENTOS NO APORTADOS*/
        $data_campos_aux= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
            ['3', $codinterno, $idControlCalidad]
        );

         if (isset($data_campos_aux[0])){
                foreach ($data_campos_aux as $key => $value) {
                    $ROW_NUMBER_ID          = $value->ROW_NUMBER_ID;
                    $CODIGO_TIPO_DOCUMENTO  = $value->CODIGO_TIPO_DOCUMENTO;
                    $DESCRIPCIONTIPO        = $value->DESCRIPCIONTIPO;
                    
                    $aux_data_cuerpo = DB::connection('sqlsrv_S_')->select('exec PA_SINABIP_OBSERVACIONES_NOTIFICADAS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?',[$idControlCalidad, $codinterno,$item,'ITEM',$ROW_NUMBER_ID,'CODIGO',$CODIGO_TIPO_DOCUMENTO,'DESCRIPCION',$DESCRIPCIONTIPO,'','','','','','','','','','','','','','','','D']);
                }
            }


        return response()->success($aux_data_cuerpo); 


    }

 
    public function Verificar_cusCheck($cod_interno, $id_control_calidad, $pestana){
        
        /* CABECERAS */
        $datosCUS= DB::connection('sqlsrv_S_')->select( 
            "exec PA_SINABIP_VERIFICADOR_OBSERVACIONES_CONTROL_REGISTRO ?,?,?,?,?",  
            [$id_control_calidad, $cod_interno, 1, '100', $pestana]    
        );


        return response()->success([
            "resultado" => (count($datosCUS) > 0) ?$datosCUS : [],
            "resultadoCabecera" => [],
            "resultadoDetalles" => []
        ]);

    }

    public function Verificar_cus(Request $request, $cod_interno, $id_control_calidad){
        header('Content-type: application/pdf');

        /* CABECERAS */
        $data_campos= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
            ['1', $cod_interno, $id_control_calidad]
        );

        $NOMBRE_ESTANDARIZADO	    = $data_campos[0]->NOMBRE_ESTANDARIZADO;
        $CUS		                = $data_campos[0]->CUS;
        $DENOMINACION_INMUEBLE      = $data_campos[0]->DENOMINACION_INMUEBLE;


        /* CAMPOS VACIOS */
        $data_campos= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
            ['2', $cod_interno, $id_control_calidad]
        );

        if (isset($data_campos[0])){
            $NOMBRE_ESTANDARIZADO           = $data_campos[0]->NOMBRE_ESTANDARIZADO;
            $AREA_REGISTRAL_VACIO           = $data_campos[0]->AREA_REGISTRAL_VACIO;
            $CODIGO_COMPETENCIA_VACIO       = $data_campos[0]->CODIGO_COMPETENCIA_VACIO;
            $DESCRIPCION_TIPODETALLE_VACIO  = $data_campos[0]->DESCRIPCION_TIPODETALLE_VACIO;
            $CODIGO_USO_ESPECIFICO_VACIO    = $data_campos[0]->CODIGO_USO_ESPECIFICO_VACIO;
            $AREA_DESOCUPADA_VACIO          = $data_campos[0]->AREA_DESOCUPADA_VACIO;
            $FECHA_APORTE_VACIO             = $data_campos[0]->FECHA_APORTE_VACIO;
            $contadoErrores = 0;
        }


        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8, $NOMBRE_ESTANDARIZADO,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('INFORMACION DE CUS NRO: ' . $CUS),1,0,'C',true);  		
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(50,6,utf8_decode('DENOMINACION DEL PREDIO: '),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(135,6,$DENOMINACION_INMUEBLE,1,0,'L',true);	
            $pdf->Ln();
            $pdf->Ln(2);
            
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('CAMPOS NO INGRESADOS'),1,0,'L',true); 
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 65, 110);
            $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CAMPO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('OBSERVACION'),1,0,'C',true);
            $pdf->Ln();

            

            /* VALIDACION DE CAMPOS VACIOS */
            if (isset($data_campos[0])){
                if($NOMBRE_ESTANDARIZADO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1; 
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('PROPIETARIO DE CUS'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($NOMBRE_ESTANDARIZADO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($AREA_REGISTRAL_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('AREA_REGISTRAL'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($AREA_REGISTRAL_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($CODIGO_COMPETENCIA_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('COMPETENCIA'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($CODIGO_COMPETENCIA_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($DESCRIPCION_TIPODETALLE_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('DETALLE DE APORTE O EQUIPAMIENTO'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($DESCRIPCION_TIPODETALLE_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($CODIGO_USO_ESPECIFICO_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('USO ESPECIFICO'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($CODIGO_USO_ESPECIFICO_VACIO),1,0,'C',true); 
                    $pdf->Ln();
                }
                if($AREA_DESOCUPADA_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('AREA DESOCUPADA'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($AREA_DESOCUPADA_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($FECHA_APORTE_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('APORTE GRAFICO'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($FECHA_APORTE_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                $pdf->Ln(3);
            }else{
                $pdf->SetFillColor(255,255,255);
                $pdf->Cell(185,5,utf8_decode('NO SE ENCONTRARON CAMPOS VACIOS'),1,0,'C',true);
                $pdf->Ln();
            }
            
            
           
            /* VALIDACION DE DOCUMENTOS APORTADOS */
            $data_documentos = DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
                ['3', $cod_interno, $id_control_calidad]
            );


            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DOCUMENTOS NO APORTADOS'),1,0,'L',true); 
            $pdf->Ln();

            $pdf->SetFont('Arial','B',8);
            $w = array(10, 20, 100, 55);
            $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CODIGO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('DESCRIPCION DE DOCUMENTO'),1,0,'C',true);
            $pdf->Cell($w[3],5,utf8_decode('OBSERVACION'),1,0,'C',true);
            $pdf->Ln();


            if (isset($data_documentos[0])){
                foreach ($data_documentos as $key => $value) {
                    $ROW_NUMBER_ID          = $value->ROW_NUMBER_ID;
                    $CODIGO_TIPO_DOCUMENTO	= $value->CODIGO_TIPO_DOCUMENTO;
                    $DESCRIPCIONTIPO	    = $value->DESCRIPCIONTIPO;
                    //$TOT_BIEN_BAJA		 = number_format($value->TOT_BIEN_BAJA,0);
                    
                    $pdf->Cell($w[0],4,$ROW_NUMBER_ID,'LR',0,'L');
                    $pdf->Cell($w[1],4,utf8_decode($CODIGO_TIPO_DOCUMENTO),'LR',0,'L');
                    $pdf->Cell($w[2],4,$DESCRIPCIONTIPO,'LR',0,'L');
                    $pdf->Cell($w[3],4,'NO APORTADO','LR',0,'R');
                    $pdf->Ln();
                    //$fill = !$fill;
                }
            }

            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
    }

    public function Verificar_historico_old($cod_interno, $idControlCalidad){
        header('Content-type: application/pdf');

      
        /* CAMPOS VACIOS */
        $data_campos= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_HISTORICO_OBSERVACIONES ?,?,?",
            ['C', $cod_interno, $idControlCalidad]
        );

        if (isset($data_campos[0])){
            $NOMBRE_ESTANDARIZADO           = $data_campos[0]->NOMBRE_ESTANDARIZADO;
            $AREA_REGISTRAL_VACIO           = $data_campos[0]->AREA_REGISTRAL_VACIO;
            $CODIGO_COMPETENCIA_VACIO       = $data_campos[0]->CODIGO_COMPETENCIA_VACIO;
            $DESCRIPCION_TIPODETALLE_VACIO  = $data_campos[0]->DESCRIPCION_TIPODETALLE_VACIO;
            $CODIGO_USO_ESPECIFICO_VACIO    = $data_campos[0]->CODIGO_USO_ESPECIFICO_VACIO;
            $AREA_DESOCUPADA_VACIO          = $data_campos[0]->AREA_DESOCUPADA_VACIO;
            $FECHA_APORTE_VACIO             = $data_campos[0]->FECHA_APORTE_VACIO;
            $contadoErrores = 0;            
        }


        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            /*$pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);        
            $pdf->SetFillColor(255,255,255);    
            $pdf->Cell(140,8, $NOMBRE_ESTANDARIZADO,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            $pdf->SetFillColor(205,205,205);    
            $pdf->Cell(185,7,utf8_decode('INFORMACION DE CUS NRO: ' . $CUS),1,0,'C',true);          
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);    
            $pdf->Cell(50,6,utf8_decode('DENOMINACION DEL PREDIO: '),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);        
            $pdf->Cell(135,6,$DENOMINACION_INMUEBLE,1,0,'L',true);  
            $pdf->Ln();
            $pdf->Ln(2);*/
            
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);    
            $pdf->Cell(185,7,utf8_decode('CAMPOS NO INGRESADOS'),1,0,'L',true); 
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 65, 110);
            $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CAMPO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('OBSERVACION'),1,0,'C',true);
            $pdf->Ln();

            

            /* VALIDACION DE CAMPOS VACIOS */
            if (isset($data_campos[0])){
                if($NOMBRE_ESTANDARIZADO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1; 
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('PROPIETARIO DE CUS'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($NOMBRE_ESTANDARIZADO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($AREA_REGISTRAL_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('AREA_REGISTRAL'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($AREA_REGISTRAL_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($CODIGO_COMPETENCIA_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('COMPETENCIA'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($CODIGO_COMPETENCIA_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($DESCRIPCION_TIPODETALLE_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('DETALLE DE APORTE O EQUIPAMIENTO'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($DESCRIPCION_TIPODETALLE_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($CODIGO_USO_ESPECIFICO_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('USO ESPECIFICO'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($CODIGO_USO_ESPECIFICO_VACIO),1,0,'C',true); 
                    $pdf->Ln();
                }
                if($AREA_DESOCUPADA_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('AREA DESOCUPADA'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($AREA_DESOCUPADA_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                if($FECHA_APORTE_VACIO == 'NO ESPECIFICADO'){
                    $contadoErrores += 1;
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell($w[0],5,utf8_decode($contadoErrores),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('APORTE GRAFICO'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode($FECHA_APORTE_VACIO),1,0,'C',true);
                    $pdf->Ln();
                }
                $pdf->Ln(3);
            }else{
                $pdf->SetFillColor(255,255,255);
                $pdf->Cell(185,5,utf8_decode('NO SE ENCONTRARON CAMPOS VACIOS'),1,0,'C',true);
                $pdf->Ln();
            }
            
            
           
            /* VALIDACION DE DOCUMENTOS APORTADOS */
            $data_documentos = DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_HISTORICO_OBSERVACIONES ?,?,?",
                ['D', $cod_interno, $idControlCalidad]
            );


            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);    
            $pdf->Cell(185,7,utf8_decode('DOCUMENTOS NO APORTADOS'),1,0,'L',true); 
            $pdf->Ln();

            $pdf->SetFont('Arial','B',8);
            $w = array(10, 20, 100, 55);
            $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CODIGO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('DESCRIPCION DE DOCUMENTO'),1,0,'C',true);
            $pdf->Cell($w[3],5,utf8_decode('OBSERVACION'),1,0,'C',true);
            $pdf->Ln();


            if (isset($data_documentos[0])){
                foreach ($data_documentos as $key => $value) {
                    $ROW_NUMBER_ID          = $value->ROW_NUMBER_ID;
                    $CODIGO_TIPO_DOCUMENTO  = $value->CODIGO_TIPO_DOCUMENTO;
                    $DESCRIPCIONTIPO        = $value->DESCRIPCIONTIPO;
                    //$TOT_BIEN_BAJA         = number_format($value->TOT_BIEN_BAJA,0);
                    
                    $pdf->Cell($w[0],4,$ROW_NUMBER_ID,'LR',0,'L');
                    $pdf->Cell($w[1],4,utf8_decode($CODIGO_TIPO_DOCUMENTO),'LR',0,'L');
                    $pdf->Cell($w[2],4,$DESCRIPCIONTIPO,'LR',0,'L');
                    $pdf->Cell($w[3],4,'NO APORTADO','LR',0,'R');
                    $pdf->Ln();
                    //$fill = !$fill;
                }
            }

            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
    }

    
    public function Verificar_historico($idControlCalidad){
        header('Content-type: application/pdf');
        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);    
            $pdf->Cell(185,7,utf8_decode('CAMPOS NO INGRESADOS'),1,0,'L',true); 
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 65, 110);
            $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CAMPO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('OBSERVACION'),1,0,'C',true);
            $pdf->Ln();


            /* CABECERA */
            $data_cabecera= DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_HISTORICO_OBSERVACIONES_NOTIFICACIONES ?,?",
                ['C', $idControlCalidad]
            );


            if (isset($data_cabecera[0])){
                foreach ($data_cabecera as $key => $value) {
                    $ROW_NUMBER_ID          = $value->ROW_NUMBER_ID;
                    $NOMBRE_OBSERVACION     = $value->NOMBRE_OBSERVACION;
                    $COMENTARIO_OBSERVACION = $value->COMENTARIO_OBSERVACION;
                    
                    $pdf->Cell($w[0],4,$ROW_NUMBER_ID,'LR',0,'C');
                    $pdf->Cell($w[1],4,utf8_decode($NOMBRE_OBSERVACION),'LR',0,'C');
                    $pdf->Cell($w[2],4,trim($COMENTARIO_OBSERVACION),'LR',0,'C');
                    $pdf->Ln();
                    //$fill = !$fill;
                }
            }else{
                $pdf->SetFillColor(255,255,255);
                $pdf->Cell(185,5,utf8_decode('NO SE ENCONTRARON CAMPOS VACIOS'),1,0,'C',true);
                $pdf->Ln();
            }
            $pdf->Cell(array_sum($w),0,'','T');
            $pdf->Ln();
            $pdf->Ln(3);

          
        
            /* VALIDACION DE DOCUMENTOS APORTADOS */
            $data_documentos = DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_HISTORICO_OBSERVACIONES_NOTIFICACIONES ?,?",
                ['D', $idControlCalidad]
            );


            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);    
            $pdf->Cell(185,7,utf8_decode('DOCUMENTOS NO APORTADOS'),1,0,'L',true); 
            $pdf->Ln();

            $pdf->SetFont('Arial','B',8);
            $w = array(10, 20, 100, 55);
            $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CODIGO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('DESCRIPCION DE DOCUMENTO'),1,0,'C',true);
            $pdf->Cell($w[3],5,utf8_decode('OBSERVACION'),1,0,'C',true);
            $pdf->Ln();


            if (isset($data_documentos[0])){
                foreach ($data_documentos as $key => $value) {
                    $ITEM               = $value->ITEM;
                    $CODIGO_OBSERVACION = $value->CODIGO_OBSERVACION;
                    $NOMBRE_OBSERVACION    = $value->NOMBRE_OBSERVACION;
                    //$TOT_BIEN_BAJA         = number_format($value->TOT_BIEN_BAJA,0);
                    
                    $pdf->Cell($w[0],4,$ITEM,'LR',0,'C');
                    $pdf->Cell($w[1],4,utf8_decode($CODIGO_OBSERVACION),'LR',0,'L');
                    $pdf->Cell($w[2],4,$NOMBRE_OBSERVACION,'LR',0,'L');
                    $pdf->Cell($w[3],4,'NO APORTADO','LR',0,'C');
                    $pdf->Ln();
                    //$fill = !$fill;
                }
            }else{
                $pdf->SetFillColor(255,255,255);
                $pdf->Cell(185,5,utf8_decode('NO SE ENCONTRARON CAMPOS VACIOS'),1,0,'C',true);
                $pdf->Ln();
            }

            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
    }


    function validar($valor){
        if ($valor){
            return;
        }
    }

    public function Imprimir_caratula(Request $request, $numero){
        header('Content-type: application/pdf');

        //87940
        /* VERIFICAR EXISTENCIA DE FOTOS */
        $data = DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_RUTA_FOTOS_MOSTRAR_CARATULA ?",[$numero]
        );
        
        $ruta = $data[0]->RUTA;
        $nombre_foto = $data[0]->NOMBRE_FOTO;
        $ruta = str_replace('\\', '/', $ruta);
        $dir = str_replace('//srv-info/documentos$/','/mnt/srvinfo_doc/',$ruta);
        
        $filenamefirst = '';
       

        
        $dir2 = substr($dir, 0, strlen($dir)-1);
        $scanfile = scandir($dir2);
        
        foreach($scanfile as $file) {
            //if (!is_dir("c:/imagenes/$file")) {
            if (!is_dir($dir . $file)) { 
                $descompuesto = explode('_',$file);
                
                $descompuesto[0] = $descompuesto[0] . '_';
               
                if ($nombre_foto == $descompuesto[0]){
                    $filenamefirst = $file;
                    break;
                }
            }
        }
        
        /* CABECERAS */
        $data_campos= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_IMPRESION_CARATULA ?,?",
            [      
                'P',
                $numero
            ]
        );

        /* EXPEDIENTES RELACIONADOS */
        $data_expedientes= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_IMPRESION_CARATULA ?,?",
            [   
                'EX',
                $numero
            ]
        );

        $CODIGO_EXPEDIENTE = '';
        if (isset($data_expedientes[0])){
            foreach ($data_expedientes as $key => $value) {
                $CODIGO_EXPEDIENTE = $CODIGO_EXPEDIENTE . $value->CODIGO_EXPEDIENTE . '  ';
            }
        }

        /* REGISTROS RELACIONADOS */
        $data_registros= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_IMPRESION_CARATULA ?,?",
            [   
                'RR',
                $numero
            ]
        );

        $REGISTRO_RELACIONADO = '';
        if (isset($data_registros[0])){
            foreach ($data_registros as $key => $value) {
                $REGISTRO_RELACIONADO = $REGISTRO_RELACIONADO . $value->REGISTRO_RELACIONADO . '  ';
            }
        }
        
        $NOMBRE_ESTANDARIZADO	    = $data_campos[0]->NRO_RSINABIP;
        $DENOMINACION_INMUEBLE		= $data_campos[0]->DENOMINACION_INMUEBLE;
        $TIPO_VIA		            = ($data_campos[0]->TIPO_VIA == "0"?"":$data_campos[0]->TIPO_VIA);
        $TIPO_HABILITACION		    = ($data_campos[0]->TIPO_HABILITACION == "0"?"":$data_campos[0]->TIPO_HABILITACION);
        $NMB_URBANIZACION		    = ($data_campos[0]->NMB_URBANIZACION == "0"?"":$data_campos[0]->NMB_URBANIZACION);
        

        $DIRECCION_INMUEBLE         = $data_campos[0]->DIRECCION_INMUEBLE;
        $NRO_INMUEBLE               = $data_campos[0]->NRO_INMUEBLE;
        $PISO_UBICACION             = $data_campos[0]->PISO_UBICACION;
        $NRO_MANZANA                = $data_campos[0]->NRO_MANZANA;
        $NRO_LOTE                   = $data_campos[0]->NRO_LOTE;
        $DESCRIPCION_HABILITACION   = $data_campos[0]->DESCRIPCION_HABILITACION;
        $DSC_SECTOR                 = $data_campos[0]->DSC_SECTOR;
        $DEPARTAMENTO               = $data_campos[0]->DEPARTAMENTO;
        $PROVINCIA                  = $data_campos[0]->PROVINCIA;
        $DISTRITO                   = $data_campos[0]->DISTRITO;
        $RESTRICCIONES              = $data_campos[0]->RESTRICCIONES;
        $DETALLE_RESTRICCIONES      = $data_campos[0]->DETALLE_RESTRICCIONES;
        $DSC_SEDE_REGISTRAL         = $data_campos[0]->DSC_SEDE_REGISTRAL;
        $OFICINA_REGISTRAL          = $data_campos[0]->OFICINA_REGISTRAL;
        $AREA_REGISTRAL             = $data_campos[0]->AREA_REGISTRAL;
        $FEC_REGISTRAL              = $data_campos[0]->FEC_REGISTRAL;
        $TOMO                       = $data_campos[0]->TOMO;
        $FOJAS                      = $data_campos[0]->FOJAS;
        $ASIENTO                    = $data_campos[0]->ASIENTO;
        $FICHA                      = $data_campos[0]->FICHA;
        $CODIGO_PREDIO              = $data_campos[0]->CODIGO_PREDIO;
        $PARTIDA_ELECTRONICA        = $data_campos[0]->PARTIDA_ELECTRONICA;
        $NOMBRE_INSTITUCION         = $data_campos[0]->NOMBRE_INSTITUCION;
        $DERECHO_INSCRITO           = $data_campos[0]->DERECHO_INSCRITO;
        $OBSERVACIONES              = $data_campos[0]->OBSERVACIONES;
        $CONDICION                  = $data_campos[0]->CONDICION;
        $CONDICION_DSC              = $data_campos[0]->CONDICION_DSC;
        
        
        
        if($DSC_SEDE_REGISTRAL == NULL || $DSC_SEDE_REGISTRAL == ''){
            $OFICINA_REGISTRAL = $OFICINA_REGISTRAL;
        }else{
            $OFICINA_REGISTRAL = $DSC_SEDE_REGISTRAL;
        }
        
         $pdf = new Pdf_cabecera_caratula;
    
         $pdf->AliasNbPages();        

            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    

            $pdf->SetFillColor(205,205,205);//color de fondo tabla  
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',8);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(90,8,'DATOS GENERALES',1,0,'C',true);		
            $pdf->SetFillColor(255,255,255);

            $pdf->SetFont('Arial','B',12);
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(95,8,'Nro CUS: '. $numero . '   ESTADO: ' . $CONDICION_DSC,1,0,'R',true);		
            $pdf->SetFillColor(255,255,255);	        
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(90,7,utf8_decode($DENOMINACION_INMUEBLE),1,0,'L',true);  		
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->MultiCell(90,6,utf8_decode('DIRECCION: ' . $TIPO_VIA . ' ' . $DIRECCION_INMUEBLE), 1,'L',true);
            //$pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(30,6,utf8_decode('NUMERO: ' . $NRO_INMUEBLE),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(20,6,utf8_decode('PISO: ' . $PISO_UBICACION),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(20,6,utf8_decode('MZ: ' . $NRO_MANZANA),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(20,6,utf8_decode('LTE: ' . $NRO_LOTE),1,0,'L',true);
            $pdf->Ln();

            $x=$pdf->GetX();
            $y=$pdf->GetY();
            $pdf->SetXY($x,$y);
            $pdf->MultiCell(90,6, utf8_decode('HABILITAC.: ' . $TIPO_HABILITACION  . ' ' . $NMB_URBANIZACION),1,'L');
            
            //$pdf->Ln(1);

            // $pdf->SetFillColor(255,255,255);	
            // $pdf->Cell(90,6,utf8_decode('HABILITAC.: ' . $TIPO_HABILITACION  . ' ' . $NMB_URBANIZACION),1,0,'L',true);
            // $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(90,6,utf8_decode('SECTOR: ' . $DSC_SECTOR),1,0,'L',true);
            $pdf->Ln();
            
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(90,6,utf8_decode('DEPARTAMENTO: ' . $DEPARTAMENTO),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(90,6,utf8_decode('PROVINCIA: ' . $PROVINCIA),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(90,6,utf8_decode('DISTRITO: ' . $DISTRITO),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(90,6,utf8_decode('CONDICION: ' . $RESTRICCIONES),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            //$pdf->Cell(90,6,utf8_decode('DETALLE: ' . $DETALLE_RESTRICCIONES),1,0,'L',true);
            $pdf->MultiCell(90,6,utf8_decode('DETALLE: ' . $DETALLE_RESTRICCIONES), 1,'L',true);
            //$pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            //$pdf->Cell(185,6,utf8_decode('EXPEDIENTES: ' . $CODIGO_EXPEDIENTE),1,0,'L',true);
            $pdf->MultiCell(185,6,utf8_decode('EXPEDIENTES: ' . $CODIGO_EXPEDIENTE), 1,'L',true);
            //$pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            //$pdf->Cell(185,6,utf8_decode('REGISTROS: ' . $REGISTRO_RELACIONADO),1,0,'L',true);
            $pdf->MultiCell(185,6,utf8_decode('REGISTROS: ' . $REGISTRO_RELACIONADO), 1,'L',true);

        
            
            if($filenamefirst != ''){
                $pdf->Image($dir . $filenamefirst,102,33,90,60, 'jpg'); 
            }
            //$pdf->Image('//192.168.5.59/documentos$/AsientosPrediales/01A101_1.jpg',102,33,90,60, 'jpg');

            $pdf->Ln(2);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(50,8,'DATOS REGISTRALES',1,0,'C',true);		
            $pdf->SetFillColor(255,255,255);	        
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(185,7,utf8_decode($OFICINA_REGISTRAL),1,0,'L',true);  		
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('AREA (M2): ' . $AREA_REGISTRAL),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('FECHA: ' . $FEC_REGISTRAL),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode(''),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('TOMO: ' . $TOMO),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('ASIENTO: ' . $ASIENTO),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('CODIGO DE PREDIO: ' . $CODIGO_PREDIO),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('FOJAS: ' . $FOJAS),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('FICHA: ' . $FICHA),1,0,'L',true); 
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('PARTIDA ELECTRONICA: ' . $PARTIDA_ELECTRONICA),1,0,'L',true);
            $pdf->Ln();
            $pdf->Ln(2); 
            
           
            /* VALIDACION DE DOCUMENTOS APORTADOS */
            $data_documentos = DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_IMPRESION_CARATULA ?,?",
                [   
                    'L',
                    $numero
                ]
            );


            $pdf->SetFont('Arial','B',8);
            $w = array(30, 80, 25, 25, 25);
            $pdf->Cell($w[0],5,utf8_decode('LINDEROS'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('COLINDANCIA'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('TRAMOS'),1,0,'C',true);
            $pdf->Cell($w[3],5,utf8_decode('LADOS (ml)'),1,0,'C',true);
            $pdf->Cell($w[4],5,utf8_decode('NRO MUNIC'),1,0,'C',true);
            $pdf->Ln();


            //$pdf->Line(10, 200, 10, 150);
            
            if (isset($data_documentos[0])){
                //$linea = 153;
                $linea=$pdf->GetY();
                //dd($ye);
                $pdf->Line(11, $linea, 195, $linea);
                foreach ($data_documentos as $key => $value) {
                    $DESCRIPCION_LINDEROS = $value->DESCRIPCION_LINDEROS;
                    $DESCRIPCION	= $value->DESCRIPCION;
                    $TRAMOS	        = $value->TRAMOS;
                    $LADOS	        = $value->LADOS;
                    $NRO_MUNICIPAL	= $value->NRO_MUNICIPAL;
                    
                    $x=$pdf->GetX();
                    $y=$pdf->GetY();
                    //$pdf->SetXY($x,$y);
                    $pdf->MultiCell($w[0],9, utf8_decode($DESCRIPCION_LINDEROS),0,'L', false);
                    $pdf->SetXY($x+$w[0],$y);
                    //$pdf->Cell($w[0],4,$DESCRIPCION_LINDEROS,'LR',1,'L');
                   
                    $pdf->SetFont('Arial','B',7);
                    $x=$pdf->GetX();
                    $y=$pdf->GetY();
                    $pdf->MultiCell($w[1],3, utf8_decode($DESCRIPCION),0,'L', false);
                    $pdf->SetXY($x+$w[1],$y);
                    //$pdf->Cell($w[1],4,utf8_decode($DESCRIPCION),'LR',0,'L');

                    $pdf->SetFont('Arial','B',8);
                    $x=$pdf->GetX();
                    $y=$pdf->GetY();
                    //$pdf->SetXY($x,$y);
                    $pdf->MultiCell($w[2],9, utf8_decode($TRAMOS),0,'R');
                    $pdf->SetXY($x+$w[2],$y);
                    //$pdf->Cell($w[2],4,$TRAMOS,'LR',1,'R');

                    $x=$pdf->GetX();
                    $y=$pdf->GetY();
                    //$pdf->SetXY($x,$y);
                    $pdf->MultiCell($w[3],9, utf8_decode($LADOS),0,'R');
                    $pdf->SetXY($x+$w[3],$y);
                    //$pdf->Cell($w[3],4,$LADOS,'LR',0,'R');

                    $x=$pdf->GetX();
                    $y=$pdf->GetY();
                    //$pdf->SetXY($x,$y);
                    $pdf->MultiCell($w[4],4, utf8_decode($NRO_MUNICIPAL),0,'L');
                    //$pdf->Cell($w[4],4,$NRO_MUNICIPAL,'LR',0,'R');
                    $pdf->SetXY($x+$w[4],$y);

                    //$pdf->Ln(2);
                    $linea = $linea + 10;
                    $pdf->Line(11, $linea, 195, $linea);
                    $pdf->Ln(10);
                    
                   
                }
            }

            $fill = false;
    
            // Línea de cierre
            //$pdf->Cell(array_sum($w),0,'','T');
            $pdf->Ln();
            $pdf->Ln(2);


            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(50,8,'TITULAR',1,0,'C',true);		
            $pdf->SetFillColor(255,255,255);	        
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(185,6,utf8_decode('PROPIETARIO            : ' . $NOMBRE_INSTITUCION),1,0,'L',true);
            $pdf->Ln();
            $pdf->Cell(185,6,utf8_decode('DERECHO INSCRITO: ' . $DERECHO_INSCRITO),1,0,'L',true);
            $pdf->Ln();
            $pdf->Ln(2);
    

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(50,8,'ACTOS REGISTRADOS',1,0,'C',true);		
            $pdf->SetFillColor(255,255,255);	        
            $pdf->Ln();

            $data_campos= DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_IMPRESION_CARATULA ?,?",
                [   
                    'A',
                    $numero
                ]
            );

            if (isset($data_campos[0])){
                foreach ($data_campos as $key => $value) {
                    $ITEM               = $value->ITEM;
                    $DSC_DETALLE_ACTO	= $value->DSC_DETALLE_ACTO;
                    $VIGENTE	        = $value->VIGENTE;
                    $DSC_NORMAL_LEGAL   = $value->DSC_NORMAL_LEGAL;
                    $NRO_DISLEGAL	    = $value->NRO_DISLEGAL;
                    $FEC_DISLEGAL	    = $value->FEC_DISLEGAL;
                    $DSC_TIPO_DOCUMENTO	= $value->DSC_TIPO_DOCUMENTO;
                    $NRO_DOCUM	        = $value->NRO_DOCUM;
                    $FEC_DOCUM	        = $value->FEC_DOCUM;
                    $OTORGADO_POR	    = $value->OTORGADO_POR;
                    $NOMBRE_INSTITUCION	= $value->NOMBRE_INSTITUCION;
                    $USO_GENERICO	    = $value->USO_GENERICO;
                    $AREA_DISPUESTA	    = $value->AREA_DISPUESTA;
                    $USO_ESPECIFICO	    = $value->USO_ESPECIFICO;
                    $OFICINA_REGISTRAL	= $value->OFICINA_REGISTRAL;

                    
                    $pdf->SetFillColor(205,205,205);	
                    $pdf->Cell(30,6,utf8_decode('ASIENTO NRO ' . $ITEM),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->Cell(110,6,utf8_decode($DSC_DETALLE_ACTO),1,0,'L',true);	
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(45,6, $VIGENTE,1,0,'R',true);
                    $pdf->Ln();

                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(90,6,utf8_decode('APROBADO POR: ' . $DSC_NORMAL_LEGAL ),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(50,6,utf8_decode('NRO: ' . $NRO_DISLEGAL),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(45,6,utf8_decode('FECHA: ' . $FEC_DISLEGAL),1,0,'L',true);
                    $pdf->Ln();

                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(90,6,utf8_decode('MEDIANTE: ' . $DSC_TIPO_DOCUMENTO),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(50,6,utf8_decode('NRO: ' . $NRO_DOCUM),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(45,6,utf8_decode('FECHA: ' . $FEC_DOCUM),1,0,'L',true);
                    $pdf->Ln();

                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(185,6,utf8_decode('OTORGADO POR: ' . $OTORGADO_POR),1,0,'L',true);
                    $pdf->Ln();

                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(185,6,utf8_decode('A FAVOR DE: ' . $NOMBRE_INSTITUCION),1,0,'L',true);
                    $pdf->Ln();

                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(100,6,utf8_decode('USO GENERICO: ' . $USO_GENERICO),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(85,6,utf8_decode('AREA DISPUESTA (m2): ' . $AREA_DISPUESTA),1,0,'L',true);
                    $pdf->Ln();

                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(100,6,utf8_decode('USO ESPECIFICO: ' . $USO_ESPECIFICO),1,0,'L',true);
                    $pdf->SetFillColor(255,255,255);	
                    $pdf->Cell(85,6,utf8_decode('INSCRITO EN: ' . $OFICINA_REGISTRAL),1,0,'L',true);
                    $pdf->Ln();
                    $pdf->Ln(2);
                   
                }
            }

            $fill = false;
    
            // Línea de cierre
            //$pdf->Cell(array_sum($w),0,'','T');
            $pdf->Ln(2);
            

            /* DATOS TECNICOS */
            $data_tecnicos= DB::connection('sqlsrv_S_')->select(
                "exec PA_SINABIP_IMPRESION_CARATULA ?,?",
                [   
                    'T',
                    $numero
                ]
            );


            $FEC_INSPECCION	    = $data_tecnicos[0]->FEC_INSPECCION;
            $NMB_FABRICA	    = $data_tecnicos[0]->NMB_FABRICA;
            $IOCUPANTE	        = $data_tecnicos[0]->IOCUPANTE;
            $TIP_ZONA	        = $data_tecnicos[0]->TIP_ZONA;
            $AREA_TECNICA	    = $data_tecnicos[0]->AREA_TECNICA;
            $UGENERICO	        = $data_tecnicos[0]->UGENERICO;
            $TIP_TERRENO	    = $data_tecnicos[0]->TIP_TERRENO;
            $VALOR_DOLARES	    = $data_tecnicos[0]->VALOR_DOLARES;
            $UESPECIFICO	    = $data_tecnicos[0]->UESPECIFICO;
            $TOLERANCIA	        = $data_tecnicos[0]->TOLERANCIA;
            $VALOR_SOLES	    = $data_tecnicos[0]->VALOR_SOLES;
            $EST_DIGITALIZACION	= $data_tecnicos[0]->EST_DIGITALIZACION;
            $ZONIFICACION	    = ($data_tecnicos[0]->ZONIFICACION == "0"?"":$data_tecnicos[0]->ZONIFICACION);


            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(50,8,'DATOS TECNICOS',1,0,'C',true);		
            $pdf->SetFillColor(255,255,255);	        
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(50,6,utf8_decode('FECHA DE INSPECCION: ' . $FEC_INSPECCION),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(185,6,utf8_decode('DENOMINACION: ' . $NMB_FABRICA),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('OCUPANTE: ' . $IOCUPANTE),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('ZONA: ' . $TIP_ZONA),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('AREA(M2): ' . $AREA_TECNICA),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('USO GENERICO: ' . $UGENERICO),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('TERRENO: ' . $TIP_TERRENO),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('VALOR($): ' . $VALOR_DOLARES),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('USO ESPECIFICO: ' . $UESPECIFICO),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('TOLERANCIA: ' . $TOLERANCIA),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(60,6,utf8_decode('VALOR(S/.): ' . $VALOR_SOLES),1,0,'L',true);
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(65,6,utf8_decode('SIT. CATASTRAL: ' . $EST_DIGITALIZACION),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(120,6,utf8_decode('ZONIFICACION: ' . $ZONIFICACION),1,0,'L',true);
            $pdf->Ln();
            $pdf->Ln(3);


            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(50,8,'OBSERVACIONES',1,0,'C',true);		
            $pdf->SetFillColor(255,255,255);	        
            $pdf->Ln();

            $pdf->SetFillColor(255,255,255);	
            //$pdf->Cell(185,12,utf8_decode($OBSERVACIONES),1,0,'L',true);
            $pdf->MultiCell(185,6,utf8_decode($OBSERVACIONES), 1,'L',true);          
            
            
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
    }

    public function obtenerTipoAccionDetalleUD(Request $request){
        $codigo_interno = $request->codigo_interno;
        
        $data = DB::connection('sqlsrv_S_')->select('EXEC dbo.PA_SINABIP_OBTENER_TIPO_ACCION_DETALLE_UD ?', [$codigo_interno]);
        return response()->success($data);    
    }


    public function Busqueda_Cus_registrados_notificados(Request $request){  
      
        $ruc            = $request->rucs;
        $cus            = $request->cus;
        $departamento   = $request->coddepa;
        $provincia      = $request->codprov;
        $distrito       = $request->coddist;
        $page           = $request->page;
        $records        = $request->records;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_LISTADO_CONTROL_REGISTRO_PREDIOS_NOTIFICADOS ?,?,?,?,?,?,?",[
                $ruc,
                $cus,
                $departamento,
                $provincia,
                $distrito,
                $page,
                $records
            ]
        );
        
        return response()->success([
            "listadoCusRegistradosNotificados" => (count($data) > 0) ?$data : []
        ]);
    }


    public function Busqueda_Cus_registrados_aprobados(Request $request){  
      
        $ruc            = $request->rucs;
        $cus            = $request->cus;
        $departamento   = $request->coddepa;
        $provincia      = $request->codprov;
        $distrito       = $request->coddist;
        $page           = $request->page;
        $records        = $request->records;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_LISTADO_CONTROL_REGISTRO_PREDIOS_APROBADOS ?,?,?,?,?,?,?",[
                $ruc,
                $cus,
                $departamento,
                $provincia,
                $distrito,
                $page,
                $records
            ]
        );
        
        return response()->success([
            "listadoCusRegistradosAprobados" => (count($data) > 0) ?$data : []
        ]);
    }


    public function verificador_errores_registro($codinterno, $idControlCalidad){                   
        
        $data_verificador1= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
            ['2', $codinterno, $idControlCalidad]
        );

        $data_verificador2= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_VERIFICADOR_CONTROL_REGISTRO ?,?,?",
            ['3', $codinterno, $idControlCalidad]
        );

        $resultado = [];
        if(isset($data_verificador1[0]) || isset($data_verificador2[0])){
            $resultado = "ERROR";
        }else{
            $resultado = "OK";
        }
        
       
        return response()->success($resultado);   

    }


    public function GuardarNotificacion(Request $request){
        $idControlCalidad       = $request->idControlCalidad;
        $codigo_observaciones   = $request->codigo_observaciones;
        $observaciones          = $request->observaciones;
        $codigo_usuario         = $request->codigo_usuario;

        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ENVIAR_NOTIFICACIONES ?,?,?,?', [$idControlCalidad, $codigo_observaciones, $observaciones, $codigo_usuario ]);
        
        return response()->success($data); 
    }

}
