<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Exception; 
use Config;
use Illuminate\Support\Facades\Storage;
use Pdf_Reporte_Entidades; 
use App\Exports\CollectionExport; 
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel;
use PHPExcel_IOFactory;

class PrediosWebController extends Controller
{
    
 
    public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoPrediosWeb(Request $request)
    {
        //header('Access-Control-Allow-Origin: *');

        $valor = '%%';
        $page     = $request->page;
        $records  = $request->records;

        //dd($request);

        $data = DB::connection('sqlsrv_M_')->select('exec MGC_LISTADO_PREDIOS_WEB ?,?,?', [
            $valor,
            $page,
            $records
        ]);
    
	    return response()->success([
            "listado" => (count($data) > 0) ?$data : []
        ]);
    }

    public function BuscarPrediosWeb(Request $request)
    {
        $valor    = $request->valor;
        $page     = $request->page;
        $records  = $request->records;

        //dd($request);

        $data = DB::connection('sqlsrv_M_')->select('exec MGC_LISTADO_PREDIOS_WEB ?,?,?', [
            $valor,
            $page,
            $records
        ]);
      
        return response()->success([
            "listado" => (count($data) > 0) ?$data : []
        ]);
    }

    public function BusquedaAvanzadaPredios(Request $request)
    {
        $ruc                = $request->ruc;
        $denominacion_cus   = $request->denominacion_cus;
        $departamento       = $request->departamento;
        $provincia          = $request->provincia;
        $distrito           = $request->distrito;  
        $direccion          = $request->direccion;
        $nroSI              = $request->nroSI; 
        $propietario        = $request->propietario;
        $areaMinima         = $request->areaMinima;
        $areaMaxima         = $request->areaMaxima;
        $tipoPartida        = $request->tipoPartida;
        $nroPartida         = $request->nroPartida;
        
        if ($tipoPartida =='1')
        {
            $partida     = 'P'.$nroPartida;
        }
        else
        {
            $partida     = $nroPartida;
        }
        $ocurrencia         = $request->ocurrencia;
        $page               = $request->page;
        $records            = $request->records;

        $data = DB::connection('sqlsrv_S_')->select('exec PA_SINABIP_BUSQUEDA_AVANZADA_PREDIOS_WALAS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', [
            $ruc,
            $denominacion_cus,
            $departamento,
            $provincia,
            $distrito,
            $direccion,
            $propietario,
            $areaMinima,
            $areaMaxima,
            $tipoPartida,
            $partida,
            $nroSI,
            $ocurrencia, 
            $page,
            $records
        ]);
    
        return response()->success([
            "listado" => (count($data) > 0) ?$data : []
        ]);
    }


    public function ListadoPrediosCS(Request $request)
    {
        $valor    = $request->valor;         
        $coddep   = $request->coddepa;
        $codprov  = $request->codprov;
        $coddist  = $request->coddist;
        $tipo     = $request->tipo;
        $page     = $request->page;
        $records  = $request->records;                   
        $data = DB::connection('sqlsrv_S_')->select('exec USP_SeaListadoPredios ?,?,?,?,?,?,?',[
                $valor,$coddep,$codprov,$coddist,$tipo,$page,$records]);
        
        return response()->success([
            "listado" => (count($data) > 0) ?$data : []
        ]);
    } 


    public function ObtenerEntidades(Request $request)
    {
        $valor    = $request->valor;                       
        $page     = $request->page;
        $records  = $request->records;                        
        $data = DB::connection('sqlsrv_M_')->select('exec USP_SeaListadoEntidades ?,?,?',[
                $valor,$page,$records]);
        
        return response()->success([
            "listado" => (count($data) > 0) ?$data : []
        ]);
    }       

    public function ObtenerUbigeo(Request $request)
    {
        $coddepa = $request->coddepa;
        $codprov = $request->codprov;
        $coddist = $request->coddist;
        $tipo    = $request->tipo;

        $data = DB::connection('sqlsrv_S_')->select('exec USP_SeaUbigeo ?,?,?,?',
            [ $coddepa,
              $codprov,
              $coddist,
              $tipo]);
    
        return response()->success([
            "listado" => (count($data) > 0) ?$data : []
        ]);
    }

    public function InsertarPrediosWeb(Request $request){         

        $archivo  = json_decode($request->predios);        


        $data = DB::connection('sqlsrv_S_')->select('SELECT IsNull(Max(codigo_interno), 0) + 1 AS codigo_interno FROM LIBROS');
        
        $aux = DB::connection('sqlsrv_S_')->select('SELECT Max(nro_rsinabip) + 1 AS nro_rsinabip FROM LIBROS');


        $xmlpdf = '<?xml version="1.0"?><root>';
        $numero = $data[0]->codigo_interno;
        $nro_sinbip = $aux[0]->nro_rsinabip;

        foreach ($archivo as $row) {             
            $dato = DB::connection('sqlsrv_S_')->select('SELECT ultimo_asiento_E + 1 AS nro_asiento 
                FROM Correlativo_Libros where codigo_libro =?',[$row->cod_depa]);

             $asiento = $dato[0]->nro_asiento;
             $xmlpdf.='<row><cu>'.$row->codigo_ubigeo.'</cu>'.                        
            '<di>'.$row->denominacion_inmueble.'</di>'.
            '<id>'.$row->id.'</id>'.
            '<ci>'.$numero.'</ci>'.
            '<na>'.$asiento.'</na>'.
            '<ns>'.$nro_sinbip.'</ns>'.
            '<dir>'.$row->direccion.'</dir>'.
            '<ip>'.$row->ID_PREDIO_SBN.'</ip>'.
            '</row>';
            $numero++;   
            $nro_sinbip++;

        }
        $xmlpdf.='</root>';        

        $data = DB::connection('sqlsrv_S_')->select('exec USP_InsPrediosWeb ?',
               [$xmlpdf]);

        DB::connection('sqlsrv_M_')->statement('exec USP_Modificar ?',[$xmlpdf]);

        return response()->success($data);            
    }

    public function ActualizarRUC(Request $request){         

        $archivo = $request->predios;        
        $ruc     = $request->ruc;        
        $xmlpdf = '<?xml version="1.0"?><root>';                
        foreach ($archivo as $row) {                                      
             $xmlpdf.='<row><id>'.$row['codigo_interno'].'</id>'.
            '</row>';            

        }
        $xmlpdf.='</root>';              
        $data = DB::connection('sqlsrv_S_')->statement('exec USP_ActualizarRUC ?,?',
               [$xmlpdf,$ruc]);
        
                                   
        return response()->success($data);            
    }
    
    public function DatosPrincipales(Request $request){
        /* OTROS PARAMETROS */  
        $P_CODIGO_INTERNO      = $request->tipoproceso;   
        $P_CODIGO_UDOCUMENTAL  = $request->unidaddoc;   
        $DETALLE_TIPO_ACCION   = $request->tipo_registro; 
        $TIPO_REGISTRO2        = $request->tipo_registro2;  
        $TIPO_REGISTRO         = $request->aux_reg;  
        $codigodocumento       = $request->codigodocumento;  
        $nro_documento         = $request->nro_documento;  

        
        /* DATOS GENERALES */  
        $ESTADO_ASIENTO        =  $request->condicion; 
        $ESTADO_CALIFICACION   =  $request->calificacion;       
        $DENOMINACION_INMUEBLE =  $request->denominacion; 
        $TIPO_VIA              =  $request->via;             
        $DIRECCION_INMUEBLE    =  $request->direccion;              
        $NRO_INMUEBLE          =  $request->numero; 
        $NRO_MANZANA           =  $request->manzana; 
        $NRO_LOTE              =  $request->lote; 
        $DIRECCION_DETALLE     =  $request->detalle; 
        $NUMERACION_DETALLE    =  $request->detalledescrip; 
        $PISO_UBICACION        =  $request->piso; 
        $TIPO_HABILITACION     =  $request->habilitacion;
        $NMB_URBANIZACION      =  $request->descripcion;
        $REFERENCIA_UBICACION  =  $request->referencia;
        $CODIGO_COMPETENCIA    =  $request->competencia;   
        $RESERVA               =  $request->reserva; 
        $CODIGO_DEP            =  $request->coddepa;  
        $CODIGO_PRO            =  $request->codprov; 
        $CODIGO_DIS            =  $request->coddist;  
        $SOLICITUD_INGRESO     = $request->solicitud_ingreso;
             
        /* ADQUISICION E INSCRIPCION */  
        $CODIGO_INST_DONANTE   = $request->otorgante; 
        $CODIGO_TIPODOCUMENTO  = $request->tipodocumento; 
        $NRO_DOCUMENTO         = $request->documento;  
        $CODIGO_ACTO_REGISTRAL = $request->actoregistral; // 2 por defecto
        $CODIGO_TIPO_ACTO      = $request->modalidad;
        $CODIGO_DISLEGAL       = $request->dislegal;
        $NRO_DISLEGAL          = $request->disnum; 
        $FEC_DISLEGAL          = $request->disfecha; // fecha 
        $CODIGO_DOCUM          = $request->coddoc;
        $NRO_DOCUM             = $request->codnum;
        $FEC_DOCUM             = $request->codfecha; //fecha
        $AREA_LEGAL            = $request->areatitulo; 
        $CODIGO_APORTE         = $request->aporte; 
        $TIPO_APORTE           = $request->predetalle; 
        $CODIGO_FAVORECIDA     = $request->afavor;   
        $SEDE_REGISTRAL        = $request->oficina;  
        $CODIGO_PARTIDA        = $request->codpartida;
        $NRO_PARTIDA           = $request->numpartida;
        if ($CODIGO_PARTIDA =='1')
        {
            $partida     = 'P'.$NRO_PARTIDA;
        }
        else
        {
            $partida     = $NRO_PARTIDA;
        }

        $FOJAS                 = $request->fojas;  
        $AREA_REGISTRAL        = $request->arearegistral;
      
        /* CAMPOS DE AUDITORIA */  
        $USUARIO_EDICION       = $request->usuario; //1913 select + from personal 
        //$ARRAY_LINDEROS        = $request->linderos;
        //$ARRAY_FABRICA         = $request->fabrica;


        $CODIGO_PARTIDA2        = $request->codpartida2;
        $NRO_PARTIDA2           = $request->numpartida2;
        if ($CODIGO_PARTIDA2 =='1')
        {
            $partida2     = 'P'.$NRO_PARTIDA2;
        }
        else
        {
            $partida2     = $NRO_PARTIDA2;
        }

        $FOJAS2                 = $request->fojas2;  



        $ARRAY_INDEPENDIZACION  = $request->independizacion;
        $array_documentosTL    = $request->documentosTL;


        $xmlindependizaciones = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_INDEPENDIZACION as $row) {                         
             $xmlindependizaciones.='<row><ii>'.$row['item'].'</ii>'.                                   
            '<ta>'.$row['tipoacto'].'</ta>'.
            '<tr>'.$row['tiporegistro'].'</tr>'.
            '<fa>'.$row['fechaacto'].'</fa>'.
            '<to>'.$row['tomo'].'</to>'.
            '<fo>'.$row['foja'].'</fo>'.
            '<fi>'.$row['ficha'].'</fi>'.
            '<pe>'.$row['partidaelectronica'].'</pe>'.
            '<cp>'.$row['codigopredio'].'</cp>'.
            '<ai>'.$row['areaindependizada'].'</ai>'.
            '<na>'. $row['nroasiento'] .'</na>'.
            '<cii>'.$row['codigoinmuebleindep'].'</cii>'.
            '</row>';
        } 
        $xmlindependizaciones.='</root>';  

        $xmldocumentosTL = '<?xml version="1.0"?><root>';     
        foreach ($array_documentosTL as $row) {                         
             $xmldocumentosTL.='<row>'.                                    
            '<ct>'.$row['CODIGO_TIPO_DOCUMENTO'].'</ct>'.
            '</row>';          
        }
        $xmldocumentosTL.='</root>';    

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_PREDIO_DATOS_PRINCIPALES_ADQUISICION_INSCRIPCION 
            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?',
            [
             $P_CODIGO_INTERNO,
             $P_CODIGO_UDOCUMENTAL,
             $DETALLE_TIPO_ACCION,
             $TIPO_REGISTRO2,
             $TIPO_REGISTRO,
             $codigodocumento,
             $nro_documento,
             $ESTADO_ASIENTO,
             $ESTADO_CALIFICACION,
             $DENOMINACION_INMUEBLE,                
             $TIPO_VIA,
             $DIRECCION_INMUEBLE,
             $NRO_INMUEBLE,
             $NRO_MANZANA,
             $NRO_LOTE,
             $DIRECCION_DETALLE,
             $NUMERACION_DETALLE,
             $PISO_UBICACION,
             $TIPO_HABILITACION,
             $NMB_URBANIZACION,
             $REFERENCIA_UBICACION,
             $CODIGO_COMPETENCIA,
             $RESERVA,        
             $CODIGO_DEP,
             $CODIGO_PRO,
             $CODIGO_DIS, 
             $SOLICITUD_INGRESO,                    
             $CODIGO_INST_DONANTE,
             $CODIGO_TIPODOCUMENTO,
             $NRO_DOCUMENTO,
             $CODIGO_ACTO_REGISTRAL,
             $CODIGO_TIPO_ACTO,
             $CODIGO_DISLEGAL,
             $NRO_DISLEGAL,
             $FEC_DISLEGAL,
             $CODIGO_DOCUM,
             $NRO_DOCUM,
             $FEC_DOCUM,
             $AREA_LEGAL,
             $CODIGO_APORTE,
             $TIPO_APORTE,
             $CODIGO_FAVORECIDA,
             $SEDE_REGISTRAL,
             $CODIGO_PARTIDA,
             $partida,
             $FOJAS,
             $AREA_REGISTRAL,

             $CODIGO_PARTIDA2,
             $partida2,
             $FOJAS2, 


             $USUARIO_EDICION,
             //$xmllinderos,
             //$xmlfabrica,
             $xmlindependizaciones,
             $xmldocumentosTL
            ]);                        
            return response()->success($data);                                      

    }



    public function fabricaLinderos(Request $request){
        /* OTROS PARAMETROS */  
        $P_CODIGO_INTERNO      = $request->codigo_interno;
        $USUARIO_EDICION       = $request->usuario; 
        $ARRAY_LINDEROS        = $request->linderos;
        $ARRAY_FABRICA         = $request->fabrica;
        
        $xmllinderos = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_LINDEROS as $row) {                         
             $xmllinderos.='<row><it>'.$row['item'].'</it>'.                                   
            '<li>'.$row['lindero'].'</li>'.
            '<de>'.$row['colindancia'].'</de>'.
            '<tr>'.$row['tramos'].'</tr>'.
            '<la>'.$row['lados'].'</la>'.
            '<nr>'.$row['municipal'].'</nr>'.
            '</row>';          
        } 
        $xmllinderos.='</root>';  
       

        $xmlfabrica = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_FABRICA as $row) {                         
            $xmlfabrica.='<row><it>'.$row['item'].'</it>'.                        
            '<ti>'.$row['tipoacto'].'</ti>'.
            '<de>'.$row['detacto'].'</de>'.
            '<fe>'.$row['fecha'].'</fe>'.
            '<ar>'.$row['area'].'</ar>'.
            '<tm>'.$row['moneda'].'</tm>'.
            '<va>'.$row['valorizacion'].'</va>'.
            '</row>';          
        }
        $xmlfabrica.='</root>';
          
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_PREDIO_FABRICA_LINDEROS 
            ?,?,?,?',
            [
             $P_CODIGO_INTERNO,
             $USUARIO_EDICION,
             $xmllinderos,
             $xmlfabrica
            ]);               
            return response()->success($data);                                      

    }



    public function Limitaciones(Request $request){                       
        $P_CODIGO_INTERNO          = $request->codigo_interno;           
        $CONDICION_ASIENTO         = $request->restricciones; 
        $TIPO_APORTE_REGLAMENTARIO = $request->coddetalle;       
        $AREA                      = $request->area;             
        $USUARIO_EDICION           = $request->codusuario; //1913 select + from personal 
        $ARRAY_PROCESO             = $request->proceso;

       $xmlproceso = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_PROCESO as $row) {                         
             $xmlproceso.='<row><it>'.$row['item'].'</it>'.                                   
            '<cm>'.$row['codmateria'].'</cm>'.
            '<nu>'.$row['numero'].'</nu>'.
            '<do>'.$row['demandado'].'</do>'.
            '<de>'.$row['demandante'].'</de>'.
            '<ar>'.$row['area'].'</ar>'.
            '<si>'.$row['situacion'].'</si>'.
            '</row>';          
        }
        $xmlproceso.='</root>';     
        

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_PREDIO_LIMITACIONES_PROPIEDAD 
            ?,?,?,?,?,?',
            [
             $P_CODIGO_INTERNO,                
             $CONDICION_ASIENTO,
             $TIPO_APORTE_REGLAMENTARIO,
             $AREA,
             $USUARIO_EDICION,         
             $xmlproceso
            ]);                        
            return response()->success($data);                                      

    }

    public function Actos(Request $request){                       
        $P_CODIGO_INTERNO   = $request->codigo_interno;
        $USUARIO_EDICION    = $request->codusuario; //1913 select + from personal 
        $ARRAY_ACTOS        = $request->actos;

        $codigoPredio = '';
        $partidaElectronica = '';
        $ficha = '';
        $tomo = '';
        $fojas = '';

       $xmlactos = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_ACTOS as $row) {  
            $codigoPredio = "";
            $partidaElectronica = "";
            $ficha = "";
            $tomo = "";
            $fojas = "";

            if(is_null($row['fechalegal'])){
                $row['fechalegal'] = NULL;
            }
            if(is_null($row['docfecha'])){
                $row['docfecha'] = NULL;
            }


            if($row['codigopartida'] == "1"){
                $codigoPredio = $row['nropartida'];     
            }
            if($row['codigopartida'] == "2"){
                $partidaElectronica = $row['nropartida'];
            }
            if($row['codigopartida'] == "3"){
                $ficha = $row['nropartida'];
            }
            if($row['codigopartida'] == "4"){
                $tomo = $row['nropartida'];
            }
            if($row['codigopartida'] == "4"){
                $fojas = $row['fojas'];
            }


             $xmlactos.='<row><it>'.$row['item'].'</it>'.                                   
            '<ar>'.$row['codigoacto'].'</ar>'.
            '<ta>'.$row['coddetalle'].'</ta>'.
            '<cd>'.$row['codnorma'].'</cd>'.
            '<nd>'.$row['numerolegal'].'</nd>'.
            '<fd>'.$row['fechalegal'].'</fd>'.            
            '<dn>'.$row['doccodigo'].'</dn>'.
            '<dc>'.$row['docnumero'].'</dc>'.
            '<df>'.$row['docfecha'].'</df>'.
            '<af>'.$row['afavor'].'</af>'.
            '<ad>'.$row['dispuesta'].'</ad>'.
            '<ae>'.$row['areare'].'</ae>'.
            '<cg>'.$row['codgenerico'].'</cg>'. 
            '<ce>'.$row['codespecifico'].'</ce>'. 
            '<di>'.$row['codderecho'].'</di>'.
            '<va>'.$row['codvigencia'].'</va>'.
            '<pi>'.$row['plazoinicial'].'</pi>'.
            '<pf>'.$row['plazofinal'].'</pf>'.
            '<tt>'.$row['tiempo'].'</tt>'.
            '<ca>'.$row['costo'].'</ca>'.
            '<sr>'.$row['oficinaregistral'].'</sr>'.
            '<ax>'.$row['arearegistral'].'</ax>'.
            '<al>'.$row['arealegal'].'</al>'.
            '<cp>'.$row['codigopartida'].'</cp>'. 
            '<pp>'. $codigoPredio .'</pp>'.
            '<pe>'. $partidaElectronica .'</pe>'.
            '<fi>'. $ficha .'</fi>'.
            '<to>'. $tomo .'</to>'.
            '<fo>'. $fojas .'</fo>'.
            '<ka>'. $row['aporte'] .'</ka>'.
            '<kad>'. $row['aporteDetalle'] .'</kad>'.
            '<pid>'.$row['plazoinicial_doc'].'</pid>'.
            '<pfd>'.$row['plazofinal_doc'].'</pfd>'.
            '<ttd>'.$row['tiempo_doc'].'</ttd>'.
            '</row>';
        }
        $xmlactos.='</root>';     
        

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_ACTOS_ADMINISTRACION_DISPOSICION 
            ?,?,?',
            [
             $P_CODIGO_INTERNO,                           
             $USUARIO_EDICION,         
             $xmlactos
            ]);                        
            return response()->success($data);                                      

    }

    public function DatosTecnicos(Request $request){                       
        $P_CODIGO_INTERNO   = $request->codigo_interno;
        $via                = $request->via;
        $direccion          = $request->direccion;
        $numero             = $request->numero;
        $manzana            = $request->manzana;
        $lote               = $request->lote;
        $detalle            = $request->detalle;
        $detalledescrip     = $request->detalledescrip;
        $piso               = $request->piso;
        $habilitacion       = $request->habilitacion;
        $descripcion        = $request->descripcion;
        $referencia         = $request->referencia;
        $cod_tipozona       = $request->cod_tipozona;
        $cod_terreno        = $request->cod_terreno;
        $zonificacion       = $request->zonificacion;
        $codgenerico        = $request->codgenerico;
        $codespecifico      = $request->codespecifico;
        $area               = $request->area;
        $codocupante        = $request->codocupante;
        $razon              = $request->razon;
        $tipodoc            = $request->tipodoc;
        $numdoc             = $request->numdoc;
        $parcial            = $request->parcial;
        $areades            = $request->areades;
        $USUARIO_EDICION    = $request->codusuario; //1913 select + from personal 
        $ARRAY_LINDEROS     = $request->linderos;            

        $xmllinderos = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_LINDEROS as $row) {                         
             $xmllinderos.='<row><it>'.$row['item'].'</it>'.                                   
            '<li>'.$row['lindero'].'</li>'. 
            '<co>'.$row['colindancia'].'</co>'.
            '<tr>'.$row['tramos'].'</tr>'.
            '<la>'.$row['lados'].'</la>'.
            '<nr>'.$row['municipal'].'</nr>'.
            '</row>';          
        }
        $xmllinderos.='</root>'; 

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_DATOS_TECNICOS 
            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?',
            [
            $P_CODIGO_INTERNO,
            $via             ,
            $direccion       ,
            $numero          ,
            $manzana         ,
            $lote            ,
            $detalle         ,
            $detalledescrip  ,
            $piso            ,
            $habilitacion    ,
            $descripcion     ,
            $referencia      ,
            $cod_tipozona    ,
            $cod_terreno     ,
            $zonificacion    ,
            $codgenerico     ,
            $codespecifico   ,
            $area            ,
            $codocupante     ,
            $razon           ,
            $tipodoc         ,
            $numdoc          ,
            $parcial         ,
            $areades         ,
            $USUARIO_EDICION ,
            $xmllinderos       
            ]);                        
            return response()->success($data);                                      

    }



    public function Construcciones(Request $request){                        
        $codigo_interno      = $request->codigo_interno;
        $habilitacion        = $request->habilitacion;
        $situacion           = $request->situacion;
        $nropiso             = $request->nropiso;
        $porcentaje          = $request->porcentaje;
        $material            = $request->material;
        $USUARIO_EDICION     = $request->codusuario;
        $ARRAY_CONST        = $request->construcciones;
        $ARRAY_DETAL         = $request->detalle;            

        $xmlconst = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_CONST as $row) {                         
             $xmlconst.='<row><cc>'.$row['codigo'].'</cc>'.                                   
            '<nc>'.$row['nombreconst'].'</nc>'.
            '<at>'.$row['area'].'</at>'.            
            '</row>';          
        }
        $xmlconst.='</root>';   

        $xmldet = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_DETAL as $row) {                         
             $xmldet.='<row><cd>'.$row['codigoconstruccion'].'</cd>'.                                   
            '<cp>'.$row['itemx'].'</cp>'.
            '<np>'.$row['piso'].'</np>'.            
            '<mc>'.$row['mes'].'</mc>'.
            '<an>'.$row['anio'].'</an>'.    
            '<cm>'.$row['codmaterial'].'</cm>'. 
            '<ce>'.$row['codestado'].'</ce>'.            
            '<mu>'.$row['muro'].'</mu>'.
            '<te>'.$row['techo'].'</te>'.  
            '<in>'.$row['elect'].'</in>'.
            '<pi>'.$row['pisos'].'</pi>'.            
            '<pu>'.$row['puerta'].'</pu>'.
            '<re>'.$row['revest'].'</re>'.    
            '<ba>'.$row['banio'].'</ba>'.
            '<vu>'.$row['valorunitario'].'</vu>'.            
            '<ac>'.$row['area'].'</ac>'.
            '<ve>'.$row['valorestimado'].'</ve>'.          
            '</row>';             
        }
        $xmldet.='</root>';    

        

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_CONSTRUCCIONES 
            ?,?,?,?,?,?,?,?,?',
            [
            $codigo_interno   ,
            $habilitacion     ,
            $situacion        ,
            $nropiso          ,
            $porcentaje       ,
            $material         ,
            $USUARIO_EDICION  ,
            $xmlconst      ,
            $xmldet      
            ]);                        
            return response()->success($data);                                      

    }


    public function Obras(Request $request){                       
        $P_CODIGO_INTERNO     = $request->codigo_interno;
        $OBSERVACIONES        = $request->OBSERVACIONES;
        $TIPO_VALORIZACION    = $request->TIPO_VALORIZACION;
        $TIPO_MONEDA          = $request->TIPO_MONEDA;
        $TIPO_CAMBIO          = $request->TIPO_CAMBIO;
        $FECHA_TASACION       = $request->FECHA_TASACION;
        $VALOR_CONSTRUCCIONES = $request->VALOR_CONSTRUCCIONES;
        $VALOR_OBRAS          = $request->VALOR_OBRAS;
        $VALOR_TERRENO        = $request->VALOR_TERRENO;
        $VALOR_INMUEBLE       = $request->VALOR_INMUEBLE;
        $VALOR_SOLES          = $request->VALOR_SOLES;
        $USUARIO_EDICION      = $request->codusuario; //1913 select + from personal 
        $ARRAY_OBRAS          = $request->obrascomplementarias;       

        $xmlobras = '<?xml version="1.0"?><root>';     
        foreach ($ARRAY_OBRAS as $row) {                         
             $xmlobras.='<row><it>'.$row['item'].'</it>'.                                   
            '<co>'.$row['codigoobra'].'</co>'.
            '<de>'.$row['denominacion'].'</de>'.
            '<an>'.$row['antiguedad'].'</an>'.
            '<va>'.$row['valor_unitario'].'</va>'.
            '<ca>'.$row['cantidad'].'</ca>'.
            '<ce>'.$row['codestado'].'</ce>'.
            '<cm>'.$row['codmaterial'].'</cm>'.
            '<ve>'.$row['valor_estimado'].'</ve>'. 
            '</row>';                   
        }
        $xmlobras.='</root>'; 

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_OBRAS_VALORIZACION 
            ?,?,?,?,?,?,?,?,?,?,?,?,?',
            [
            $P_CODIGO_INTERNO,
            $OBSERVACIONES,
            $TIPO_VALORIZACION,
            $TIPO_MONEDA,
            $TIPO_CAMBIO,
            $FECHA_TASACION,
            $VALOR_CONSTRUCCIONES,
            $VALOR_OBRAS,
            $VALOR_TERRENO,
            $VALOR_INMUEBLE,
            $VALOR_SOLES,
            $USUARIO_EDICION,
            $xmlobras        
            ]);                        
            return response()->success($data);                                      

    }

    public function Zona(Request $request){                       
        $P_CODIGO_INTERNO   =  $request->codigo_interno;
        $codlam             = $request->codlam;
        $observaciones      = $request->observaciones;
        $codzona            = $request->codzona;
        $areazona           = $request->areazona;
        $codrestringido     = $request->codrestringido;
        $arearestringido    = $request->arearestringido;
        $codprivado         = $request->codprivado;
        $areaprivado        = $request->areaprivado;
        $USUARIO_EDICION    = $request->codusuario; //1913 select + from personal 
        

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_ZONA_PLAYA 
            ?,?,?,?,?,?,?,?,?,?',
            [
            $P_CODIGO_INTERNO  ,
            $codlam            ,
            $observaciones     ,
            $codzona           ,
            $areazona          ,
            $codrestringido    ,
            $arearestringido   ,
            $codprivado        ,
            $areaprivado       ,
            $USUARIO_EDICION   
            ]);                        
            return response()->success($data);                                      

    }
    /***********************OBTENER DATA******************************/

    public function ObtenerData($accion,$codinterno){                   
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_MOSTRAR_DATOS_CUS ?,?', [$accion,$codinterno]);
        return response()->success($data);                             
    }

    public function DetallesPiso($codinterno,$codconst){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_DETALLES_POR_PISO ?,?', [$codinterno,$codconst]);
        return response()->success($data);                             
    }

    public function UltimoProceso($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_OBTENER_ULTIMO_ITEM_PROCESOJUDICIAL ?', [$codinterno]);
        return response()->success($data);                             
    }

    public function UltimoLL($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(ITEM), 0) AS ULTIMO FROM LINDEROS_REGISTRALES WHERE CODIGO_INTERNO= $codinterno");
        return response()->success($data);                             
    }

    public function UltimoLF($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(ITEM), 0) AS ULTIMO FROM DATOS_FABRICA WHERE CODIGO_INTERNO= $codinterno");
        return response()->success($data);                             
    }

    public function UltimoLT($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(NRO_LINDERO), 0) AS ULTIMO FROM LINDEROS_TECNICOS WHERE CODIGO_INTERNO= $codinterno AND nro_lindero = 1");
        return response()->success($data);
    }

    public function UltimoO($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(CODIGO_OBRA), 0) AS ULTIMO FROM OBRAS_COMPLEMENTARIAS WHERE CODIGO_INTERNO= $codinterno");
        return response()->success($data);                             
    }

    public function UltimoC($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(CODIGO_CONSTRUCCION), 0) AS ULTIMO FROM CONSTRUCCIONES_INMUEBLE WHERE CODIGO_INTERNO= $codinterno and item_inspeccion = 1");
        return response()->success($data);                             
    }

    public function UltimoDetPiso($codinterno, $codigo_construccion){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(codigo_piso), 0) AS ULTIMO FROM detalle_construcciones WHERE CODIGO_INTERNO= $codinterno and item_inspeccion = 1 and codigo_construccion = $codigo_construccion ");
        return response()->success($data);                             
    }

    public function UltimoIA($codinterno){               
    
        $data = DB::connection('sqlsrv_S_')->select("SELECT  ISNULL(MAX(ITEM_INDEPENDIZACION), 0) AS ULTIMO FROM INDEPENDIZACION_ASIENTOS WHERE CODIGO_INMUEBLE = $codinterno");
        return response()->success($data);                             
    }


    /**************************ELIMINAR DETALLES**************************************/
    public function EliminarProceso($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ELIMINAR_RESTRICCION_PROCESOJUDICIAL ?,?', [$codinterno,$item]);
        return response()->success($data);                             
    }
    public function EliminarLL($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_LINDERO_LEGAL ?,?', [$codinterno,$item]);
        return response()->success($data);                            
    }
    public function EliminarLF($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_LINDERO_FABRICA ?,?', [$codinterno,$item]);
        return response()->success($data);                            
    }
    public function EliminarActosAd($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_ACTOS_ADMINISTRACION ?,?', [$codinterno,$item]);
        return response()->success($data);                            
    }
    public function EliminarLT($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_LINDERO_TECNICO ?,?', [$codinterno,$item]);
        return response()->success($data);                            
    }
    public function EliminarO($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_OBRA_COMPLEMENTARIA ?,?', [$codinterno,$item]);
        return response()->success($data);                            
    }
    public function EliminarC($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_CONSTRUCCION ?,?', [$codinterno,$item]);
        return response()->success($data);                            
    }
    public function EliminarDP($codinterno,$item,$codconst){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_DETALLE_PISO ?,?,?', [$codinterno,$item,$codconst]);
        return response()->success($data);                            
    }
    public function EliminarIndependizacion($codinterno,$item){                   
        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_ELIMINAR_ITEM_INDEPENDIZACION ?,?', [$codinterno,$item]);
        return response()->success($data);              
    }

    public function validar_cus($nrocus){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_VALIDAR_EXISTENCIA_CUS ?', [$nrocus]);
        return response()->success($data);                             
    }

    public function AgregarExpediente(Request $request){               
        $P_CODIGO_INTERNO   =  $request->codigointerno;
        $P_VALOR   =  $request->valor;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_AGREGAR_EXPEDIENTE_RELACIONADO ?,?', [$P_CODIGO_INTERNO, $P_VALOR]);
        return response()->success($data);                             
    }

    public function CargarListadoExpedientesRelacionados(Request $request){               
        $P_CODIGO_INTERNO   =  $request->codigointerno;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_EXPEDIENTES_RELACIONADOS ?', [$P_CODIGO_INTERNO]);
        return response()->success($data);                             
    }

    public function QuitarExpediente(Request $request){               
        $P_CODIGO_INTERNO   =  $request->codigointerno;
        $P_VALOR            =  $request->valor;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ELIMINAR_EXPEDIENTE_RELACIONADO ?,?', [$P_CODIGO_INTERNO, $P_VALOR]);
        return response()->success($data);                             
    }


    public function AgregarCus(Request $request){               
        $P_CODIGO_INTERNO   =  $request->codigointerno;
        $P_VALOR   =  $request->valorCus;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_AGREGAR_CUS_RELACIONADO ?,?', [$P_CODIGO_INTERNO, $P_VALOR]);
        return response()->success($data);                             
    }

    public function CargarListadoCusRelacionados(Request $request){               
        $P_CODIGO_INTERNO   =  $request->codigointerno;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_CUS_RELACIONADOS ?', [$P_CODIGO_INTERNO]);
        return response()->success($data);                             
    }

    public function QuitarCus(Request $request){               
        $P_CODIGO_INTERNO   =  $request->codigointerno;
        $P_VALOR            =  $request->valorCus;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ELIMINAR_CUS_RELACIONADO ?,?', [$P_CODIGO_INTERNO, $P_VALOR]);
        return response()->success($data);                             
    }

    public function subirArchivo(Request $request){                
        $files = $request->file("files"); 
        $data = $request->get("data");
        $ruc = $data; 
        /* VALIDACION DE ARCHIVOS */
        if($files != null){
            //for  ($i =  0; $i < count($files); $i++){
                $extension = $files->getClientOriginalExtension();
                $tamano = $files->getSize();
    
                if ($extension != 'JPG' && $extension != 'jpg'){
                    return response()->success([
                        "resultado" => 'EXTENSION' 
                    ]);
                } 
                $pesoArchivos = $tamano;
                $documentosAdjuntos = true;
            //}
            if($pesoArchivos > 1050000){
                return response()->success([
                    "resultado" => 'PESO'
                ]);
            }
        }
        /* FIN DE VALIDACION DE ARCHIVOS */


        /* SUBIDA DE ARCHIVOS */
        if($files != null){
            $uploadFolder =  "logoEntidades/";  
            if(Config::get('app.APP_LINUX')) {
                //dd('linux');
                $rutaRaiz = Config::get('app.ruta_documentosLIX');
                //dd($rutaRaiz);
                //Storage::makeDirectory($rutaRaiz . $uploadFolder . $nombre_carpeta);
                //mkdir($rutaRaiz . $uploadFolder . $nombre_carpeta, 0777, true);
            }else{
                dd('windows');
            }
                        
            $nombre_carpeta = 'logoEntidades';
            $extension = $files->getClientOriginalExtension();
            $tamano = $files->getSize();

            /* CALCULAR NOMBRE DE ARCHIVO CARGADO */
            $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $codigoVerificador = substr(str_shuffle($permitted_chars), 0, 9);
            $extension = $files->getClientOriginalExtension();
            $nombreArchivo = $ruc . $codigoVerificador . '.' . $extension;

            $result = DB::connection('sqlsrv_S_')->select("SELECT TOP 1 1 from ENTE where codigo = ?", [
                $ruc
            ]);
            
            if ($result == []){
                return response()->success([
                    "resultado" => 'NOEXISTE',
                    'data' => $result
                ]);
            }
            DB::connection('sqlsrv_S_')->beginTransaction();
            try {
                DB::connection('sqlsrv_S_')->statement("UPDATE ENTE set nombre_archivo_logo = '" . $nombreArchivo . "' where codigo = ?", [
                    $ruc
                ]);
                if(Config::get('app.APP_LINUX')) {
                    $url = Storage::disk('public_pruebas')->putFileAs($nombre_carpeta, $files, $nombreArchivo);
                    $url_temp = storage_path("app/public/").$nombre_carpeta."/".$nombreArchivo;
                    $rutaFinal = $rutaRaiz . $uploadFolder . "/".$nombreArchivo;
                    exec("mv '$url_temp' '$rutaFinal'", $output, $retrun_var);
                }else{
                    dd('windows');
                    $url = Storage::disk('public')->putFileAs($uploadFolder, $files[$i], $nombreArchivo);
                }
                DB::connection('sqlsrv_S_')->commit();

                return response()->success([
                    "resultado" => 'OK',
                    'data' => $url
                ]);
            } catch (\Exception $e) {
                DB::connection('sqlsrv_S_')->rollback();

                return response()->success([
                    "resultado" => 'ERROR',
                    'data' => $e->getMessage()
                ]);
            }
            

        }
        /* FIN DE SUBIDA DE ARCHIVOS */

        return response()->success($url);                             
    }

    public function verReportePDF($ruc){
        header('Content-type: application/pdf');
        
        $rutaRaiz = Config::get('app.ruta_documentosLIX');

        $pdf = new Pdf_Reporte_Entidades('L');
        $pdf->AliasNbPages();    
         
        $pdf->SetFont('Arial','',10);
        $pdf->AddPage();
        $pdf->Ln(4);

        $data = DB::connection('sqlsrv_S_')->select("SELECT TOP 1 ISNULL(nombre_archivo_logo, '')[resultado], nmb_ente from ENTE where codigo = ?", [
            $ruc
        ]);
        $nmb_ente = $data[0]->nmb_ente;
        if($data[0]->resultado != ""){
            $ruta_logos = $rutaRaiz . 'logoEntidades/' . $data[0]->resultado;
            $pdf->Image($ruta_logos,10,1,40,20);//left,higth,ancho, alto
            //$pdf->Image(public_path('./webimages/iconos/sbn_11.png'),10,-3,45);//left,higth,tamano
        }
        $pdf->SetFont('Arial','',9);
        $pdf->Image(public_path('./webimages/iconos/sbn_11.png'),245,-3,50);//ancho,alto 
        $pdf->SetFont('Arial','',9); 

        
        
        //$this->Ln(-1);
        $pdf->SetFont('Arial','B',15);		
        $pdf->Ln(-5);
        //$this->Cell(18);
        $pdf->Cell(0,1,utf8_decode('REPORTE DE PREDIOS: ' . strtoupper($nmb_ente)),0,0,'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','B',12);	
        $pdf->SetTextColor(255,0,0);
        $pdf->Cell(0,12,utf8_decode('SISTEMA DE INFORMACION NACIONAL DE BIENES ESTATALES'),0,0,'C');
        
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(0,1,'',0,0,'C');
        

        //Dibujamos una linea
        //$pdf->Line(10, 20, 196, 20);
        // Salto de línea
        $pdf->Ln(5);
        /**/

        $pdf->SetFillColor(205,205,205);//color de fondo tabla  
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',8);
            
       
    
        // $pdf->SetFillColor(255,255,255);
        // $pdf->MultiCell(185,6,utf8_decode('EXPEDIENTES: ' . $CODIGO_EXPEDIENTE), 1,'L',true);

    
        
        // if($filenamefirst != ''){
        //     $pdf->Image($dir . $filenamefirst,102,33,80,60, 'jpg'); 
        // }
        //$pdf->Image('//192.168.5.59/documentos$/AsientosPrediales/01A101_1.jpg',102,33,90,60, 'jpg');

        $pdf->Ln(2);

        // $pdf->SetFont('Arial','',8);
        // $pdf->SetFillColor(244, 241, 240);
        // $pdf->Rect(10, 35, 280, 150, 'F');
        // $pdf->Line(10, 35, 15, 40);
        // $pdf->SetXY(15, 40);
        // $pdf->Cell(15, 6, '10, 35', 0 , 1);
        
        /* DATOS DE PREDIOS */
        $data_predios= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_REPORTE_ENTIDADES ?",
            [  
                $ruc
            ]
        );

        
        $pdf->SetFillColor(205,205,205);
        $pdf->SetFont('Arial','B',6);
        $w = array(8, 13, 50, 20, 25, 25, 45, 45, 15, 16, 18);
        $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
        $pdf->Cell($w[1],5,utf8_decode('NRO CUS'),1,0,'C',true);
        $pdf->Cell($w[2],5,utf8_decode('DENOMINACION DEL PREDIO'),1,0,'C',true);
        $pdf->Cell($w[3],5,utf8_decode('DEPARTAMENTO'),1,0,'C',true);
        $pdf->Cell($w[4],5,utf8_decode('PROVINCIA'),1,0,'C',true);
        $pdf->Cell($w[5],5,utf8_decode('DISTRITO'),1,0,'C',true);
        $pdf->Cell($w[6],5,utf8_decode('DIRECCION'),1,0,'C',true);
        $pdf->Cell($w[7],5,utf8_decode('PROPIETARIO'),1,0,'C',true);
        $pdf->Cell($w[8],5,utf8_decode('AREA (M2)'),1,0,'C',true);
        $pdf->Cell($w[9],5,utf8_decode('P. REGISTRAL'),1,0,'C',true);
        $pdf->Cell($w[10],5,utf8_decode('SITUACION'),1,0,'C',true);
        $pdf->Ln(); 
       

        $total_reg_pag=10;
        $contador_reg = 0;
        if (isset($data_predios[0])){
            foreach ($data_predios as $key => $value) {
                $contador_reg = $contador_reg + 1;
                if($contador_reg > $total_reg_pag){
                    $pdf->AddPage();
                    $contador_reg = 1;
                    $pdf->SetXY(10.00125, 10.00125);
                    //dd($x . ' ' . $y);
                    $pdf->SetFillColor(205,205,205);
                    $pdf->SetFont('Arial','B',6);
                    $w = array(8, 13, 50, 20, 25, 25, 45, 45, 15, 16, 18);
                    $pdf->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
                    $pdf->Cell($w[1],5,utf8_decode('NRO CUS'),1,0,'C',true);
                    $pdf->Cell($w[2],5,utf8_decode('DENOMINACION DEL PREDIO'),1,0,'C',true);
                    $pdf->Cell($w[3],5,utf8_decode('DEPARTAMENTO'),1,0,'C',true);
                    $pdf->Cell($w[4],5,utf8_decode('PROVINCIA'),1,0,'C',true);
                    $pdf->Cell($w[5],5,utf8_decode('DISTRITO'),1,0,'C',true);
                    $pdf->Cell($w[6],5,utf8_decode('DIRECCION'),1,0,'C',true);
                    $pdf->Cell($w[7],5,utf8_decode('PROPIETARIO'),1,0,'C',true);
                    $pdf->Cell($w[8],5,utf8_decode('AREA (M2)'),1,0,'C',true);
                    $pdf->Cell($w[9],5,utf8_decode('P. REGISTRAL'),1,0,'C',true);
                    $pdf->Cell($w[10],5,utf8_decode('SITUACION'),1,0,'C',true);
                    $pdf->Ln(); 
                    
                    
                    $pdf->Ln(); 
                }
                // $x=$pdf->GetX();
                // $y=$pdf->GetY();
                // dd($x . ' ' . $y);
                
                //$w = array(8, 13, 70, 20, 25, 25, 60, 60, 20, 20);
                $ITEM	        = $value->ROW_NUMBER_ID;
                $NRO_CUS	    = $value->NRO_RSINABIP;
                $DENOMINACION	= $value->DENOMINACION_INMUEBLE;
                $DEPARTAMENTO	= $value->NOMBRE_DEPARTAMENTO;
                $PROVINCIA	    = $value->NOMBRE_PROVINCIA;
                $DISTRITO	    = $value->NOMBRE_DISTRITO;
                $DIRECCION	    = $value->DIRECCION_INMUEBLE;
                $PROPIETARIO	= $value->NMB_ENTE;
                $AREA	        = $value->AREA_REGISTRAL;
                $PARTIDAREGISTRAL   = $value->NRO_PARTIDA_REGISTRAL;
                $CONDICION      = $value->CONDICION;

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //dd($x . ' ' . $y);
                //$pdf->SetXY($x+$w[0],$y);
                $pdf->MultiCell($w[0],3, $ITEM,0,'L', false);
                $pdf->SetXY($x+$w[0],$y);
                
                
                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[1],$y); 
                $pdf->MultiCell($w[1],3, utf8_decode($NRO_CUS),0,'L');
                $pdf->SetXY($x+$w[1],$y); 
            
                $x=$pdf->GetX(); 
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[2],$y);
                $pdf->MultiCell($w[2],3, utf8_decode($DENOMINACION),0,'L');
                $pdf->SetXY($x+$w[2],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[3],$y);
                $pdf->MultiCell($w[3],3, utf8_decode($DEPARTAMENTO),0,'L');
                $pdf->SetXY($x+$w[3],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[4],$y);
                $pdf->MultiCell($w[4],3, utf8_decode($PROVINCIA),0,'L');
                $pdf->SetXY($x+$w[4],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[5],$y);
                $pdf->MultiCell($w[5],3, utf8_decode($DISTRITO),0,'L');
                $pdf->SetXY($x+$w[5],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[6],$y);
                $pdf->MultiCell($w[6],3, utf8_decode($DIRECCION),0,'L');
                $pdf->SetXY($x+$w[6],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[7],$y);
                $pdf->MultiCell($w[7],3, utf8_decode($PROPIETARIO),0,'L');
                $pdf->SetXY($x+$w[7],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[8],$y);
                $pdf->MultiCell($w[8],3, number_format($AREA, 2, '.', ','),0,'R');
                $pdf->SetXY($x+$w[8],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[9],$y);
                $pdf->MultiCell($w[9],3, utf8_decode($PARTIDAREGISTRAL),0,'L');
                $pdf->SetXY($x+$w[9],$y);

                $x=$pdf->GetX();
                $y=$pdf->GetY();
                //$pdf->SetXY($x+$w[9],$y);
                $pdf->MultiCell($w[10],3, utf8_decode($CONDICION),0,'L');
                $pdf->SetXY($x+$w[10],$y);
                $pdf->Ln(2);
             

                // $pdf->Cell($w[0],3, $ITEM,'LR',0,'L'); 
                // $pdf->Cell($w[1],3, utf8_decode($NRO_CUS),'LR',0,'L');
                // $pdf->Cell($w[2],3, utf8_decode($DENOMINACION),'LR',0,'L');
                // $pdf->Cell($w[3],3,$DEPARTAMENTO,'LR',0,'L');
                // $pdf->Cell($w[4],3,$PROVINCIA,'LR',0,'L');
                // $pdf->Cell($w[5],3,$DISTRITO,'LR',0,'L');
                // $pdf->Cell($w[6],3, utf8_decode($DIRECCION),'LR',0,'L');
                // $pdf->Cell($w[7],3, utf8_decode($PROPIETARIO),'LR',0,'L');
                // $pdf->Cell($w[8],3,$AREA,'LR',0,'L');
                // $pdf->Cell($w[9],3,$PARTIDAREGISTRAL,'LR',0,'L');
                // $pdf->Ln(2);
                
                // $pdf->SetFillColor(255,255,255);
        // $pdf->MultiCell(185,6,utf8_decode('EXPEDIENTES: ' . $CODIGO_EXPEDIENTE), 1,'L',true);
                $pdf->Ln(14);
                //$pdf->Line(10, 20, 196, 20);
                
            }
        }
        //$pdf->Ln(2);
        $fill = false;

        // Línea de cierre
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->Ln();
        //$pdf->Ln(2);

        $pdf->Output(); 
        exit; 
    }

    public function ExportaPrediosExcel($ruc){
        header('Access-Control-Allow-Origin: *');
        if(Config::get('app.APP_LINUX')) 
            //dd('linux');
            //$name = '/mnt/Inventarios_zip/'."Plantilla/";
            $name = storage_path('app/public')."/Plantilla/";
            //$name = "//DESKTOP-1V6NRT4/archivos$/";
            
        else
            //dd('windows');
            //$name = ruta_server()."Plantilla/";     
            $name = "//DESKTOP-1V6NRT4/archivos$/"; 
            //dd($name);  

        $inputFileName = $name."Reporte_Predios_Entidades.xlsx";        
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        
        $objReader->setLoadAllSheets();
        $objPHPExcel = $objReader->load($inputFileName);
        $objPHPExcel->setActiveSheetIndex(0);        

        $contador = 0; 
       
         $data_predios= DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_REPORTE_ENTIDADES ?",
            [  
                $ruc
            ]
        );

        $data = DB::connection('sqlsrv_S_')->select("SELECT TOP 1 ISNULL(nombre_archivo_logo, '')[resultado], nmb_ente from ENTE where codigo = ?", [
            $ruc
        ]);
        $nmb_ente = $data[0]->nmb_ente;

        foreach ($data_predios as $key => $value) {                           
            $item      = $value->ROW_NUMBER_ID;
            $nro_cus     = $value->NRO_RSINABIP;
            $denominacion = $value->DENOMINACION_INMUEBLE;
            $departamento     = $value->NOMBRE_DEPARTAMENTO;
            $provincia     = $value->NOMBRE_PROVINCIA;
            $distrito     = $value->NOMBRE_DISTRITO;
            $direccion     = $value->DIRECCION_INMUEBLE;
            $propietario     = $value->NMB_ENTE;
            $area     = $value->AREA_REGISTRAL;
            $partida     = $value->NRO_PARTIDA_REGISTRAL;
            $condicion     = $value->CONDICION;
            
            $contador++;
            $FILA = 3+$contador;

            $objPHPExcel->getActiveSheet()->SetCellValue('D1', "REPORTE DE PREDIOS: $nmb_ente\nSISTEMA DE INFORMACION NACIONAL DE BIENES ESTATALES");

            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$FILA, $item);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$FILA, $nro_cus);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$FILA, $denominacion);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$FILA, $departamento);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$FILA, $provincia);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$FILA, $distrito);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$FILA, $direccion);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$FILA, $propietario);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$FILA, number_format($area, 2, '.', ','));
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$FILA, $partida);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$FILA, $condicion);
                         
        }         
 
        $NOM_ARVHICO = 'Reporte_Predios_Entidades';        
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="'.$NOM_ARVHICO.'.xlsx"');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $NOM_ARVHICO . ' .xlsx"');
        header('Cache-Control: max-age=0');
        ob_clean();
        flush();

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->setPreCalculateFormulas(true);
        //$objWriter->save(__DIR__."/".$NOM_ARVHICO.".xlsx");
        $objWriter->save('php://output');
    }

    public function ValidarExistenciaRUC($ruc){
        $data = DB::connection('sqlsrv_S_')->select('EXEC PA_SINABIP_VALIDAR_EXISTENCIA_ENTIDAD ?',
            [ 
                $ruc
            ]
        );
        //$resultado = $data[0]->RESULTADO;

        return response()->success($data);     
    }

}
