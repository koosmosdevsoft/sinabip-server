<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;

class estandarizacionController extends Controller
{
   
    public function buscarLiterales(Request $request){               
        $accion = 1; 
        $ent_nombre = $request->ent_nombre;
        $ent_ruc    = $request->ent_ruc;
        $select     = $request->select;
        $page       = $request->page;
        $records    = $request->records;

        if($ent_nombre == null){
            $ent_nombre = '';
        }
        if($ent_ruc == null){
            $ent_ruc = '';
        }
        //dd($ent_nombre);

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_BUSQUEDA_ENTIDADES_LITERALES ?, ?, ?, ?, ?, ?',[$accion, $ent_nombre, $ent_ruc, $select, $page, $records]);
        return response()->success($data);                             
    }

    public function grabarEntidad(Request $request){               
        $accion                 = $request->accion; 
        $ruc                    = $request->ruc;
        $nombreEstandarizado    = $request->nombreEstandarizado;
        $nombreLiteral          = $request->nombreLiteral;
        $siglas                 = $request->siglas;
        $estado                 = $request->estado;
        $org                    = $request->org;
        $nivelGobierno          = $request->nivelGobierno;
        $nivelGeneral           = $request->nivelGeneral;
        $nivelSectorial         = $request->nivelSectorial;
        $organismo              = $request->organismo;
        $usuario                = $request->usuario;
        $fecha                  = $request->fecha;
        $codigo_institucion     = $request->codigo_institucion; 
        $usuarioCreacion        = $request->usuarioCreacion; 

        $data = DB::connection('sqlsrv_S_')->select('exec 
            dbo.PA_SINABIP_GUARDAR_ENTIDADES ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
            [
                $accion, $ruc, $nombreEstandarizado, $nombreLiteral, $siglas,
                $estado, $org, $nivelGobierno, $nivelGeneral, $nivelSectorial,
                $organismo, $usuario, $fecha, $codigo_institucion, $usuarioCreacion
            ]);
        
        return response()->success($data);
    }



    public function VincularCusIndividual(Request $request){
        $nroCUS = $request->nroCUS;
        $nroRUC = $request->nroRUC;
        $usuarioCreacion = $request->usuarioCreacion;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ESTANDARIZACION_CUS_INDIVIDUAL ?,?,?', [$nroCUS, $nroRUC, $usuarioCreacion]);
        
        return response()->success($data);
    }

    public function AdjuntarCusMasivo(Request $request, $data){
        $id_entidad	= $data;
        try{
            $rutaRaiz = Config::get('app.DIR_documentos_csv');   
            $CusMasivo =  "cusMasivo/";  
            $name = $rutaRaiz . $CusMasivo;

            if ($request->hasFile('file0')) {  
                $image = $request->file('file0');
                $ARCHIVO_EXTENSION = 'csv'; 
                $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
                $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
                //$name = "//DESKTOP-1V6NRT4/reclamosDoc$/";
    
                $id_adjuntado = 1;
                
                $result = DB::connection('sqlsrv_S_')->select(
                    "exec PA_SINABIP_ADJUNTAR_ESTANDARIZACION_MASIVA ?,?,?,?",[ 
                        'C', '', 500, 100]
                );
                $id_adjuntado = $result[0]->ID_ADJUNTADO;
                $ARCHIVO_NOMBRE_GENERADO ='CSV_'. strval($id_adjuntado).'_CUS_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
                DB::connection('sqlsrv_S_')->statement("UPDATE TBL_ADJUNTADOS_ESTANDARIZACION_TL set NOMB_ARCHIVO = '" . $ARCHIVO_NOMBRE_GENERADO . "' where ID_ADJUNTADOS_TL = ?", [
                    $id_adjuntado
                ]);

                $namerutaFinal = "cusMasivo/";
                \Storage::disk('local')->put("public/".$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                $origen = storage_path('app/public')."/".$ARCHIVO_NOMBRE_GENERADO;
                $url = " ".$ARCHIVO_NOMBRE_GENERADO;  
                $final = '/mnt/srvinfowww/'.$namerutaFinal.$ARCHIVO_NOMBRE_GENERADO;
                shell_exec('mv '.$origen.' '.$final);
                //file_put_contents($name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                
                return response()->success([
                    "error" => false,
                    "ID" => $id_adjuntado
                ]);
    
            }

        }catch (\Exception $e) { 
            return response()->success([
                "error" => $e->getMessage(),
                "ID" => 0
            ]);
        }         

    }

    public function VincularCusMasivo(Request $request){
        $id_adjuntado = $request->id_adjuntado;
        $ruc = $request->nroRUC;
        $usuario = $request->usuarioCreacion;

        $rutaRaiz = Config::get('app.DIR_documentos_csv');   
        $CusMasivo =  "cusMasivo/";  

        $data = DB::connection('sqlsrv_S_')->select('SELECT NOMB_ARCHIVO FROM TBL_ADJUNTADOS_ESTANDARIZACION_TL WHERE ID_ADJUNTADOS_TL = ?', [$id_adjuntado]);  
        $nombreArchivo = $data[0]->NOMB_ARCHIVO;

        $nombreArchivoCompleto = $rutaRaiz . $CusMasivo . $nombreArchivo;
        //$nombreArchivoCompleto = "//DESKTOP-1V6NRT4/reclamosDoc$/CUS.csv";

        $linea = 0;
        //Abrimos nuestro archivo
        $archivo = fopen($nombreArchivoCompleto, "r");
        //Lo recorremos
        $codigos ='';
        while (($datos = fgetcsv($archivo, ",")) == true) 
        {
            $num = count($datos);
            //Recorremos las columnas de esa linea
            for ($columna = 0; $columna < $num; $columna++)
            {
                //$codigos = $codigos . $datos[$columna] . ',';
                if(is_numeric($datos[$columna])){
                    $codigos = $codigos . $datos[$columna] . ',';    
                }

                //echo $datos[$columna] . "\n";
            }
        }
        
        //dd($codigos);
        //Cerramos el archivo
        //echo $codigos;
        fclose($archivo);

        $codigos = substr($codigos, 0, strlen($codigos)-1);
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_VINCULAR_LISTADO_CUS ?,?,?,?', 
        [
            $id_adjuntado, $ruc, $codigos, $usuario 
        ]);
        return response()->success($data);
    }

 
   

    public function VincularActoIndividual(Request $request){
        $nroCodigo = $request->nroCodigo;
        $nroRUC = $request->nroRUC;
        $usuarioCreacion = $request->usuarioCreacion;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ESTANDARIZACION_ACTO_INDIVIDUAL ?,?,?', [$nroCodigo, $nroRUC, $usuarioCreacion]);
        
        return response()->success($data);
    }

    public function AdjuntarActoMasivo(Request $request, $data){
        $id_entidad	= $data;
        try{
            $rutaRaiz = Config::get('app.DIR_documentos_csv');   
            $ActoMasivo =  "actoMasivo/";  
            $name = $rutaRaiz . $ActoMasivo;

            if ($request->hasFile('file0')) {  
                $image = $request->file('file0');
                $ARCHIVO_EXTENSION = 'csv'; 
                $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
                $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
                //$name = "//DESKTOP-1V6NRT4/reclamosDoc$/";
    
                $id_adjuntado = 1;
                
                $result = DB::connection('sqlsrv_S_')->select(
                    "exec PA_SINABIP_ADJUNTAR_ESTANDARIZACION_MASIVA ?,?,?,?",[ 
                        'A', '', 500, 100
                        ]
                );
                $id_adjuntado = $result[0]->ID_ADJUNTADO;
                $ARCHIVO_NOMBRE_GENERADO ='CSV_'. strval($id_adjuntado).'ACTO'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
                DB::connection('sqlsrv_S_')->statement("UPDATE TBL_ADJUNTADOS_ESTANDARIZACION_TL set NOMB_ARCHIVO = '" . $ARCHIVO_NOMBRE_GENERADO . "' where ID_ADJUNTADOS_TL = ?", [
                    $id_adjuntado
                ]);

                $namerutaFinal = "ActoMasivo/";
                \Storage::disk('local')->put("public/".$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                $origen = storage_path('app/public')."/".$ARCHIVO_NOMBRE_GENERADO;
                $url = " ".$ARCHIVO_NOMBRE_GENERADO;  
                $final = '/mnt/srvinfowww/'.$namerutaFinal.$ARCHIVO_NOMBRE_GENERADO;
                shell_exec('mv '.$origen.' '.$final);
                //file_put_contents($name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                
                return response()->success([ 
                    "error" => false,
                    "ID" => $id_adjuntado
                ]);
    
            }

        }catch (\Exception $e) { 
            return response()->success([
                "error" => $e->getMessage(),
                "ID" => 0
            ]);
        }         

    }

    public function VincularActoMasivo(Request $request){
        $id_adjuntado = $request->id_adjuntado;
        $ruc = $request->nroRUC;    
        $usuario = $request->usuarioCreacion;

        $rutaRaiz = Config::get('app.DIR_documentos_csv');   
        $ActoMasivo =  "actoMasivo/";  

        $data = DB::connection('sqlsrv_S_')->select('SELECT NOMB_ARCHIVO FROM TBL_ADJUNTADOS_ESTANDARIZACION_TL WHERE ID_ADJUNTADOS_TL = ?', [$id_adjuntado]);  
        $nombreArchivo = $data[0]->NOMB_ARCHIVO;
        
        //$nombreArchivoCompleto = "//DESKTOP-1V6NRT4/reclamosDoc$/ACTO.csv";
        $nombreArchivoCompleto = $rutaRaiz . $ActoMasivo . $nombreArchivo;
        
        $linea = 0;
        //Abrimos nuestro archivo
        $archivo = fopen($nombreArchivoCompleto, "r");
        //Lo recorremos
        $codigos ='';
        while (($datos = fgetcsv($archivo, ",")) == true) 
        {
            $num = count($datos);
            //Recorremos las columnas de esa linea
            for ($columna = 0; $columna < $num; $columna++)
            {
                
                    $codigos = $codigos . $datos[$columna] . ',';    
               
               
            }
        }
        
        //Cerramos el archivo
        
        fclose($archivo);
        //dd(substr($codigos, 0, strlen($codigos)-1));
        
        
        $codigos = substr($codigos, 0, strlen($codigos)-1);
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_VINCULAR_LISTADO_CODIGO_ACTO ?,?,?,?', 
        [
            $id_adjuntado, $ruc, $codigos, $usuario
        ]);
        return response()->success($data);
    }


    public function VincularNiveles(Request $request){
        $nroRUC = $request->nroRUC;
        $RucUnidadEjecutora = $request->RucUnidadEjecutora;
        $RucUnidadxxx = $request->RucUnidadxxx;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_VINCULACION_NIVELES ?,?,?', [$nroRUC, $RucUnidadEjecutora, $RucUnidadxxx]);
        
        return response()->success($data); 
    }


    public function cargarListaGobRegionales(Request $request){

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTA_GOBIERNO_REGIONALES_COMPETENCIA');
        return response()->success($data);
    }

    public function cambiar_gobRegional(Request $request){
        $idgobiernoRegional = $request->idgobiernoRegional;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_MOSTRAR_DATOS_GOBIERNO_REGIONAL ?', [$idgobiernoRegional]);
        return response()->success($data);
    }

    public function VincularPredios(Request $request){
        $nroRUCEntidadPrimerNivel = $request->nroRUCEntidadPrimerNivel;
        $nroRUCGobReg = $request->nroRUCGobReg;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_VINCULACION_PREDIOS_UBICACION_GEOGRAFICA ?,?', [$nroRUCEntidadPrimerNivel, $nroRUCGobReg]);        
        return response()->success($data);
    }




    public function cargarListaNivelGobierno(Request $request){

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTA_NIVEL_GOBIERNO');
        return response()->success($data);
    }

    public function cargarListaNivelGeneral(Request $request){

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTA_NIVEL_GENERAL');
        return response()->success($data);
    }

    public function cargarListaNivelSectorial(Request $request){

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTA_NIVEL_SECTORIAL');
        return response()->success($data);
    }

    public function cargarListaOrganismo(Request $request){

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTA_ORGANISMOS');
        return response()->success($data);
    }

    public function obtenerDescripcionEntidad(Request $request){
        $ruc = $request->ruc;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_DESCRIPCION_ENTIDAD_ESTANDARIZADA ?', 
        [
            $ruc
        ]);
        return response()->success($data);
    }
    
          
}
