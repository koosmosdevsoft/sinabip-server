<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;

class UnidadController extends Controller
{
   
    //DATOS GENERALES
    public function ListadoDocumentos(Request $request){                       

        $tipodocumento   = $request->tipodocumento;
       
        if ($request->numerodocumento=="")
            {$numerodocumento='';}
        else 
            { $numerodocumento = $request->numerodocumento; }
        $codigousuario   = $request->codigousuario;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DOCUMENTOS_MEMO_SI ?,?,?',[
            $tipodocumento,
            $numerodocumento,
            $codigousuario
        ]);
        return response()->success($data);                             
    }
    public function TecnicoLegal($codusu){               
    
        $dataT = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_RESPONSABLES_TECNICO_LEGAL ?,?',[$codusu, 'T']);
        $dataL = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_RESPONSABLES_TECNICO_LEGAL ?,?',[$codusu, 'L']);
        return response()->success([
            "tecnico" => (count($dataT) > 0) ?$dataT : [],
            "legal" => (count($dataL) > 0) ?$dataL : []
        ]);      
        
    }

    public function EliminarUnidad($codigo){               
    
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ELIMINACION_UD ?',[$codigo]);
        return response()->success($data);                             
    }

    public function GenerarUnidad(Request $request){                       

        $documental      = $request->documental;       
        $tipodocumento   = $request->tipodocumento;
        $codigodocumento = $request->codigodocumento; 
        $numerodocumento = $request->numerodocumento;      
        $codtecnico      = $request->codtecnico;
        $codlegal        = $request->codlegal;
        $codigousuario   = $request->codigousuario;
        $externo        = $request->externo; 
        
        
        
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_UNIDAD_DOCUMENTAL ?,?,?,?,?,?,?,?',[
            $documental,
            $tipodocumento,
            $codigodocumento,
            $numerodocumento,
            $codtecnico,
            $codlegal,
            $codigousuario, 
            $externo

        ]);
        return response()->success($data);                             
    }

    public function ListadoUnidad(Request $request){               
        $numerodocumento = $request->numerodocumento; 
        $tipodocumento   = $request->tipodocumento;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_UNIDADES_DOCUMENTALES ?,?',[$numerodocumento,$tipodocumento]);
        return response()->success($data);                             
    }

    public function CargarVerificacion($cod_documental){               
               
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_VERIFICACION_DOCUMENTOS_TECNICO_LEGAL ?',[$cod_documental]);
        return response()->success($data);                             
    }

    public function Documentos($tipo){               
               
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_DOCUMENTOS_PARA_UD ?',[$tipo]);
        return response()->success($data);                             
    }
    public function BuscarDocumento(Request $request){               
        $coddocunental = $request->coddocunental; 
        $codtipodoc    = $request->codtipodoc;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_BUSQUEDA_OTROS_TIPOS_DOCUMENTOS ?,?',[$coddocunental,$codtipodoc]);
        return response()->success($data);                             
    }
    public function AgregarDocumento(Request $request){               
        $coddocunental = $request->coddocunental; 
        $codtipodoc    = $request->codtipodoc;

        $data = DB::connection('sqlsrv_S_')->statement('exec dbo.PA_SINABIP_REGISTRO_OTROS_TIPOS_DOCUMENTOS ?,?',[$coddocunental,$codtipodoc]);
        return response()->success($data);                             
    }
    public function EliminarDocumento(Request $request){               
        $coddocunental = $request->coddocunental; 
        $codtipodoc    = $request->codtipodoc;

        $data = DB::connection('sqlsrv_S_')->statement('exec dbo.PA_SINABIP_ELIMINACION_OTROS_TIPOS_DOCUMENTOS ?,?',[$coddocunental,$codtipodoc]);
        return response()->success($data);                             
    }

    public function BuscarUD(Request $request){                
        $buscarUD    = $request->codud;
        
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_BUSQUEDA_UNIDAD_DOCUMENTAL ?',[ $buscarUD ]);
        return response()->success($data);                             
    }

    public function BusquedaCUSparaActualizar(Request $request){                
        $nroCUS = $request->nroCUS; 

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_BUSQUEDA_CUS_ACTUALIZAR ?',[$nroCUS]);
        return response()->success($data);                            
    }

    public function BuscarCodigoPredio($nroCUS){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT CODIGO_INTERNO[aux_codinterno], 
        CASE TIPO_ASIENTO      
            WHEN 'E' THEN 'ESTATAL'      
            WHEN 'M' THEN 'MUNICIPAL'      
            WHEN 'O' THEN 'EMPRESARIAL'
            WHEN 'P' THEN 'PROVISIONAL'
            WHEN 'C' THEN 'COFOPRI' 
        END[DSC_ASIENTO]
        FROM LIBROS WHERE NRO_RSINABIP = $nroCUS");
        return response()->success($data);                
    }

    public function ObtenerSuma($codigopredio){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT SUM(VALOR_ESTIMADO) [suma_total] FROM DETALLE_CONSTRUCCIONES WHERE  CODIGO_INTERNO = $codigopredio");
        return response()->success($data);                
    }

    public function BuscarCUS($codigopredio){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT NRO_RSINABIP [NRO_CUS] FROM LIBROS WHERE CODIGO_INTERNO = $codigopredio");
        return response()->success($data);                
    }

    public function ValidarUD($nroCUS){                
           $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_VALIDAR_UD_ACTUALIZAR ?',[$nroCUS]);
        return response()->success($data);                
    }

    public function TerminarProceso(Request $request){                 
        $CODIGO_UDOCUMENTAL = $request->CODIGO_UDOCUMENTAL;
        $USUARIO_MODIFICACION = $request->USUARIO_MODIFICACION;
        $CODIGO_INTERNO = $request->CODIGO_INTERNO;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_TERMINAR_PROCESO_UD ?, ?, ?',[$CODIGO_UDOCUMENTAL, $USUARIO_MODIFICACION, $CODIGO_INTERNO]);
        return response()->success($data);                            
    }

    public function EnviarSbn(Request $request){                
        $NRO_SINABIP = $request->NRO_SINABIP;
        $COD_USER_APRO = $request->COD_USER_APRO;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ENVIAR_SBN ?, ?',[$NRO_SINABIP, $COD_USER_APRO]);
        return response()->success($data);                            
    }

    public function ActualizarTipoDetalleAccionUD(Request $request){                
        $codigo_udocumental = $request->codigo_udocumental;
        $tipoAccion         = $request->tipoAccion;
        $detalleRegistro    = $request->detalleRegistro;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ACTUALIZAR_TIPO_DETALLE_UD ?, ?, ?',[$codigo_udocumental, $tipoAccion, $detalleRegistro]);
        return response()->success($data);                            
    }

    public function ActualizaNotificacion(Request $request){                
        $CODIGO_UDOCUMENTAL = $request->CODIGO_UDOCUMENTAL;
        $USUARIO_MODIFICACION = $request->USUARIO_MODIFICACION;
        $CODIGO_INTERNO = $request->CODIGO_INTERNO;
        $ESTADO = $request->ESTADO;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REGISTRO_ACTUALIZACION_NOTIFICACIONES ?, ?, ?, ?, ?',[$CODIGO_INTERNO, $CODIGO_UDOCUMENTAL, $USUARIO_MODIFICACION, $USUARIO_MODIFICACION, $ESTADO]);
        return response()->success($data);                            
    }

    public function ValidarEstadoUD($nroCUS){                
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_VALIDAR_ESTADO_UD ?", [$nroCUS]);
        return response()->success($data);                
    }

    public function verificacionExistenciaDocsTL($codUD){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT COUNT(codigo_udocumental)[RESULTADO] FROM UNIDAD_DOCUMENTAL_DOC WHERE codigo_udocumental = $codUD");
        return response()->success($data);                
    }

    public function traerProfesionalesUD($codUD){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT resp_evaluacion_tecnica[TECNICO], resp_evaluacion_legal[LEGAL] FROM UNIDAD_DOCUMENTAL WHERE codigo_udocumental = $codUD");
        return response()->success($data);                
    }

    public function actualizarProfesionales(Request $request){ 
        $codud = $request->codud;               
        $codtecnico = $request->codtecnico;
        $codLegal = $request->codLegal;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_ACTUALIZAR_PROFESIONALES ?, ?, ?',[$codud, $codtecnico, $codLegal]);
        return response()->success($data);                            
    }

    public function existeCUS($codcus){                
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_EXISTE_CUS ?", [$codcus]);
        return response()->success($data);                
    }

    public function CargarPrediosExternos(Request $request){   
       
        $codigoUsuario  = $request->codigousuario;
        $page           = $request->page;
        $records        = $request->records;
        $ruc            = $request->ruc;  

        if ($request->cus==null)    
        { $cus = '';}
        else 
        { $cus = $request->cus;}          
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_PREDIOS_POR_ENTIDAD ?,?,?,?,?', [$codigoUsuario, $page, $records,$cus, $ruc]);
        //$data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_LISTADO_PREDIOS_ENTIDADES_EXTERNAS ?,?,?,?,?', [$codigoUsuario, $page, $records,$cus, $ruc]);
        return response()->success($data);                
    }

    public function openverDocumentos($codigoudocumental){                
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_LISTADO_VERIFICADOR_DOCUMENTOS ?", [$codigoudocumental]);
        return response()->success($data);                
    }

    public function openDocumentosxDefecto(){                
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_LISTADO_VERIFICADOR_DOCUMENTOS_POR_DEFECTO");
        return response()->success($data);                
    }
    

    public function ObtenerDatosDocumento(Request $request){               
        $codtipodoc    = $request->codtipodoc;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_OBTENER_DATOS_DOCUMENTO ?',[$codtipodoc]);
        return response()->success($data);                             
    }

    public function CargarListadoAuditoriaPredios(Request $request){   
       
        $cus        = $request->cus;
        $fechaini   = $request->fechaini;
        $fechafin   = $request->fechafin;
        $page       = $request->page;
        $records    = $request->records;  
        if ($request->cus==null)    
            { $cus = '';}
        else 
            { $cus = $request->cus;}         
            $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_AUDITORIA_PREDIOS ?,?,?,?,?', [$cus, $fechaini, $fechafin, $page, $records]);
            return response()->success($data);                
    }



    

    public function openDetalleAuditoria(Request $request){ 
        $accion         = $request->accion;               
        $codigo_interno = $request->codigo_interno;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_DETALLE_AUDITORIA_PREDIO ?,?,?,?,?,?',[
            $accion,
            $codigo_interno,
            '',
            '',
            0,
            0 
            ]);
        return response()->success($data);            
    }

    public function validarExistenciaRUC($nroRUC){                
        $data = DB::connection('sqlsrv_S_')->select("SELECT TOP 1 codigo FROM ENTE WHERE CODIGO = '$nroRUC'");
        return response()->success($data);               
    }
    

    

}
