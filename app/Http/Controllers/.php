<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;

class NotificacionController extends Controller
{
   
    //DATOS GENERALES    
    public function ListadoNotificaciones(Request $request){  
      
        $codigo_entidad = $request->codigo_entidad;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_LISTADO_NOTIFICACIONES ?",[$codigo_entidad]
        );
        
        return response()->success([
            "listadoNotificaciones" => (count($data) > 0) ?$data : []
        ]);
    }


}

