<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{
   
    //DATOS GENERALES
    public function traerData(Request $request){                   
        $anio = $request->anio;
        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_REPORTE_UNIDADES_DOCUMENTALES ?', [$anio]);
    
        //dd($data);
        return response()->success([
            "resultado" => $data
        ]);                         
    }

    public function detalleporDepartamento($codTecnico, $codLegal, $mes, $anio){
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_UNIDADES_DOCUMENTALES_BRIGADA_POR_MES '$codTecnico', '$codLegal', '$mes', '$anio'");
        
        return response()->success([
            "resultado" => $data
        ]); 
    }

    public function detalleporEtapas($codTecnico, $codLegal, $etapa, $anio){
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_UNIDADES_DOCUMENTALES_BRIGADA_POR_ETAPAS '$codTecnico', '$codLegal', '$etapa', '$anio'");
        
        return response()->success([
            "resultado" => $data
        ]); 
    }
    

  

    public function descargarXLS_detalleporDepartamento($codTecnico, $codLegal, $mes, $anio){
        
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_UNIDADES_DOCUMENTALES_BRIGADA_POR_MES '$codTecnico', '$codLegal', '$mes', '$anio'");
        
        $arrayTrans = [];
        
        $arrayTrans[] = [
            'ITEM',
            'NRO U. DOCUMENTAL',
            'DOCUMENTO',
            'CUS',
            'FECHA U. DOCUMENTAL'
        ];  
        foreach ($data as $key => $value) {
            $arrayTrans[] = [ 
                $value->ROW_NUMBER_ID,
                $value->Nro_UDocumental,
                $value->Documento,
                $value->CUS,
                $value->fecini_udocumental

                // strval('=""&"'.$value->CODIGO_PATRIMONIAL.'"'),
                // strval('="'.$vsalue->CODIGO_PATRIMONIAL.'"'),
               
            ];
        }
        //dd($data);
        $collection = new CollectionExport();
        $collection->load($arrayTrans);
        return Excel::download($collection, 'listadoUDporMes.xlsx');

        // return response()->success([
        //     "resultado" => $data
        // ]); 
    }

    public function descargarXLS_detalleporEtapas($codTecnico, $codLegal, $etapa, $anio){
        
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_UNIDADES_DOCUMENTALES_BRIGADA_POR_ETAPAS '$codTecnico', '$codLegal', '$etapa', '$anio'");
        
        $arrayTrans = [];
        
        $arrayTrans[] = [
            'ITEM',
            'NRO U. DOCUMENTAL',
            'DOCUMENTO',
            'CUS',
            'FECHA U. DOCUMENTAL'
        ];  
        foreach ($data as $key => $value) {
            $arrayTrans[] = [ 
                $value->ROW_NUMBER_ID,
                $value->Nro_UDocumental,
                $value->Documento,
                $value->CUS,
                $value->fecini_udocumental
               
            ];
        }
        //dd($data);
        $collection = new CollectionExport();
        $collection->load($arrayTrans);
        return Excel::download($collection, 'listadoUDporEtapa.xlsx');

        // return response()->success([
        //     "resultado" => $data
        // ]); 
    }

    
    public function detallesinPoligonos($anio, $accion){
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_UNIDADES_DOCUMENTALES_DETALLES_SIN_POLIGONO '$anio', '$accion'");
        
        return response()->success([
            "resultado" => $data
        ]); 
    }
    
    public function descargarXLS_detallesinPoligonos($anio, $accion){
        
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_UNIDADES_DOCUMENTALES_DETALLES_SIN_POLIGONO '$anio', '$accion'");
        
        $arrayTrans = [];
        
        $arrayTrans[] = [
            'ITEM',
            'NRO U. DOCUMENTAL',
            'DOCUMENTO',
            'CUS',
            'FECHA U. DOCUMENTAL'
        ];  
        foreach ($data as $key => $value) {
            $arrayTrans[] = [ 
                $value->ROW_NUMBER_ID,
                $value->Nro_UDocumental,
                $value->Documento,
                $value->CUS,
                $value->fecini_udocumental

                // strval('=""&"'.$value->CODIGO_PATRIMONIAL.'"'),
                // strval('="'.$vsalue->CODIGO_PATRIMONIAL.'"'),
               
            ];
        }
        //dd($data);
        $collection = new CollectionExport();
        $collection->load($arrayTrans);
        return Excel::download($collection, 'listadoUDSinPoligonos.xlsx');

        // return response()->success([
        //     "resultado" => $data
        // ]); 
    }
  

    
    
}

