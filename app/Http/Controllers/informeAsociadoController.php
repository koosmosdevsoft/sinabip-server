<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;

class informeAsociadoController extends Controller
{
   
    public function ListadoInformesAsociados(Request $request){               
        $codigo_interno    = $request->codigo_interno;

        $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_LISTADO_INFORMES_ASOCIADOS ?',[$codigo_interno]);
        return response()->success($data);                             
    }

       public function Detalle_Informes_Asociados($accion,$codigo_interno,$sol_ingreso,$cod_preventivo,$cod_construccion){ 
       	 $data = DB::connection('sqlsrv_S_')->select('exec dbo.PA_SINABIP_DETALLE_INFORMES_ASOCIADOS ?,?,?,?,?',[$accion,$codigo_interno,$sol_ingreso,$cod_preventivo,$cod_construccion]);
        return response()->success($data);                             
    }

    
       
}

