<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;

class AporteFotosController extends Controller
{
   
    //DATOS GENERALES    
    public function ListadoAporteFotos(Request $request){  
        $codigo_interno = $request->codigo_interno; 
            
        // $data = DB::connection('sqlsrv_S_')->select(
        //     "exec dbo.PA_SINABIP_LISTADO_APORTE_FOTOS ?",[$codigo_interno]
        // );
        
        $data = DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_NOMBRES_FOTOS_CUS ?",[$codigo_interno]
        );
        
        $dato = array();
        $node = '';
        $ruta_arr = array();
        $dds = array();
        $compatible = array();
        foreach($data as $da){ 
            $ruta = str_replace('\\', '/',$da->RUTA);
            $dir = str_replace('//srv-info/documentos$/','/mnt/srvinfo_doc/',$ruta);
            if($node != $dir){
                $node = $dir;
                $ruta_arr[] = $node;
            }else{
                $node = '';
            }
            $dato[] = $da->NOMBRE_FOTO;
        }
        foreach($ruta_arr as $rut){
            $dd = $this->rutas($rut);
            if($dd){
                $dds[] = $this->rutas($rut);
            }
        }
        $contador = 0;
        if(count($dds)>0){
            foreach($dds as $unos ){
                foreach($unos as $uno){
                    foreach($dato as $dos){
                        if($uno['file'] == $dos){
                            $contador++;
                            $compatible[] = array(
                                'ROW_NUMBER_ID' => $contador,
                                'ruta'          => $uno['Nombre'],
                                'NMB_FOTO'      => $uno['archivo'],
                            );
                        }
                    }
                }
            }
        }
        
        return response()->success([
            "listadoAporteFotos" => (count($compatible) > 0) ?$compatible : []
        ]);
    }

    public function VisualizarFoto(Request $request,$archivo,$codigo){
        $ruta_fotos = '';
        $data = DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_NOMBRES_FOTOS_CUS ?",[$codigo]
        );
        $node = '';
        $name = '';

        if(Config::get('app.APP_LINUX')) {
            foreach($data as $da){
                $ruta = str_replace('\\', '/',$da->RUTA);
                //$dir = str_replace('//srv-info02/documentos$/','/mnt/documentos/',$ruta);
                $dir = str_replace('//srv-info/documentos$/','/mnt/srvinfo_doc/',$ruta);
                $file = explode('_',strrev($archivo)); 
                $file = strlen($file[0]);
                $fileNew = substr($archivo, 0, -$file);
                if($da->NOMBRE_FOTO == $fileNew){
                    $name = $dir;
                }
            }
        }else{
            $ruta_fotos = Config::get('app.ruta_fotosWIN');
            $name = $ruta_fotos . "AsientosPrediales/";
            if(!file_exists($name.$archivo)){
                $ruta_fotos = Config::get('app.ruta_fotosWINProd');
                $name = $ruta_fotos . "AsientosPrediales/";
            }
        }
        $contents = $name.$archivo;
		header("Content-type: application/jpg");
		header('Content-Disposition: inline; filename="'.$contents.'"');
		$data = file_get_contents($contents);
		echo $data;
		exit;
    }

    public function Guardar_imagenes(Request $request){  
        //print_r($request->uploadFile);die();
        $maxFileSize = Config::get('app.maxFileSize');
        $valid_extension = array("jpg");
        $codigo_interno = $request->codigo_interno; 
        $error = false;
        $url = '';

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_VALIDAR_EXISTENCIA_UD ?",[$codigo_interno]
        );
        $resultado = $data[0]->RESULTADO;
        
        if( $resultado == 'EXISTE'){
            if($request->hasFile('uploadFile')){
                $files = $request->file('uploadFile');
               
                /* VALIDACION DE PESO y EXTENSION DE IMAGENES */
                foreach($files as $key => $file)
                {         
                    $extension = $files[$key]->getClientOriginalExtension();
                    
                    if(in_array(strtolower($extension),$valid_extension)){
                        $imageSize = $files[$key]->getSize();
                        if($imageSize > $maxFileSize){
                            $error = true;
                            $data = 'Solo se permite imagenes con peso hasta 1.5MB';
                            $url = '';
                            return response()->success([
                                "error"     => $error,
                                "resultado" => $data,
                                "imagen"    => $url
                            ]);
                        }
                    }else{
                        $error = true;
                        $data = 'Inválido Extensión de Imagen';
                        $url = '';
                        return response()->success([
                            "error"     => $error,
                            "resultado" => $data,
                            "imagen"    => $url
                        ]);
                    }
                }
                /* FIN: VALIDACION DE PESO y EXTENSION DE IMAGENES */
                
                
                /* OBTENER CORRELATIVO DE ULTIMA IMAGEN */
                $codigo_interno = $request->codigo_interno; 
                //dd($codigo_interno);
                $data = DB::connection('sqlsrv_S_')->select(
                    "exec PA_SINABIP_NOMBRES_FOTOS_CUS ?",[$codigo_interno]
                );
                //dd($data[0]->RUTA);
                
                $dato = array(); 
                $node = '';
                $ruta_arr = array();
                $dds = array();
                $compatible = array();
                
                $ruta = str_replace('\\', '/',$data[0]->RUTA);
                $dir = str_replace('//srv-info/documentos$/','/mnt/srvinfo_doc/',$ruta);
                //dd($dir);
                if($node != $dir){
                    $node = $dir;
                    $ruta_arr[] = $node;
                }else{
                    $node = '';
                }
                $dato[] = $data[0]->NOMBRE_FOTO;
                //dd($dato);
                foreach($ruta_arr as $rut){
                    $dd = $this->rutas($rut);
                    //dd($dd);
                    if($dd){
                        $dds[] = $this->rutas($rut);
                    }
                }
                
                $contador = 0;
                if(count($dds)>0){
                    foreach($dds as $unos ){
                        foreach($unos as $uno){
                            foreach($dato as $dos){
                                if($uno['file'] == $dos){
                                    $contador++;
                                    $compatible[] = array(
                                        'ROW_NUMBER_ID' => $contador,
                                        'ruta'          => $uno['Nombre'],
                                        'NMB_FOTO'      => $uno['archivo'],
                                    );
                                }
                            }
                        }
                    }
                }
                $contadorImagenes = count($compatible) + 1;
                $nombre_carpeta = 'AsientosPrediales'; 
                //dd($contadorImagenes);
                /* FIN: CORRELATIVO DE ULTIMA IMAGEN */


                /* SUBIDA DE IMAGENES */
                foreach($files as $key => $file)
                {         
                    $name_origen = $files[$key]->getClientOriginalName();
                    $path = "AsientosPrediales/"; 

                    $nmb_archivo = $dato[0] . $contadorImagenes . '.JPG';
                    //dd($nmb_archivo);
                    //return;
                    if(Config::get('app.APP_LINUX') == true){ 
                        
                        // \Storage::disk('local')->put("public/".$nmb_archivo, file_get_contents($file));
                        // $origen = storage_path('app/public')."/".$nmb_archivo;
                        // $url = '/mnt/srvinfo_doc/'.$path.$nmb_archivo;
                        // shell_exec('mv '.$origen.' '.$url);  
                        
  
                        $url = Storage::disk('public_pruebas')->putFileAs($nombre_carpeta, $files[$key], $nmb_archivo);
                        $url_temp = storage_path("app/public/").$nombre_carpeta."/".$nmb_archivo;
                        $rutaFinal = '/mnt/srvinfo_doc/' . $nombre_carpeta . "/".$nmb_archivo;
                        exec("mv '$url_temp' '$rutaFinal'", $output, $retrun_var); 
                        //dd($url_temp . ' - ' . $rutaFinal );
                    }else{ 
                        dd('windows');
                        $url = Storage::disk('public')->putFileAs($path, $file, $nmb_archivo);
                    }
                    $contadorImagenes = $contadorImagenes + 1;

                }
                $data = "Archivos Subidos Correctamente";
            }else{
                $data = 'Error de subida de archivos';
            }
        }else{
            $data = $resultado;  //VALOR DEL RESULTADO: NO EXISTE UD
            $error = true;
        }

        
        return response()->success([ 
            "error"     => $error,
            "resultado" => $data,
            "imagen"    => $url
        ]);
       
    }




    // public function Guardar_imagenes(Request $request){
    //     $maxFileSize = Config::get('app.maxFileSize');
    //     $valid_extension = array("jpg");
    //     $codigo_interno = $request->codigo_interno; 
    //     $error = false;
    //     $url = '';
    //     //$codigo_usuario = $request->codigo_usuario;

    //     $data = DB::connection('sqlsrv_S_')->select(
    //         "exec dbo.PA_SINABIP_VALIDAR_EXISTENCIA_UD ?",[$codigo_interno]
    //     );
    //     $resultado = $data[0]->RESULTADO;
    //     $codigo_udocumental = $data[0]->CODIGO_UDOCUMENTAL;
    //     if( $resultado == 'EXISTE'){
    //         if($request->hasFile('uploadFile')){
    //             $files = $request->file('uploadFile');
                
    //             /* VALIDACION DE PESO y EXTENSION DE IMAGENES */
    //             foreach($files as $key => $file)
    //             {         
    //                 $extension = $files[$key]->getClientOriginalExtension();
    //                 if(in_array(strtolower($extension),$valid_extension)){
    //                     $imageSize = $files[$key]->getSize();
    //                     if($imageSize > $maxFileSize){
    //                         $error = true;
    //                         $data = 'Solo se permite imagenes con peso hasta 1.5MB';
    //                         $url = '';
    //                         return response()->success([  
    //                             "error"     => $error,
    //                             "resultado" => $data,
    //                             "imagen"    => $url
    //                         ]);
    //                     }
    //                 }else{
    //                     $error = true;
    //                     $data = 'Inválido Extensión de Imagen';
    //                     $url = '';
    //                     return response()->success([
    //                         "error"     => $error,
    //                         "resultado" => $data,
    //                         "imagen"    => $url
    //                     ]);
    //                 }
    //             }
                
                
    //             /* SUBIDA DE IMAGENES */
    //             foreach($files as $key => $file)
    //             {         
    //                 $name_origen = $files[$key]->getClientOriginalName();
    //                 /* REGISTRO EN BD */
    //                 $data1 = DB::connection('sqlsrv_S_')->select(
    //                     "exec dbo.PA_SINABIP_GUARDAR_APORTE_FOTOS_UD_CUS ?,?,?",[
    //                         $codigo_interno,
    //                         $codigo_udocumental,
    //                         100
    //                     ]
    //                 );
    //                 $resultado2 = $data1[0]->RESULTADO;  //RESULTADO: OK
    //                 $nmb_archivo = $data1[0]->NMB_FOTO;
    //                 if( $resultado2 == 'OK'){
    //                     /* COPIAR FOTOS JPG */
    //                     $path = "AsientosPrediales/";

    //                     //$numerador_image = $numerador_image + 1;
    //                     if(Config::get('app.APP_LINUX') == true){
                            
    //                         \Storage::disk('local')->put("public/".$nmb_archivo, file_get_contents($file));
    //                         $origen = storage_path('app/public')."/".$nmb_archivo;
    //                         $url = '/mnt/srvinfo_doc/'.$path.$nmb_archivo;
    //                         shell_exec('mv '.$origen.' '.$url);

    //                     }else{
    //                         $url = Storage::disk('public')->putFileAs($path, $file, $nmb_archivo);
    //                     }
                        
    //                 }else{
    //                     $mensaje[] = array(
    //                         'error'  => true,
    //                         'nombre' => $name_origen,
    //                         'url'    => $name_origen
    //                     );
    //                 }
    //             }
    //             $data = "Archivos Subidos Correctamente";
    //         }else{
    //             $data = 'Error de subida de archivos';
    //         }
    //     }else{
    //         $data = $resultado;  //VALOR DEL RESULTADO: NO EXISTE UD
    //         $error = true;
    //     }

        
    //     return response()->success([
    //         "error"     => $error,
    //         "resultado" => $data,
    //         "imagen"    => $url
    //     ]);
       
    // }

    public function EliminarAporteFotos(Request $request){
        
        $codigo_interno = $request->codigo_interno;
        $NMB_FOTO = $request->NMB_FOTO;

        /*
        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_ELIMINACION_APORTE_FOTO ?,?",[
                $codigo_interno, 
                $NMB_FOTO
            ]
        );
        */
        
        $nombreArchivo_a_Eliminar = $NMB_FOTO;
        $nameCarpeta = "AsientosPrediales/";

        if(Config::get('app.APP_LINUX')){ 
            // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            // $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
            // $url = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
            // shell_exec('cp '.$origen.' '.$url);
            
            $Filefinal = '/mnt/srvinfo_doc/'.$nameCarpeta.$nombreArchivo_a_Eliminar;

            if(file_exists($Filefinal)){
                unlink($Filefinal);
            }
            $url = $nombreArchivo_a_Eliminar;   

        }else{ 
            $url = Storage::disk('public')->delete($nameCarpeta.$nombreArchivo_a_Eliminar);
        }
        
        return response()->success([
            //"resultado" => (count($data) > 0) ?$data : [],
            "resultado" => 'OK',
            "imagen" => $url
        ]);
    }
    

    private function mensajeValidacionUpload($mensaje,$request) : array
    {
        $msg = $mensaje->errors()->all();
        $TypeFile = $request->file('uploadFile')->getClientOriginalExtension();
        $arrMsg = array();
        foreach($msg as $mm){
            switch($mm){
                case "The upload file may not be greater than $this->fileSize kilobytes.":
                    $arrMsg['file'] = 'No se acepta archivos mayores a '.($this->fileSize/1024).' MB.';
                break;
                case "The tipo field is required.":
                    $arrMsg['tipo'] = 'El tipo de campo es requerido';
                break;
                case 'validation.mime_types':
                    $arrMsg['file'] = 'El tipo de archivo .'.strtoupper($TypeFile).' no se admite';
                break;
                case 'The upload file field is required.':
                    $arrMsg['file'] = 'El archivo es requerido';
                break;
            }
        }
        $arrMsgFinal = array(
            'error'  => true,
            'mensaje' => $arrMsg
        );
        return $arrMsgFinal;
    }

    private function validacionTipoFile($fileDatos,$tipoData) : array
    {
        $TypeFile = $fileDatos->getClientOriginalExtension();
        
        $arrMsg = array();
        
        if($tipoData == 'imagenes' && $TypeFile != 'jpg'){
            $arrMsg['file'] = 'Solo se admiten archivo con extensión .PDF';
        }

        $arrMsgFinal = array(
            'error'  => true,
            'mensaje' => $arrMsg
        );
        return $arrMsgFinal;
    }


    public function verImagenes($cus){

        $data = DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_NOMBRES_FOTOS_CUS ?",[$cus]
        );
        
        $dato = array();
        $node = '';
        $ruta_arr = array();
        foreach($data as $da){
            $ruta = str_replace('\\', '/',$da->RUTA);
            $dir = str_replace('//srv-info/documentos$/','/mnt/documentos/',$ruta);
            if($node != $dir){
                $node = $dir;
                $ruta_arr[] = $node;
            }else{
                $node = '';
            }
            $dato[] = $da->NOMBRE_FOTO;
        }
        foreach($ruta_arr as $rut){
            $dd = $this->rutas($rut);
            if($dd){
                $dds[] = $this->rutas($rut);
            }
        }
        $contador = 0;
        foreach($dds as $unos ){
            foreach($unos as $uno){
                foreach($dato as $dos){
                    if($uno['file'] == $dos){
                        $contador++;
                        $compatible[] = array(
                            'ROW_NUMBER_ID' => $contador,
                            'ruta'          => $uno['Nombre'],
                            'NMB_FOTO'      => $uno['archivo'],
                        );
                    }
                }
            }
        }

        dd($compatible);

    }

    public function rutas($directorio){
        $res = array();
        if(substr($directorio, -1) != "/") $directorio .= "/";

        // Creamos un puntero al directorio y obtenemos el listado de archivos
        // echo $directorio; die();
        if(!is_dir($directorio)){
            return false;
        }
        $dir = @dir($directorio) or die("getFileList: Error abriendo el directorio $directorio para leerlo");
        while(($archivo = $dir->read()) !== false) {
            // Obviamos los archivos ocultos
            if($archivo[0] == ".") continue;
            if(is_dir($directorio . $archivo)) {
                $res[] = array(
                    "Nombre" => $directorio . $archivo . "/",
                    "archivo" => $archivo,
                    "Tamaño" => 0,
                    "file"  => '',
                    "Modificado" => filemtime($directorio . $archivo)
                );
            } else if (is_readable($directorio . $archivo)) {
                $file = explode('_',strrev($archivo));
                if(count($file)>0){
                    $file = strlen($file[0]);
                    $fileNew = substr($archivo, 0, -$file);
                    // dd($archivo,$fileNew);
                }
                $res[] = array(
                    "Nombre" => $directorio . $archivo,
                    "archivo" => $archivo,
                    "file" => $fileNew,
                    "Tamaño" => filesize($directorio . $archivo),
                    "Modificado" => filemtime($directorio . $archivo)
                );
            }
        }
        $dir->close();
        return $res;
    }


    public function cargaImagenesPrueba(Request $request){  
        $codigo_interno = $request->codigo_interno; 
        //dd($request->codigo_interno);
            
        $data = DB::connection('sqlsrv_S_')->select(
            "exec PA_SINABIP_NOMBRES_FOTOS_CUS ?",[$codigo_interno]
        );
        //dd($data[0]->RUTA);
        
        $dato = array(); 
        $node = '';
        $ruta_arr = array();
        $dds = array();
        $compatible = array();
        
        $ruta = str_replace('\\', '/',$data[0]->RUTA);
        $dir = str_replace('//srv-info/documentos$/','/mnt/srvinfo_doc/',$ruta);
        //dd($dir);
        if($node != $dir){
            $node = $dir;
            $ruta_arr[] = $node;
        }else{
            $node = '';
        }
        $dato[] = $data[0]->NOMBRE_FOTO;
        //dd($dato);
        foreach($ruta_arr as $rut){
            $dd = $this->rutas($rut);
            //dd($dd);
            if($dd){
                $dds[] = $this->rutas($rut);
            }
        }
        
        
        $contador = 0;
        if(count($dds)>0){
            foreach($dds as $unos ){
                foreach($unos as $uno){
                    foreach($dato as $dos){
                        if($uno['file'] == $dos){
                            $contador++;
                            $compatible[] = array(
                                'ROW_NUMBER_ID' => $contador,
                                'ruta'          => $uno['Nombre'],
                                'NMB_FOTO'      => $uno['archivo'],
                            );
                        }
                    }
                }
            }
        }
        dd($compatible);
        
        return response()->success([
            "listadoAporteFotos" => (count($compatible) > 0) ?$compatible : []
        ]);
    }
}

