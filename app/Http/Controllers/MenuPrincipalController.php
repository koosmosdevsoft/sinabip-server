<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use Pdf_Resumen;
use PHPExcel;
use PHPExcel_IOFactory;

class MenuPrincipalController extends Controller
{
    

    public function MenuBandejaReporteria(Request $request,$id) 
    {
        
        header('Access-Control-Allow-Origin: *');
        // $id    = $request->id;
	    $data1 = DB::select(
	        "exec PA_LISTADO_SUBMENU_BANDEJAREPORTERIA ?",[$id]
        );

        $data = json_decode(json_encode($data1),true);
        $menu = [];
    
        foreach($data as $rows){
            
            $menu[$rows['ID_MENU']] = [ 
                'estado' => $rows['COLUMNA'],
                'descripcion' => $rows['DESC_MENU'],
                'hijos' => []
            ];
            
        }

        foreach($data as $rows){
            $menu[$rows['ID_MENU']]['hijos'][$rows['ID_MENUSUB']] = [ 
            'descripcion' => $rows['DESC_MENUSUB'],
            'nombre_carpeta' => $rows['LINK_MENUSUB']
            ];
           
        }
        return response()->success($menu);
    }

}
