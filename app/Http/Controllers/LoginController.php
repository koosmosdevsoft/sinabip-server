<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\User;
use DB;
class LoginController extends Controller
{
    /*public function LoguearUsuario(Request $request){      
         $login  = $request->username;
         $passw    = $request->password;         

         $data = DB::select('exec REC.USP_SeaBuscarUsuarioxLogin ?,?',
            [$login,$passw] );

        return response()->success($data);                             
    }*/
 
       public function LoguearUsuario(Request $request){      
       $login  = $request->username;      
       $user = User::where('Usu_Login', $login)->first();       
       if ($user) {
           if (Hash::check($request->password, $user->Usu_Clave)) {
                 $tokenResult = $user->createToken('Laravel Password Grant Client');
                 $token = $tokenResult->token;
                 if ($request->remember_me)
                     $token->expires_at = Carbon::now()->addWeeks(1);
                 $token->save();
                 $response = [
                  'access_token' => $tokenResult->accessToken,
                  'token_type'   => 'Bearer',
                  'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                  'change_key'   => $user->Usu_CambioClave
                  
              ];
              return response()->success($response);
           } else {
               /*$response = "Password missmatch";
               return response()->error($response);*/
               $response = [
                'message'     =>"Password missmatch",
                'status_code' => 422
               ];
               return response()->success($response);
           }
   
       } else {
           //$response = 'User does not exist';
           //return response()->error($response);
            $response = [
                'message'     =>"User does not exist",
                'status_code' => 422
               ];
            return response()->success($response);
       }


       // return response()->success($data);                             
   }
  
    public function MenuporUsuario($codigousuario,$codigosistema){               
         $data = DB::select('exec REC.USP_SeaMenuporUsuario ?,?,0,0',[$codigousuario,$codigosistema] );
        return response()->success($data);                             
    }   

  public function _Menu($userid, $idsistema,$idperfil, $idmodulo)
    {        
        $results = DB::select('exec SEC.USP_SeaMenuporUsuario ?,?,?,?,?,?', [$userid, $idsistema,$idperfil, 0, 0,
                    $idmodulo]);        
        $result =  array();
        foreach ($results as $menupadre) {
            $result[] = array(
                'menu'    => $menupadre->Men_Nombre,                
                'submenu' => $this->_submenu($userid, $idsistema, $idperfil,$menupadre->PK_Men_Codigo,$idmodulo),
            );
        }
        return response()->json($result);
    }

    public function _submenu($userid, $idsistema,$idperfil, $idpadre,$idmodulo)
    {
        $result = DB::select('exec SEC.USP_SeaMenuporUsuario ?,?,?,?,?,?', [$userid, $idsistema,$idperfil, 1, 
                   $idpadre,$idmodulo]);
        return $result;
    } 
}
