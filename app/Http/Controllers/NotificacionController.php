<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use funciones\funciones;

class NotificacionController extends Controller
{
   
    //DATOS GENERALES    
    public function ListadoNotificaciones(Request $request){  
      
        $cood_usuario = $request->cood_usuario;

        $data = DB::connection('sqlsrv_S_')->select(
            "exec dbo.PA_SINABIP_LISTADO_NOTIFICACIONES ?",[$cood_usuario]
        );
        
        return response()->success([
            "listadoNotificaciones" => (count($data) > 0) ?$data : []
        ]);
    }

    public function Archivar_Notificacion(Request $request){

        $id_notificacion = $request->id_notificacion;
        $data = DB::connection('sqlsrv_S_')->select("exec dbo.PA_SINABIP_ARCHIVAR_NOTIFICACION ?",[$id_notificacion]);
        return response()->success($data);      
        
    }

    


}

