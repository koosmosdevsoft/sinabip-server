<?php
function url_exists( $url = NULL ) {

    if(( $url == '' ) ||( $url == NULL ) ){
        return false;
    }

    $headers = @get_headers( $url );
    sscanf($headers[0], 'HTTP/%*d.%*d %d', $httpcode);

    //Aceptar solo respuesta 200 (Ok), 301 (redirecci�n permanente) o 302 (redirecci�n temporal)
    $accepted_response = array(200,301,302);
    if( in_array( $httpcode, $accepted_response ) ) {
        return true;
    } else {
        return false;
    }
}

function prepareImageDBString($filepath)
{
    $out = 'null';
    $handle = @fopen($filepath, 'rb');
    if ($handle)
    {
        $content = @fread($handle, filesize($filepath));
        $content = bin2hex($content);
        @fclose($handle);
        $out = "0x".$content;
    }
}

function Fecha_a_Letras($x) {
      
   $day = substr($x, 0, 2);   
   $mon = substr($x, 3, 2);
   $year = substr($x, 6, 4);
   
   switch($mon) {
      case "01":
         $month = "Enero";
         break;
      case "02":
         $month = "Febrero";
         break;
      case "03":
         $month = "Marzo";
         break;
      case "04":
         $month = "Abril";
         break;
      case "05":
         $month = "Mayo";
         break;
      case "06":
         $month = "Junio";
         break;
      case "07":
         $month = "Julio";
         break;
      case "08":
         $month = "Agosto";
         break;
      case "09":
         $month = "Septiembre";
         break;
      case "10":
         $month = "Octubre";
         break;
      case "11":
         $month = "Noviembre";
         break;
      case "12":
         $month = "Diciembre";
         break;
   }
   
   return $day." de ".$month." de ".$year;
}

function getDirectorySize($path) 
{ 
  $totalsize = 0; 
  $totalcount = 0; 
  $dircount = 0; 
  if ($handle = opendir ($path)) 
  { 
    while (false !== ($file = readdir($handle))) 
    { 
      $nextpath = $path . '/' . $file; 
      if ($file != '.' && $file != '..' && !is_link ($nextpath)) 
      { 
        if (is_dir ($nextpath)) 
        { 
          $dircount++; 
          $result = getDirectorySize($nextpath); 
          $totalsize += $result['size']; 
          $totalcount += $result['count']; 
          $dircount += $result['dircount']; 
        } 
        elseif (is_file ($nextpath)) 
        { 
          $totalsize += filesize ($nextpath); 
          $totalcount++; 
        } 
      } 
    } 
  } 
  closedir ($handle); 
  $total['size'] = $totalsize; 
  $total['count'] = $totalcount; 
  $total['dircount'] = $dircount; 
  return $total; 
} 


function convertir_a_utf8($valor){
	$new_valor = iconv("UTF-8", "CP1252", $valor);
	return $new_valor; 
}

function sizeFormat($size) 
{ 
    if($size<1024) 
    { 
        return $size." bytes"; 
    } 
    else if($size<(1024*1024)) 
    { 
        $size=round($size/1024,1); 
        return $size." KB"; 
    } 
    else if($size<(1024*1024*1024)) 
    { 
        $size=round($size/(1024*1024),1); 
        return $size." MB"; 
    } 
    else 
    { 
        $size=round($size/(1024*1024*1024),1); 
        return $size." GB"; 
    } 

}  

function extension_archivo($ruta) {
	$res = explode(".", $ruta);
	$extension = $res[count($res)-1];
	return $extension ;
} 

function left($string,$count) {
$string = substr($string,0,$count);
return $string;
}

function right($string,$count) {
$string = substr($string, -$count, $count);
return $string;
}

function raiz() {
//$_SERVER["DOCUMENT_ROOT"];
$string = "/WEBSIEX";
//$string = "";
return $string;
}

function redimensionar_jpeg($img_original, $img_nueva, $img_nueva_anchura, $img_nueva_altura, $img_nueva_calidad)
{ 
	// crear una imagen desde el original 
	$img = ImageCreateFromJPEG($img_original); 
	// crear una imagen nueva 
	$thumb = imagecreatetruecolor($img_nueva_anchura,$img_nueva_altura); 
	// redimensiona la imagen original copiandola en la imagen 
	ImageCopyResized($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura,ImageSX($img),ImageSY($img)); 
 	// guardar la nueva imagen redimensionada donde indicia $img_nueva 
	ImageJPEG($thumb,$img_nueva,$img_nueva_calidad);
	ImageDestroy($img);
}

function tiempo_session($fechaGuardada){
	//$_SESSION["ultimoAcceso"]= date("Y-n-j H:i:s");// esto va en la pagina q inicia
	$fechaGuardada = $fechaGuardada;//=$_SESSION["ultimoAcceso"];
	$ahora = date("Y-n-j H:i:s");
	$tiempo_transcurrido = (strtotime($ahora)- strtotime($fechaGuardada));
	
	if($tiempo_transcurrido >= 10000) {
		//si pasaron 100 equivale a 10 minutos
		$url = raiz().'/SIGAP/index.php';
		session_destroy();
		header("location: $url");
	}else {
	$_SESSION["ultimoAcceso"] = $ahora;
	}
}

function cerrar_session(){
	session_destroy();
	header("location: ../index.php");
}

function fecha(){
$dias = array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
$meses=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
//$fecha=date("j-n-Y H:s a");
$fecha=date("j-n-Y");

$data=explode("-",$fecha);
$dia=$data[0];
$mes=$data[1];
$anio=$data[2];

$retval=$dias[date('w')].", ". $dia ." de ".$meses[$mes-1]." del ".$anio; 
echo $retval;
}

function mostrar_mes($mes){
$meses=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

return $meses[$mes-1];

}

function ObtenerIP(){
	
	if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"),"unknown")) 
		$ip = getenv("HTTP_CLIENT_IP");
	else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
	else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
		$ip = getenv("REMOTE_ADDR");
	else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
		$ip = $_SERVER['REMOTE_ADDR'];
	else
		$ip = "IP desconocida";
	return($ip);
}

function ObtenerURLPage() {
 $pageURL = 'http';
// if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

function getSistemaOperativo(){
	
	$useragent= strtolower($_SERVER['HTTP_USER_AGENT']);

	//winxp
	if (strpos($useragent, 'windows nt 5.1') !== FALSE) {
		return 'Windows XP';
		
	}elseif (strpos($useragent, 'windows nt 6.1') !== FALSE) {
		return 'Windows 7';
		
	}elseif (strpos($useragent, 'windows nt 6.0') !== FALSE) {
		return 'Windows Vista';
		
	}elseif (strpos($useragent, 'windows 98') !== FALSE) {
		return 'Windows 98';
		
	}elseif (strpos($useragent, 'windows nt 5.0') !== FALSE) {
		return 'Windows 2000';

	}elseif (strpos($useragent, 'windows nt 5.2') !== FALSE) {
		return 'Windows 2003 Server';

	}elseif (strpos($useragent, 'windows nt') !== FALSE) {
		return 'Windows NT';

	}elseif (strpos($useragent, 'win 9x 4.90') !== FALSE && strpos($useragent, 'win me')) {
		return 'Windows ME';

	}elseif (strpos($useragent, 'win ce') !== FALSE) {
		return 'Windows CE';
	
	}elseif (strpos($useragent, 'win 9x 4.90') !== FALSE) {
		return 'Windows ME';
	
	}elseif (strpos($useragent, 'windows phone') !== FALSE) {
		return 'Windows Phone';
	
	}elseif (strpos($useragent, 'iphone') !== FALSE) {
		return 'iPhone';
	
	}// experimental 
	elseif (strpos($useragent, 'ipad') !== FALSE) {
		return 'iPad';
	}elseif (strpos($useragent, 'webos') !== FALSE) {
		return 'webOS';
	
	}elseif (strpos($useragent, 'symbian') !== FALSE) {
		return 'Symbian';
	
	}elseif (strpos($useragent, 'android') !== FALSE) {
		return 'Android';
	
	}elseif (strpos($useragent, 'blackberry') !== FALSE) {
		return 'Blackberry';
	
	}elseif (strpos($useragent, 'mac os x') !== FALSE) {
		return 'Mac OS X';
		
	}elseif (strpos($useragent, 'macintosh') !== FALSE) {
		return 'Macintosh';
		
	}elseif (strpos($useragent, 'linux') !== FALSE) {
		return 'Linux';
		
	}elseif (strpos($useragent, 'freebsd') !== FALSE) {
		return 'Free BSD';
	
	}elseif (strpos($useragent, 'symbian') !== FALSE) {
		return 'Symbian';
	}else{
		return 'Desconocido';
	}
}

function getNavegador(){ 

	$u_agent = $_SERVER['HTTP_USER_AGENT']; 
	$bname = 'Unknown';
	$platform = 'Unknown';
	$version= "";


	// Next get the name of the useragent yes seperately and for good reason
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) { 
		$bname = 'Internet Explorer'; 
		$ub = "MSIE"; 
		
	}elseif(preg_match('/Firefox/i',$u_agent)){ 
		$bname = 'Mozilla Firefox'; 
		$ub = "Firefox"; 
		
	}elseif(preg_match('/Chrome/i',$u_agent)){ 
		$bname = 'Google Chrome'; 
		$ub = "Chrome"; 
		
	}elseif(preg_match('/Safari/i',$u_agent)){ 
		$bname = 'Apple Safari'; 
		$ub = "Safari"; 
		
	}elseif(preg_match('/Opera/i',$u_agent)){ 
		$bname = 'Opera'; 
		$ub = "Opera"; 
		
	}elseif(preg_match('/Netscape/i',$u_agent)){ 
		$bname = 'Netscape'; 
		$ub = "Netscape"; 
		
	}elseif(preg_match('/Gecko/i',$u_agent)){ 
		$bname = 'Mozilla'; 
		$ub = "Mozilla"; 
		
	}elseif(preg_match('/Galeon/i',$u_agent)){ 
		$bname = 'Galeon'; 
		$ub = "Galeon"; 
		
	}elseif(preg_match('/MyIE/i',$u_agent)){ 
		$bname = 'MyIE'; 
		$ub = "MyIE"; 
		
	}elseif(preg_match('/Lynx/i',$u_agent)){ 
		$bname = 'Lynx'; 
		$ub = "Lynx"; 
		
	}elseif(preg_match('/Konqueror/i',$u_agent)){ 
		$bname = 'Konqueror'; 
		$ub = "Konqueror"; 
	}
	
	
	// finally get the correct version number
	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	
	if (!preg_match_all($pattern, $u_agent, $matches)) {
	// we have no matching number just continue
	}

	// see how many we have
	$i = count($matches['browser']);
	if ($i != 1) {
	//we will have two since we are not using 'other' argument yet
	//see if version is before or after the name
	if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
		$version= $matches['version'][0];
	}else {
		$version= $matches['version'][1];}
	}else {
		$version= $matches['version'][0];
	}

	// check if we have a number
	if ($version==null || $version=="") {$version="?";}

	return array(
	'userAgent' => $u_agent,
	'name' => $bname,
	'version' => $version,
	'pattern' => $pattern
	);
} 

function Obtener_Datos_Navegador(){
	
	return array(
	'ip_secundario' => $_SERVER["REMOTE_ADDR"],
	'dominio' => $_SERVER['HTTP_HOST'],
	'puerto' => $_SERVER['SERVER_PORT'],
	'nombre_pc_2' => php_uname('n'),
	//'nombre_pc' => $_ENV["COMPUTERNAME"],
	'nombre_pc' => '',//$_ENV["USER"],	
	'nombre_host' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
	'nombre_user_sys' => get_current_user(),
	'nombre_nav_agente' => $_SERVER['HTTP_USER_AGENT'],
	'nom_usuario' => php_uname()
	);
	
}

/*
$sistema.="Sistema Operativo: ".getSistemaOperativo();
$ua=getNavegador();
$sistema.="<br> Navegador: ".$ua['name'] . " - " . $ua['version'];
$sistema.="<br > Agente Completo: " .$_SERVER['HTTP_USER_AGENT'];
echo $sistema;
*/


//-------------------------------------------------------------------------

function getNavegador_2(){
	
	$user_agent=$_SERVER['HTTP_USER_AGENT'];
	
	$navegadores = array(
	'Opera' => 'Opera',
	'Mozilla Firefox'=> '(Firebird)|(Firefox)',
	'Google Chrome'=>'(Chrome)',
	'Galeon' => 'Galeon',
	'Mozilla'=>'Gecko',
	'MyIE'=>'MyIE',
	'Lynx' => 'Lynx',
	'Chrome'=>'Chrome',
	'Netscape' => '(CHROME/23\.0\.1271\.97)|(Mozilla/4\.75)|(Netscape6)|(Mozilla/4\.08)|(Mozilla/4\.5)|(Mozilla/4\.6)|(Mozilla/4\.79)',
	'Konqueror'=>'Konqueror',
	'Internet Explorer 7' => '(MSIE 7\.[0-9]+)',
	'Internet Explorer 6' => '(MSIE 6\.[0-9]+)',
	'Internet Explorer 5' => '(MSIE 5\.[0-9]+)',
	'Internet Explorer 4' => '(MSIE 4\.[0-9]+)',
	);
	
	foreach($navegadores as $navegador=>$pattern){
		if (eregi($pattern, $user_agent))
		return $navegador;
	}
}

//-------------------------------------------------------------------------
function paginar($actual, $total, $por_pagina, $enlace) {  
	$total_paginas = ceil($total/$por_pagina);  
	$anterior = $actual - 1;  $posterior = $actual + 1;  
	if ($actual>1) 
	$texto = "<a href=\"$enlace$anterior\">�</a> ";  
	else    
	$texto = "<b>�</b> ";  
	for ($i=1; $i<$actual; $i++)    
	$texto .= "<a href=\"$enlace$i\">$i</a> ";  
	$texto .= "<b>$actual</b> ";  
	for ($i=$actual+1; $i<=$total_paginas; $i++)    
	$texto .= "<a href=\"$enlace$i\">$i</a> ";  
	if ($actual<$total_paginas)    
	$texto .= "<a href=\"$enlace$posterior\">�</a>";  
	else    
	$texto .= "<b>�</b>";  
	return $texto;
}

/***************  Para formularios Antes del Jquery ************/

function fcnphp_AccionMensaje($opt) {  
	if($opt==1){
		$clase='exito';
		$contenido="Se registro correctamente un registro.";
	}else if($opt==2){
		$clase='info';
		$contenido="Se actualizo correctamente un registro.";
	}else if($opt==3){
		$clase='alerta';
		$contenido="Se elimino un registro  correctamente.";
	}else if($opt==4){
		$clase='error';
		$contenido="Este Registro ya existe en la Base de Datos.";
	}
	return array ($clase, $contenido); 
}

/*************************************************************/

function PHP_Mensaje($opt) { 
	if($opt==1){
		$clase='exito';
		$contenido="Se registro correctamente un registro.";
	}else if($opt==2){
		$clase='info';
		$contenido="Se actualizo correctamente un registro.";
	}else if($opt==3){
		$clase='alerta';
		$contenido="Se elimino un registro  correctamente.";
	}else if($opt==4){
		$clase='error';
		$contenido="Este Registro ya existe en la Base de Datos.";
	}
echo '<script>$("#div_mensaje").removeClass("'.$clase.'", 1000);</script>';
echo '<script>$("#div_mensaje").html("'.$contenido.'").addClass("'.$clase.'");</script>';
echo '<script>$("#div_mensaje").fadeIn(600).fadeOut(500).fadeIn(600).fadeOut(500)</script>';
}


function PHP_Nuevo_Mensaje($div,$clase,$mensaje) { 
echo '<script>$("#'.$div.'").removeClass("error", 1000);</script>';
echo '<script>$("#'.$div.'").html("'.$mensaje.'").addClass("'.$clase.'");</script>';
}
/*************************************************************/

function PHP_MostrarDepa($codigo) { 
	$query1 ="select * from t_departamento where cod_pais=1 and cod_depa=$codigo";
	$result = odbc_exec(conectar(),$query1); 
	while(odbc_fetch_row($result)) { 	
	  	$cod_depa = odbc_result($result, "cod_depa"); 
		$nom_depa = odbc_result($result, "nom_depa"); 
	} 
	desconectar();	
	return array ($cod_depa, $nom_depa); 
}

//$data= PHP_MostrarDepa($cod);
//$codigo=$data[1];

/*************************************** Calculo de fechas ************************************************/

function calculo_fechas($fechaInicio,$fechaActual){

	$diaActual = substr($fechaActual, 0, 2);
	$mesActual = substr($fechaActual, 3, 5);
	$anioActual = substr($fechaActual, 6, 10);
	
	$diaInicio = substr($fechaInicio, 0, 2);
	$mesInicio = substr($fechaInicio, 3, 5);
	$anioInicio = substr($fechaInicio, 6, 10);
	$b = 0;
	$mes = $mesInicio-1;

	if($mes==2){
		if(($anioActual%4==0 && $anioActual%100!=0) || $anioActual%400==0){
			$b = 29;
		}else{
			$b = 28;
		}
	}else if($mes<=7){
		if($mes==0){
		 $b = 31;
		}else if($mes%2==0){
				$b = 30;
			  }else{
				$b = 31;
			  }
	}else if($mes>7){
	  if($mes%2==0){
		$b = 31;
	  }else{
		$b = 30;
	  }
	}
	
	  
  if(($anioInicio>$anioActual) || ($anioInicio==$anioActual && $mesInicio>$mesActual) || 
  ($anioInicio==$anioActual && $mesInicio == $mesActual && $diaInicio>$diaActual)){
	//echo "alert('La fecha de inicio ha de ser anterior a la fecha Actual.')";
  }else{
	  
	  if($mesInicio <= $mesActual){
		$anios = $anioActual - $anioInicio;
		  if($diaInicio <= $diaActual){
			$meses = $mesActual - $mesInicio;
			$dies = $diaActual - $diaInicio;
		  }else{
			  if($mesActual == $mesInicio){
				$anios = $anios - 1;
			  }
			  $meses = ($mesActual - $mesInicio - 1 + 12) % 12;
			  $dies = $b-($diaInicio-$diaActual);
		  }
	  }else{
		$anios = $anioActual - $anioInicio - 1;
		  if($diaInicio > $diaActual){
			$meses = $mesActual - $mesInicio -1 +12;
			$dies = $b - ($diaInicio-$diaActual);
		  }else{
			$meses = $mesActual - $mesInicio + 12;
			$dies = $diaActual - $diaInicio;
		  }
	  }
	
	  $valor=$anios.'-'.$meses.'-'.$dies;
	return $valor;
	 /* 
	//calculo timestam de las dos fechas
	$timestamp1 = mktime(0,0,0,$mesActual,$diaActual,$anioActual);
	$timestamp2 = mktime(4,12,0,$mesInicio,$diaInicio,$anioInicio);
	
	//resto a una fecha la otra
	$segundos_diferencia = $timestamp1 - $timestamp2;
	echo "xdcx".$segundos_diferencia."ooo";
	
	//convierto segundos en d�as
	$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
	
	//obtengo el valor absoulto de los d�as (quito el posible signo negativo)
	$dias_diferencia = abs($dias_diferencia);
	
	//quito los decimales a los d�as de diferencia
	$dias_diferencia = floor($dias_diferencia);
	
	echo $dias_diferencia;  
	*/
	}
}

/************************************************************************************************************/

function MES_A_TEXTO($MES){

	switch ($MES) { 
		case 1: $DESCRIPCION="ENERO"; break;
		case 2: $DESCRIPCION="FEBRERO"; break;
		case 3: $DESCRIPCION="MARZO"; break;
		case 4: $DESCRIPCION="ABRIL"; break;
		case 5: $DESCRIPCION="MAYO"; break;
		case 6: $DESCRIPCION="JUNIO"; break;
		case 7: $DESCRIPCION="JULIO"; break;
		case 8: $DESCRIPCION="AGOSTO"; break;
		case 9: $DESCRIPCION="SETIEMBRE"; break;
		case 10: $DESCRIPCION="OCTUBRE"; break;
		case 11: $DESCRIPCION="NOVIEMBRE"; break;
		case 12: $DESCRIPCION="DICIEMBRE"; break;
	} 

	return $DESCRIPCION; 
}

function estado_pago($estados_pagos){

	switch ($estados_pagos) { 
		case 0: 
		    $estado="Pendiente"; 
			$color="#FF0000";
			break;
		case 1: 
		    $estado="Cancelado"; 
			$color="#336699";
			break;
		case 2: 
		    $estado="Otros"; 
			$color="#000000";
			break;
	} 

	return array ($estado, $color); 
}

function restaFechas($dFecIni, $dFecFin){
	/*
	$dFecIni = str_replace("-","",$dFecIni);
	$dFecIni = str_replace("/","",$dFecIni);
	$dFecFin = str_replace("-","",$dFecFin);
	$dFecFin = str_replace("/","",$dFecFin);
	*/
	
	list($day_INI,$month_INI,$year_INI)=explode("-",$dFecIni);
	$date1 = mktime(0,0,0,$month_INI, $day_INI, $year_INI);
	
	list($day_FIN,$month_FIN,$year_FIN)=explode("-",$dFecFin);
	$date2 = mktime(0,0,0,$month_FIN, $day_FIN, $year_FIN);
	
	/*
	ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
	ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);
	
	$date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[1], $aFecIni[3]);
	$date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[1], $aFecFin[3]);
	*/
	
	return round(($date2 - $date1) / (60 * 60 * 24)+1);
}

function tiempo_resta($fecha1,$fecha2){ 
     //separo la fecha1 
   list($fecha,$hora)=explode(' ',$fecha1); 
   list($a�o,$mes,$dia)=explode('-', $fecha); 
   list($hor,$min,$sec)=explode(':',$hora); 
     //calculo timestamp de la fecha1 
   $y=mktime($hor,$min,$sec,$mes,$dia,$a�o); 

     //separo la fecha2 
   list($fecha,$hora)=explode(' ',$fecha2); 
   list($a�o,$mes,$dia)=explode('-', $fecha); 
   list($hor,$min,$sec)=explode(':',$hora); 
     //calculo timestamp de la fecha1 
   $z=mktime($hor,$min,$sec,$mes,$dia,$a�o); 
   $resta = $z - $y; 
   $dias = $resta / 86400; 
   return $dias; 
} 

function tanto_porciento($gastos,$importe){ 
	$tanto_porciento=round(($gastos*100)/$importe,2);
	$tanto_porciento=$tanto_porciento.'%';
	return $tanto_porciento; 
}

function tanto_porciento_numero($gastos,$importe){ 
	$tanto_porciento=round(($gastos*100)/$importe,2);
	return $tanto_porciento; 
}

function diferencia_fechas($date1, $date2){
	
	//$date1 = strtotime('2013-07-03 18:00:00');
	//$date2 = time();
	$subTime = $date1 - $date2;
	$y = ($subTime/(60*60*24*365));
	$d = ($subTime/(60*60*24))%365;
	$h = ($subTime/(60*60))%24;
	$m = ($subTime/60)%60;
	
//	echo "Difference between ".date('Y-m-d H:i:s',$date1)." and ".date('Y-m-d H:i:s',$date2)." is:\n";
//	echo $y." years\n";
//	echo $d." days\n";
//	echo $h." hours\n";
//	echo $m." minutes\n";
	return $subTime;
}

/*
 * segundos minuto = 60
 * segundos hora = 3600
 * segundos d�a = 86400
 * segundos mes = 2592000
 * segundos a�o = 31104000
 */

function interval_date($init,$finish){
    //formateamos las fechas a segundos tipo 1374998435
    $diferencia = strtotime($finish) - strtotime($init);

    //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
    //floor devuelve el n�mero entero anterior, si es 5.7 devuelve 5
    if($diferencia < 60){
        $tiempo = "Hace " . floor($diferencia) . " segundos";
    }else if($diferencia > 60 && $diferencia < 3600){
        $tiempo = "Hace " . floor($diferencia/60) . " minutos'";
    }else if($diferencia > 3600 && $diferencia < 86400){
        $tiempo = "Hace " . floor($diferencia/3600) . " horas";
    }else if($diferencia > 86400 && $diferencia < 2592000){
        $tiempo = "Hace " . floor($diferencia/86400) . " d�as";
    }else if($diferencia > 2592000 && $diferencia < 31104000){
        $tiempo = "Hace " . floor($diferencia/2592000) . " meses";
    }else if($diferencia > 31104000){
        $tiempo = "Hace " . floor($diferencia/31104000) . " a�os";
    }else{
        $tiempo = "Error";
    }
    return $tiempo;
}

//echo interval_date("2013/07/26","2013/07/28");
//Hace 2 d�as
//echo interval_date("2013/07/28 10:00:00","2013/07/28 10:00:35");
//Hace 35 segundos
//echo interval_date("2012/07/28","2013/07/28");
//Hace 1 a�os
/*******************************************************/

function filtrar_caracteres($TEXTO){	
	$nuevo_texto = str_replace("\\","",str_replace("'", '', str_replace('"', '', $TEXTO )));
	return  $nuevo_texto ; 
}

function filtrar_caracteres_02($TEXTO){	
	$nuevo_texto = str_replace(")"," ",str_replace("("," ",str_replace("N�","Nro",str_replace("-"," ",str_replace(","," ",str_replace("/", "",str_replace("\\","",str_replace("'", '', str_replace('"', '', $TEXTO )))))))));
	return  $nuevo_texto ; 
}

////////////////////////////////////////////////////  
//Convierte fecha de mysql a normal  
////////////////////////////////////////////////////  
function  cambiaf_a_normal ( $fecha_Hora ){  
	
	list($fecha,$hora)=explode(" ",$fecha_Hora);
	
	list($year,$month,$day)=explode("-",$fecha);
	if(checkdate((int)$month,(int)$day,(int)$year)){		
		$lafecha = $day."-".$month."-".$year;
	}else{
		$lafecha = '';
	}
	
	return  $lafecha ; 
	
	/*
	ereg (  "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})" ,  $fecha ,  $mifecha );  
	$lafecha = $mifecha [ 3 ]. "-" . $mifecha [ 2 ]. "-" . $mifecha [ 1 ];
	if($lafecha == '--') $lafecha = '';
	return  $lafecha ;  
	*/
}  

function  cambiaf_a_normal_2 ( $fecha ){  
	
	//list($fecha,$hora)=explode(" ",$fecha_Hora);
	
	list($year,$month,$day)=explode("-",$fecha);
	if(checkdate((int)$month,(int)$day,(int)$year)){		
		$lafecha = $day."-".$month."-".$year;
	}else{
		$lafecha = '';
	}
	
	return  $lafecha ; 
	
	/*
	ereg (  "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})" ,  $fecha ,  $mifecha );  
	$lafecha = $mifecha [ 3 ]. "-" . $mifecha [ 2 ]. "-" . $mifecha [ 1 ];
	if($lafecha == '--') $lafecha = '';
	return  $lafecha ;  
	*/
}  
////////////////////////////////////////////////////  

//Convierte fecha de normal a mysql  
////////////////////////////////////////////////////  

function  cambiaf_a_mysql ( $fecha_Hora ){  

	list($fecha,$hora)=explode(" ",$fecha_Hora);
	
	list($day,$month,$year)=explode("-",$fecha);
	if(checkdate((int)$month,(int)$day,(int)$year)){		
		$lafecha = $year."-".$month."-".$day;
	}else{
		$lafecha = '';
	}
	
	return  $lafecha ;
	
	/*
	ereg (  "([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})" ,  $fecha ,  $mifecha );  
	$lafecha = $mifecha [ 3 ]. "-" . $mifecha [ 2 ]. "-" . $mifecha [ 1 ];  
	if($lafecha == '--') $lafecha = '';
	return  $lafecha ;  
	*/
} 

function  cambiar_Fecha_a_SqlServer ( $fecha_Hora ){  
	
	list($fecha,$hora)=explode(" ",$fecha_Hora);
	
	list($day,$month,$year)=explode("-",$fecha);
	if(checkdate((int)$month,(int)$day,(int)$year)){		
		$lafecha = $month."-".$day."-".$year;
	}else{
		$lafecha = '';
	}
	
	return  $lafecha ; 
	
	/*
	ereg (  "([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})" ,  $fecha ,  $mifecha );  
	$lafecha =  $mifecha [ 2 ]. "-" . $mifecha [ 1 ]. "-" .$mifecha [ 3 ];  
    if($lafecha == '--') $lafecha = '';
	return  $lafecha ;  
	*/
}  

function  cambiar_Fecha_a_SqlPostGres ( $fecha_Hora ){  

	list($fecha,$hora)=explode(" ",$fecha_Hora);

	list($day,$month,$year)=explode("-",$fecha);
	if(checkdate((int)$month,(int)$day,(int)$year)){		
		$lafecha = $year."-".$month."-".$day;
	}else{
		$lafecha = '';
	}
	
	return  $lafecha ; 
	
	/*
	ereg (  "([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})" ,  $fecha ,  $mifecha );  
	$lafecha =  $mifecha [ 3 ]. "-" . $mifecha [ 2 ]. "-" .$mifecha [ 1 ];  
    if($lafecha == '--') $lafecha = '';
	return  $lafecha ;  
	*/
}  

function encriptar($cadena, $clave = "una clave secreta password"){
	$cifrado = MCRYPT_RIJNDAEL_256;
	$modo = MCRYPT_MODE_ECB;
	return mcrypt_encrypt($cifrado, $clave, $cadena, $modo,
		mcrypt_create_iv(mcrypt_get_iv_size($cifrado, $modo), MCRYPT_RAND)
		);
}

function desencriptar($cadena, $clave = "una clave secreta password"){
	$cifrado = MCRYPT_RIJNDAEL_256;
	$modo = MCRYPT_MODE_ECB;
	return mcrypt_decrypt($cifrado, $clave, $cadena, $modo,
		mcrypt_create_iv(mcrypt_get_iv_size($cifrado, $modo), MCRYPT_RAND)
		);
}

function redondear_dos_decimal($valor) { 
   $float_redondeado=round($valor * 100) / 100; 
   return $float_redondeado; 
}

function  Agregar_ceros($numdecimal){
  
	$new_numdecimal = explode(".",$numdecimal);
	
	$Parte_Entera	= $new_numdecimal [0];
	$Parte_decimal	= $new_numdecimal [1];
	
	$long_decimal = strlen($Parte_decimal);
	
	if($Parte_decimal==''){
		$decimal = $Parte_decimal.'00';
	}else if($Parte_decimal<10 && $long_decimal == 1){
		$decimal = $Parte_decimal.'0';
	}else if($Parte_decimal<10 && $long_decimal == 2){
		$decimal = $Parte_decimal;
	}else if($Parte_decimal>10){
		$decimal = $Parte_decimal;
	}
	$numero_valor = $Parte_Entera.'.'.$decimal;
	
     return  $numero_valor ;  
} 


function filtrar_datos($str){
	//$str = "the cat\r\nsat\r\non the mat";
	
	$arr = explode ("\n", $str);
	$cadena = '';

	$cadena = $cadena."<ul>";
	foreach ($arr as $item){
    	$cadena = $cadena."<li>".htmlentities($item)."</li>";
	}
	$cadena = $cadena."</ul>";
	return $cadena;
}


function sqlEscape($sql) { 

    $fix_str    = stripslashes($sql); 
    $fix_str    = str_replace("'","''",$sql); 
    $fix_str    = str_replace("\n\r","<br><br>",$fix_str); 

    return htmlentities($fix_str); 

}


function palabras($min = 4, $max = 8){		
	$vocales 	= array('a', 'e', 'i', 'o', 'u');
	$consonantes 	= array('b', 'c', 'd', 'f', 'g', 'j', 'l', 'm', 'n', 'p', 'r', 's', 't');
	$tamano 	= intval(rand($min, $max));
	$actual		= intval(rand(1,2));		
	$nombre 	= '';	
	
	for($x=0;$x<$tamano;$x++){			
		if($actual == 0){
			
			$actual	= 1;
			$pos 	= rand(0,count($vocales)-1);
			$nombre	.=  $vocales[$pos];				
			
		}else{
			
			$actual	= 0;
			$pos 	= rand(0,count($consonantes)-1);
			$nombre	.=  $consonantes[$pos];				
		}				
	}
	return ucfirst($nombre);
}


function texto_aleatorio($numerodeletras){
	
	$caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
	//$numerodeletras=25; //numero de letras para generar el texto
	$cadena = ""; 
	for($i=0;$i<$numerodeletras;$i++){
		$cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); 
		/*Extraemos 1 caracter de los caracteres entre el rango 0 a Numero de letras que tiene la cadena */
	}
	
	return utf8_encode($cadena); 
}


/**
 * Renderiza un template con las variables necesarias.
 * @param $template_file Nombre del archivo a renderizar.
 * @param $variables Array [clave => valor] con las variables a renderizar.
 * @return resultado de la interpretacion de elementos.
 */
function render($template_file, $variables) {

  extract($variables, EXTR_SKIP);

  ob_start();


  // include DIRECTORY_ROOT . '/' . $template_file;
  include $template_file;

  print ob_get_clean();
}

function ruta_server(){
	 return '//srv-portalsbn/htdocs$/repositorio_muebles/Inventarios_zip/'; 
	//return '/mnt/Invenarios_zip';
}

?>