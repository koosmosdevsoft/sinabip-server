<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();  
        $this->mapLoginRoutes();
        $this->mapWebRoutes();
        $this->mapCombosBusqueda();
        $this->mapUnidad();
        $this->mapNotificacion();
        $this->mapcontrolregistroSDRC();
        $this->mapaporteFotos();
        $this->mapinformeAsociados();
        $this->mapestandarizacionRouter();
        $this->mapprediosIncorporadosRouter();
        
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapLoginRoutes()
    {
        Route::prefix('login')
             ->namespace($this->namespace)
             ->group(base_path('routes/login.php'));
    }
    protected function mapCombosBusqueda()
    {
        Route::prefix('busqueda')
             ->namespace($this->namespace)
             ->group(base_path('routes/busqueda.php'));
    }
    protected function mapUnidad()
    {
        Route::prefix('unidad')
             ->namespace($this->namespace)
             ->group(base_path('routes/unidad.php'));
    }

    protected function mapNotificacion()
    {
        Route::prefix('notificacion')
             ->namespace($this->namespace)
             ->group(base_path('routes/notificacion.php'));
    }

    protected function mapcontrolregistroSDRC()
    {
        Route::prefix('controlregistroSDRC')
             ->namespace($this->namespace)
             ->group(base_path('routes/controlregistroSDRC.php'));
    }

    protected function mapaporteFotos()
    {
        Route::prefix('aporteFotos')
             ->namespace($this->namespace)
             ->group(base_path('routes/aporteFotos.php'));
    }

    protected function mapinformeAsociados()
    {
        Route::prefix('informeAsociados')
             ->namespace($this->namespace)
             ->group(base_path('routes/informeAsociados.php'));
    }

    protected function mapestandarizacionRouter()
    {
        Route::prefix('estandarizacionRouter')
             ->namespace($this->namespace)
             ->group(base_path('routes/estandarizacionRouter.php'));
    }

    protected function mapprediosIncorporadosRouter()
    {
        Route::prefix('prediosIncorporados')
             ->namespace($this->namespace)
             ->group(base_path('routes/prediosIncorporados.php'));
    }


    

    
}
